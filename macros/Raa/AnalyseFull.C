#ifndef ANALYSEFULL_C_5HKGVLF1
#define ANALYSEFULL_C_5HKGVLF1



/**
 * @file   AnalyseFull.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 10 10:30:19 2017
 *
 * @brief Run analysis of tree for all centrality bins, and optionally
 * do the final RAA calculation.
 * @ingroup alice_masterclass_raa_part2
 */
#include "../Analyse.C"
#include "Final.C"

/**
 * Run all stuff
 *
 * @param alsoFinal If true, also finalize
 *
 * @ingroup alice_masterclass_raa_part2
 */
inline void AnalyseFull(Bool_t alsoFinal = false)
{
  Analyse(0, 5);
  Analyse(5, 10);
  Analyse(10, 20);
  Analyse(20, 30);
  Analyse(30, 40);
  Analyse(40, 50);
  Analyse(50, 60);
  Analyse(60, 70);
  Analyse(70, 80);

  if (alsoFinal) {
    Final();
  }
}

#endif /* end of include guard: ANALYSEFULL_C_5HKGVLF1 */
