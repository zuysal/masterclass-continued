#ifndef ANALYSEPART2START_C_RIJ0IXYU
#define ANALYSEPART2START_C_RIJ0IXYU

/**
 * @file   Analyse.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Mar  9 20:30:55 2017
 *
 * @brief  Analyse data in a single centrality bin
 *
 * @ingroup alice_masterclass_raa_part2
 */
#include <iostream>
#include "Raa/Utilities.C"

/**
 * Run the single centrality bin analysis
 *
 * @param minCentrality Least centrality to consider
 * @param maxCentrality Largest centrality to consider
 * @ingroup alice_masterclass_raa_part2
 */
inline void Analyse(Int_t minCentrality, Int_t maxCentrality)
{
  // Set-up default style
  StyleSettings();

  // Open the input file
  TFile* input = OpenInput(ResourcePath("data/Raa/MasterClassesTree_LHC10h_Run139036.root"));
  if (!input)
    return;

  // Set up variables to read data into, get our tree, make
  // histograms, and find maximum number of entries (events) to read.
  Int_t nTrack;
  Float_t centrality;
  TTree* eventTree = GetEventTree(input, &centrality, &nTrack);
  TH1* multHist = MakeMultHistogram(minCentrality, maxCentrality);
  TH2* multCentHist = MakeMultCentHistogram(minCentrality, maxCentrality);
  ULong64_t nEntries = MaxEntries(eventTree);

  // Loop over entries in the event tree, and record the track
  // multiplicty as well as the correlation of the track multiplicity
  // and centrality.
  Int_t nEvents = 0;
  std::cout << "Loop over track tree " << std::flush;
  for (ULong64_t i = 0; i < nEntries; i++) {
    // Print update on every 10 thousand events
    Progress(i, 10000);
    // Try to read entry from tree.  If it fails, stop the loop
    if (!ReadEntry(eventTree, i, nEntries))
      break;
    // Check if the read centrality is within range. If not, go on the
    // next entry
    if (!CheckCentrality(centrality, minCentrality, maxCentrality))
      continue;
    // The centrality is OK, so we fill into our histograms
    multHist->Fill(nTrack);
    multCentHist->Fill(nTrack, centrality);
    nEvents++;
  }
  // Scale multiplicity to number of events
  std::cout << ' ' << nEvents << " accepted" << std::endl;
  multHist->Scale(1. / nEvents);

  // Set up variables to read data into, get our tree, make
  // histogram, and find maximum number of entries (tracks) to read.
  Double_t pt;
  TTree* trackTree = GetTrackTree(input, &centrality, &pt);
  TH1* ptHist = MakePtHistogram(minCentrality, maxCentrality);
  nEntries = MaxEntries(trackTree);

  // Loop over all tracks
  std::cout << "Loop over track tree " << std::flush;
  for (ULong64_t i = 0; i < nEntries; i++) {
    Progress(i, 1000000);
    // Try to read entry from tree.  If it fails, stop the loop

    break;

    // next entry
    if (!CheckCentrality(centrality, minCentrality, maxCentrality))
      continue;
    // The centrality is OK, so we fill into our histogram
    ptHist->Fill(pt);
  }
  // Scale pT to number of events and bins size
  std::cout << " done" << std::endl;
  ptHist->Scale(1. / nEvents, "WIDTH");

  // Open output file, write histogram to it, and flush to disk.
  // Note, we overwrite any previous histograms
  TFile* output = OpenOutput();
  multHist->Write(multHist->GetName(), TObject::kOverwrite);
  multCentHist->Write(multCentHist->GetName(), TObject::kOverwrite);
  ptHist->Write(ptHist->GetName(), TObject::kOverwrite);
  output->Write();
  output->Close();

  // --- Plot the results --------------------------------------------
  // First we set up some styling
  gStyle->SetOptStat(0);

  // Then make a drawing area (a canvas), which we divide into parts
  TCanvas* c = new TCanvas(HistogramName("c", minCentrality, maxCentrality),
                           HistogramTitle("", minCentrality, maxCentrality), 1200, 800);
  c->SetTopMargin(0.01);
  c->SetRightMargin(0.01);
  c->Divide(2, 1, 0, 0);

  // Left part, divided into two parts
  TVirtualPad* p = c->cd(1);
  p->SetRightMargin(0.01);
  p->SetBottomMargin(0.13);
  p->Divide(1, 2, 0, 0);

  // Left column top
  TVirtualPad* q = p->cd(1);
  q->SetLeftMargin(0.13);
  q->SetRightMargin(0.13);
  DrawH1(multHist, q, true);

  // Left column bottom
  q = p->cd(2);
  q->SetLeftMargin(0.13);
  q->SetRightMargin(0.13);
  DrawH2(multCentHist, q, true);

  // Right column
  p = c->cd(2);
  p->SetRightMargin(0.01);
  p->SetLeftMargin(0.09);
  p->SetBottomMargin(0.08);
  DrawH1(ptHist, p, true, 0.03);

  // Print to file
  c->SaveAs(Form("Result_%03d_%03d.pdf", minCentrality, maxCentrality));
}
//
// EOF
//
#endif /* end of include guard: ANALYSEPART2START_C_RIJ0IXYU */
