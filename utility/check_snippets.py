#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Do static analysis if all strings for the translations are provided.
"""
import logging
import os
import re
import sys


def base_dirs():
    """Return the list of directories containing snippet translations."""
    return [
        "translation/EntryPoint",
        "translation/Raa",
        "translation/Strangeness",
        "translation/Utility",
        "translation/Jpsi",
    ]


def enlish_files(directory: str):
    """
    Returns a list of filenames that are an english snippet file.
    """
    filenames = []
    snippet_pattern = re.compile(r'.*\.txt\.en')
    for file in os.listdir(directory):
        if snippet_pattern.match(file):
            logging.debug("Matched pattern on %s", file)
            filenames.append(file)
        else:
            logging.debug("Did not match pattern on %s", file)

    return sorted(filenames)


def translated_files(directory: str, base_file: str):
    """
    Returns a list of file that translate the `base_file`.
    English is explicitly excluded and the `base_file` is expected to NOT have
    the `.en` extension at the end.
    """
    filenames = []
    for file in os.listdir(directory):
        if not file.endswith(".en") and file.startswith(base_file):
            filenames.append(file)
    return filenames


def parse_registered_keys(filename):
    keys = set()
    with open(filename) as snippet_file:
        for line in snippet_file:
            logging.debug(line[:-1])

            if not line.strip().startswith("RegisterText"):
                continue
            second_qutation = line.find("\"", 15)
            assert second_qutation > 14, "Did not find good key"

            key = line[14:second_qutation]
            logging.debug("Extracted key '%s'", key)

            keys.add(key)
    return keys


def main():
    """
    Parse all snippet translations that exist and find out if there is
    a inconsistency between the english version and the other languages.
    Every inconsistency is reported.
    """
    return_code = 0
    for translation_dir in base_dirs():
        for base_file in enlish_files(translation_dir):
            ref_filename = os.path.join(translation_dir, base_file)
            print("Using {} as reference file".format(ref_filename))

            # Retrieve a list of files that translate the english file
            # in the currently analyzed directory. Remove the `.en` from the
            # filename.
            translations = translated_files(translation_dir, base_file[:-3])

            ref_keys = parse_registered_keys(ref_filename)
            for translation_file in translations:
                trans_fname = os.path.join(translation_dir, translation_file)
                trans_keys = parse_registered_keys(trans_fname)

                # Calculate the difference of the keys for the reference and
                # the translation.
                diff = ref_keys.symmetric_difference(trans_keys)

                if len(diff) > 0:
                    return_code = 1
                    e = "The keys in {} and {} differ!".format(
                        ref_filename, trans_fname)
                    print(e)

                    for key in diff:
                        print("Detected missmatch for '{}'".format(key))
                else:
                    print("{} registers the same keys as {}".format(
                        trans_fname, ref_filename))
    return return_code


if __name__ == "__main__":
    logging.basicConfig(level=logging.WARNING)
    sys.exit(main())
