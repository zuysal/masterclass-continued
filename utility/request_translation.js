// This script uses 'phantomjs' to request the translation from google.

var page = require('webpage').create(),
  system = require('system');

if (system.args.length != 4) {
  console.log("Usage: <prog> <source_lang> <target_lang> <string to translate>");
  console.log("");
  console.log(`Example: ${system.args[0]} de en \"Hallo ich bin ein Bot\"`);
  phantom.exit()
}

var source = system.args[1];
var target = system.args[2];
var text = encodeURIComponent(system.args[3]);
var address = `https://translate.google.com/#${source}/${target}/${text}`;

page.open(address,
  function(status) {
    if (status != "success") {
      console.log("Failed to query from translate.google.com");
    } else {
      var html = page.evaluate(function() {
        return document.getElementById("result_box").textContent;
      });
      console.log(html);
    }
    phantom.exit();
  });
