#!/bin/bash

# This script will check Include-What-You-Use for the whole project
# and is meant to be run from gitlab-ci

echo "Running Include-What-You-Use"
iwyu_tool.py -j$(nproc) -p .
exit $?
