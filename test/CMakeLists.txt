set(ROOT_DEPENDENCIES Core EG Eve Geom Gpad Graf Hist MathCore Matrix Physics RIO Tree VMC Gui GuiHtml)
include_directories(SYSTEM ${ROOT_INCLUDE_DIR})
link_directories(SYSTEM ${ROOT_LIBDIR})

configure_file(${CMAKE_SOURCE_DIR}/test/base_path.h.in ${CMAKE_SOURCE_DIR}/test/base_path.h @ONLY)

# FIXME Remove the code duplications for adding test executables
add_executable(test_utility.x
  test_driver.cpp
  #test_translation.cpp
  #test_renderelements.cpp
  #test_vsd_reader.cpp
  )
target_link_libraries(test_utility.x 
  PUBLIC
  ${ROOT_DEPENDENCIES}
  Catch2::Catch2 
  # Order in the project libraries matters!
  EntryPoint Raa Strangeness Jpsi Utility 
  gsl)
target_include_directories(test_utility.x 
  PRIVATE 
  ${CMAKE_SOURCE_DIR}/src
  ${CMAKE_SOURCE_DIR}/translation
  ${CMAKE_SOURCE_DIR}/lib/gsl-lite/include) # FIXME Hack, but works

add_executable(test_raa.x
  test_driver.cpp
  #test_collision_system.cpp
  #test_track_classification.cpp
  #test_track_counter.cpp
  #test_raa_event_display.cpp
  )
target_link_libraries(test_raa.x 
  PUBLIC
  ${ROOT_DEPENDENCIES}
  Catch2::Catch2 
  # Order in the project libraries matters!
  Raa Utility EntryPoint Strangeness Jpsi 
  gsl)
target_include_directories(test_raa.x 
  PRIVATE 
  ${CMAKE_SOURCE_DIR}/src
  ${CMAKE_SOURCE_DIR}/translation
  ${CMAKE_SOURCE_DIR}/lib/gsl-lite/include) # FIXME Hack, but works

add_executable(test_raa_analysis.x
  test_driver.cpp
  test_raa_statistics.cpp
  )
target_link_libraries(test_raa_analysis.x 
  PUBLIC
  ${ROOT_DEPENDENCIES}
  Catch2::Catch2 
  # Order in the project libraries matters!
  Raa Utility EntryPoint Strangeness Jpsi 
  gsl)
target_include_directories(test_raa_analysis.x 
  PRIVATE 
  ${CMAKE_SOURCE_DIR}/src
  ${CMAKE_SOURCE_DIR}/translation
  ${CMAKE_SOURCE_DIR}/lib/gsl-lite/include) # FIXME Hack, but works

include(ParseAndAddCatchTests)
ParseAndAddCatchTests(test_utility.x)
ParseAndAddCatchTests(test_raa.x)
ParseAndAddCatchTests(test_raa_analysis.x)
