#include "catch2/catch.hpp"

#include "Raa/part1/TTrackCounter.h"
#include "Raa/part1/TrackClassification.h"
#include "Utility/HelperPrimaryTracks.h"
#include "util_track_creation.h"

TEST_CASE("TrackCounter basics", "")
{
  using Raa::TTrackCounter;

  WHEN("Default construction")
  {
    TTrackCounter C;

    AND_THEN("All Counters must be zero")
    {
      REQUIRE(C.NPrimaries() == 0);
      REQUIRE(C.NPrimariesPositive() == 0);
      REQUIRE(C.NSecondaries() == 0);
      REQUIRE(C.PrimaryHighestPt() == nullptr);
    }
    AND_THEN("Moving the Counter to another object does not change counters")
    {
      TTrackCounter C2(std::move(C));

      REQUIRE(C2.NPrimaries() == 0);
      REQUIRE(C2.NPrimariesPositive() == 0);
      REQUIRE(C2.NSecondaries() == 0);
      REQUIRE(C.PrimaryHighestPt() == nullptr);

      TTrackCounter C3;
      C3 = std::move(C2);

      REQUIRE(C3.NPrimaries() == 0);
      REQUIRE(C3.NPrimariesPositive() == 0);
      REQUIRE(C3.NSecondaries() == 0);
      REQUIRE(C.PrimaryHighestPt() == nullptr);
    }
  }
}

TEST_CASE("Count Single Tracks", "")
{
  TEveTrack t_primary = internal::CreatePrimary();
  TEveTrack t_primary_pos = internal::CreatePrimaryCharged();
  TEveTrack t_primary_high_pt = internal::CreatePrimaryHighPt();
  TEveTrack t_sec = internal::CreateSecondary();

  WHEN("Only primary tracks are counted")
  {
    Raa::TTrackCounter c;
    for (int i = 0; i < 10; ++i)
      c.CountPrimary(&t_primary);

    REQUIRE(c.NPrimaries() == 10);
    REQUIRE(c.NPrimariesPositive() == 0);
    REQUIRE(c.NSecondaries() == 0);
    REQUIRE(c.PrimaryHighestPt() == &t_primary);

    AND_THEN("The Counter is reset")
    {
      c.Clear();
      REQUIRE(c.NPrimaries() == 0);
      REQUIRE(c.NPrimariesPositive() == 0);
      REQUIRE(c.NSecondaries() == 0);
      REQUIRE(c.PrimaryHighestPt() == nullptr);
    }
  }

  WHEN("Only primary positive tracks are counted")
  {
    Raa::TTrackCounter c;
    for (int i = 0; i < 10; ++i)
      c.CountPrimaryPositive(&t_primary_pos);

    REQUIRE(c.NPrimaries() == 0);
    REQUIRE(c.NPrimariesPositive() == 10);
    REQUIRE(c.NSecondaries() == 0);
    // This is null, because only counting Primaries will decide on the highest
    // pt track. CountPrimaryPositive is only concerned about the postiveness
    // of the tracks.
    REQUIRE(c.PrimaryHighestPt() == nullptr);

    AND_THEN("The Counter is reset")
    {
      c.Clear();
      REQUIRE(c.NPrimaries() == 0);
      REQUIRE(c.NPrimariesPositive() == 0);
      REQUIRE(c.NSecondaries() == 0);
      REQUIRE(c.PrimaryHighestPt() == nullptr);
    }
  }

  WHEN("Only primary high pt tracks are counted")
  {
    Raa::TTrackCounter c;
    for (int i = 0; i < 10; ++i)
      c.CountPrimary(&t_primary_high_pt);

    REQUIRE(c.NPrimaries() == 10);
    REQUIRE(c.PrimaryHighestPt() == &t_primary_high_pt);
    REQUIRE(c.NPrimariesPositive() == 0);
    REQUIRE(c.NSecondaries() == 0);

    AND_THEN("The Counter is reset")
    {
      c.Clear();
      REQUIRE(c.NPrimaries() == 0);
      REQUIRE(c.NPrimariesPositive() == 0);
      REQUIRE(c.NSecondaries() == 0);
      REQUIRE(c.PrimaryHighestPt() == nullptr);
    }
  }

  WHEN("Only secondary tracks are counted")
  {
    Raa::TTrackCounter c;
    for (int i = 0; i < 10; ++i)
      c.CountSecondary(&t_sec);

    REQUIRE(c.NPrimaries() == 0);
    REQUIRE(c.NPrimariesPositive() == 0);
    REQUIRE(c.NSecondaries() == 10);
    REQUIRE(c.PrimaryHighestPt() == nullptr);

    AND_THEN("The Counter is reset")
    {
      c.Clear();
      REQUIRE(c.NPrimaries() == 0);
      REQUIRE(c.NPrimariesPositive() == 0);
      REQUIRE(c.NSecondaries() == 0);
      REQUIRE(c.PrimaryHighestPt() == nullptr);
    }
  }

  WHEN("All kinds of tracks occur")
  {
    Raa::TTrackCounter c;
    constexpr int NumPrimaries = 5;
    constexpr int NumPrimariesPositive = 3;
    constexpr int NumPrimariesHighPt = 7;
    constexpr int NumSecondaries = 8;

    for (int i = 0; i < NumPrimaries; ++i)
      c.CountPrimary(&t_primary);

    for (int i = 0; i < NumPrimariesPositive; ++i)
      c.CountPrimaryPositive(&t_primary_pos);

    for (int i = 0; i < NumPrimariesHighPt; ++i)
      c.CountPrimary(&t_primary_high_pt);

    for (int i = 0; i < NumSecondaries; ++i)
      c.CountSecondary(&t_sec);

    REQUIRE(c.NPrimaries() == NumPrimaries + NumPrimariesHighPt);
    REQUIRE(c.NPrimariesPositive() == NumPrimariesPositive);
    REQUIRE(c.NSecondaries() == NumSecondaries);
    REQUIRE(c.PrimaryHighestPt() == &t_primary_high_pt);

    AND_THEN("The counter is reset")
    {
      c.Clear();
      REQUIRE(c.NPrimaries() == 0);
      REQUIRE(c.NPrimariesPositive() == 0);
      REQUIRE(c.NSecondaries() == 0);
      REQUIRE(c.PrimaryHighestPt() == nullptr);
    }
  }
}

TEST_CASE("Track counter ranges", "")
{
  TEveTrack t_primary = internal::CreatePrimary();
  TEveTrack t_primary_pos = internal::CreatePrimaryCharged();
  TEveTrack t_primary_high_pt = internal::CreatePrimaryHighPt();
  TEveTrack t_sec = internal::CreateSecondary();
  constexpr int NumPrimaries = 5;
  constexpr int NumPrimariesPositive = 3;
  constexpr int NumPrimariesHighPt = 7;
  constexpr int NumSecondaries = 8;

  Raa::TTrackCounter SingleCounter;

  AND_THEN("Fill the SingleCounter will the tracks")
  {
    for (int i = 0; i < NumPrimaries; ++i)
      SingleCounter.CountPrimary(&t_primary);

    for (int i = 0; i < NumPrimariesPositive; ++i)
      SingleCounter.CountPrimaryPositive(&t_primary_pos);

    for (int i = 0; i < NumPrimariesHighPt; ++i)
      SingleCounter.CountPrimary(&t_primary_high_pt);

    for (int i = 0; i < NumSecondaries; ++i)
      SingleCounter.CountSecondary(&t_sec);
  }

  AND_THEN("Fill a Multicounter with the range of SingleCounter")
  {
    Raa::TTrackCounter RangeCounter;

    RangeCounter.CountPrimaries(SingleCounter.Primaries());
    RangeCounter.CountPrimariesPositive(SingleCounter.Primaries());
    RangeCounter.CountSecondaries(SingleCounter.Secondaries());

    REQUIRE(RangeCounter.Primaries().size() == SingleCounter.Primaries().size());
    REQUIRE(RangeCounter.PrimariesPositive().size() == SingleCounter.PrimariesPositive().size());
    REQUIRE(RangeCounter.Secondaries().size() == SingleCounter.Secondaries().size());
    REQUIRE(RangeCounter.PrimaryHighestPt() == SingleCounter.PrimaryHighestPt());
  }
}
