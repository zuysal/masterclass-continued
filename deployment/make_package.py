#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file automates the package creation for the masterclasses.
A package is an archive that can be uploaded to the web and will run
just fine on linux boxes.

There are currently two supported packages:

1. AppImage
This works on all linux distributions, but can not run Raa-Part2 due to
write restrictions within the AppImage format.
2. Archive
The archive contains root and a compiled version of all the code
of the masterclass. It is mainly tested on Ubuntu 18.04 and might
require more testing. In principle this might require downloading
a different ROOT6 version and compiling on the requested distributions.
The only breaking point would be the shared-libraries on the distribution
like libstdc++ that might differ and not be binary compatible.
"""

import argparse
import multiprocessing
import os
import subprocess
import tarfile
import shutil
import sys
import urllib.request


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f",
        "--force",
        help="Force to overwrite existing files",
        action='store_true')
    parser.add_argument(
        "-k",
        "--keep-files",
        help="Keep generated temporary files (e.g. build-dir)",
        action='store_true')
    parser.add_argument(
        "-o",
        "--out-directory",
        help="Name of the directory to create (e.g. for versioning)",
        default="ALICEMasterClass",
        type=str)
    parser.add_argument(
        "-s",
        "--skip-packing",
        help="Don't package but only build.",
        action='store_true')
    parser.add_argument(
        "-t",
        "--type",
        help="Type of the package to build",
        choices=("archive", "appimage", "vminstall"))
    return parser


def find_repo():
    """
    Find the absolute path to the repository of the MasterClasses and return it.
    """
    old_path = os.getcwd()
    repo_path = ""
    while True:
        current_path = os.getcwd()
        if not os.path.exists(os.path.join(current_path, ".git")):
            os.chdir(os.path.abspath(".."))
        else:
            # found a git repository, is it the MasterClass repo?
            if os.path.exists(
                    os.path.join(current_path, "AliceMasterClass.desktop")):
                repo_path = current_path
                break
            else:
                print(
                    "Found git-repository in '{}', but does not seem to be MasterClass repo. Bailing out!".
                    format(current_path),
                    file=sys.stderr)
                sys.exit(1)

        # we are in the root directory, stop iterating
        if current_path == os.path.abspath(os.sep):
            print(
                "Could not find git-repository, stop at root directory",
                file=sys.stderr)
            sys.exit(1)

    os.chdir(old_path)
    return repo_path


def configure_cmake(build_type: str,directory: str,
                    code_dir: str,
                    root_dir: str,
                    install_prefix: str = "/"):
    """
    Call CMake in :directory: with the necessary flags to build the
    project
    :directory: Directory that shall be used to build
    :code_dir: Path to the toplevel directory of the repository
    :root_dir: Set the base path for ROOT.
    :install_prefix: CMAKE variable necessary for installing
    """
    old_dir = os.getcwd()
    os.chdir(directory)
    if build_type == "vminstall":
        data_prefix = "/home/masterclass/masterclass/share"
    else:
        data_prefix = "share"
    try:
        subprocess.run([
            "cmake", code_dir, "-DROOTSYS={}".format(root_dir),
            "-DCMAKE_INSTALL_PREFIX={}".format(install_prefix),
            "-DMASTERCLASS_DIR={}".format(data_prefix),
            "-DTESTING=OFF", "-DCMAKE_BUILD_TYPE=Release"
        ])
    except Exception as e:
        raise e
    finally:
        os.chdir(old_dir)


def compile_code(directory: str, ncores: int = -1):
    """
    Call make in the specified directory

    :directory: directory to run the command in
    :ncores: number of cores to build, -1 means all available cores.
    """
    old_dir = os.getcwd()
    os.chdir(directory)

    if ncores == -1:
        ncores = multiprocessing.cpu_count()

    assert ncores > 0

    try:
        subprocess.run(["make", "-j{}".format(ncores)])
    except Exception as e:
        raise e
    finally:
        os.chdir(old_dir)


def install_code(build_dir: str, destdir: str):
    """
    Call 'make install' with the appropriate destination in :build_dir:

    :build_dir: Directory that has been build before
    :destdir: Destination to install to
    """
    old_dir = os.getcwd()
    os.chdir(build_dir)
    try:
        subprocess.run(["make", "install", "DESTDIR={}".format(destdir)])
    except Exception as e:
        raise e
    finally:
        os.chdir(old_dir)


def build_code(build_dir_postfix: str, root_dir: str, install_dir: str,
               force_overwrite: bool, build_type: str):
    """
    Build the source code for a release.
    :build_dir_postfix: Directory to build in. 'build_' will prepended.
    :root_dir: Directory containing the installation of ROOT
    :install_dir: DESTDIR=<install_dir> in 'make install DESTDIR=' call
    :force_overwrite: Force to overwrite existing files.
    """
    build_dir = os.path.abspath("build_{}".format(build_dir_postfix))

    if os.path.exists(build_dir) and not force_overwrite:
        print(
            "Requested build directory '{}' already exists!".format(build_dir),
            file=sys.stderr)
        sys.exit(1)

    if not os.path.exists(build_dir):
        os.mkdir(build_dir)

    try:
        configure_cmake(build_type, build_dir, find_repo(), root_dir)
    except subprocess.CalledProcessError:
        print(
            "Could not configure directory '{}' with cmake!".format(build_dir),
            file=sys.stderr)
        sys.exit(1)

    try:
        compile_code(build_dir)
    except subprocess.CalledProcessError:
        print(
            "Could not compile code in directory '{}'!".format(build_dir),
            file=sys.stderr)
        sys.exit(1)

    try:
        install_code(build_dir, install_dir)
    except subprocess.CalledProcessError:
        print(
            "Could not install code in directory '{}' to destination '{}'!".
            format(build_dir, install_dir),
            file=sys.stderr)
        sys.exit(1)

    return build_dir


def download_root(version: str, target: str, gcc_version: str, out_file: str):
    """
    Download the ROOT binaries from the official site.

    :version: describe the version, e.g. "6.14.04"
    :target: target that ROOT supports, e.g. "ubuntu18"
    :out_file: file name ROOT shall be saved to.
    :returns: directory name of the the extract ROOT archive
    """
    url = "https://root.cern.ch/download/root_v{version}.Linux-{platform}-x86_64-{gcc}.tar.gz".format(version=version, platform=target, gcc=gcc_version)

    out_dir = out_file.rstrip(".tar.gz")

    if not os.path.exists(out_dir):
        with urllib.request.urlopen(url) as response, open(out_file,
                                                           'wb') as out_f:
            shutil.copyfileobj(response, out_f)

        root_archive = tarfile.open(out_file)

        root_archive.extractall(out_dir)
        root_archive.close()

    return os.path.abspath(os.path.join(out_dir, "root/"))


def main():
    """
    Implements the program logic of compiling, installing and archiving
    the masterclass to the requested format.
    """
    args = create_parser().parse_args()

    if args.type == "appimage":
        target_name = "{}.AppDir".format(args.out_directory)
    elif args.type == "archive" or args.type == "vminstall":
        target_name = "{}".format(args.out_directory)
    else:
        print("Wrong type supplied!", file=sys.stderr)
        sys.exit(1)

    cache_path = os.path.join(find_repo(), "deployment/download_cache/")
    root_ver_f = os.path.join(cache_path, "ROOT6_16_00_ubuntu_18.tar.gz")
    install_dir = os.path.abspath(os.path.join(os.getcwd(), target_name))

    # Download root, will use the cached version if existing
    ROOT_dir = download_root("6.18.04", "ubuntu18", "gcc7.4", root_ver_f)

    # Compile the code in release mode and install into the install_dir
    build_dir = build_code("release_ubuntu18", ROOT_dir, install_dir,
                           args.force, args.type)
    if not args.keep_files:
        shutil.rmtree(build_dir)

    # TODO: fix the paths!!!!

    # subprocess.run(['rm', '-rf', os.path.join(install_dir, 'headers')])
    # shutil.copytree(find_repo() + '/src', os.path.join(install_dir, 'headers'))
    # subprocess.run(["find", os.path.join(install_dir, 'headers'), '-name', '*.cxx', '-type', 'f', '-delete'])
    # subprocess.run(["find", os.path.join(install_dir, 'headers'), '-name', '*.txt', '-type', 'f', '-delete'])
    # subprocess.run(["find", os.path.join(install_dir, 'headers'), '-name', '*.cmake', '-type', 'f', '-delete'])
    # subprocess.run(["find", os.path.join(install_dir, 'headers'), '-name', '*.md', '-type', 'f', '-delete'])
    # subprocess.run(['cp', '-r', find_repo() + '/translation/.', os.path.join(install_dir, 'headers/')])
    # subprocess.run(['cp', '-r', find_repo() + '/lib/gsl-lite/include/.', os.path.join(install_dir, 'headers/')])

    if args.type == "vminstall":

        subprocess.run(['rm', os.path.join(install_dir, 'AppRun')])

        subprocess.run(['sudo', 'rsync', '-av', install_dir + '/', '/mnt/vdi/home/masterclass/masterclass'])
        subprocess.run(['sudo', 'cp', '-r', os.path.join(install_dir, '.'), '/mnt/vdi/home/masterclass/masterclass'])
        subprocess.run(['sudo', 'cp', '-r', '/home/user/masterclass/deployment/startMasterClass.sh', '/mnt/vdi/home/masterclass/masterclass/'])
        subprocess.run(['sudo', 'mv', '-n', '/mnt/vdi/home/masterclass/masterclass/AliceMasterClass.desktop', '/mnt/vdi/home/masterclass/Desktop/'])
        subprocess.run(['sudo', 'chmod', 'o+x', '/mnt/vdi/home/masterclass/Desktop/AliceMasterClass.desktop'])
    else:
        # make AppRun executable
        apprun_path = os.path.join(install_dir, "AppRun")
        subprocess.run(["chmod", "+x", "{}".format(apprun_path)])

        # copy in ROOT for convenience
        if not os.path.exists(os.path.join(install_dir, "root")):
            shutil.copytree(ROOT_dir, os.path.join(install_dir, "root"))

    if args.skip_packing:
        return

    # create tar.gz if making an normal archive
    if args.type == "archive":
        archive_name = os.path.abspath(install_dir + ".tar.gz")
        if os.path.exists(archive_name) and args.force:
            os.remove(archive_name)
        elif os.path.exists(archive_name):
            print(
                "Target archive '{}' already exists!".format(archive_name),
                file=sys.stderr)
            sys.exit(1)

        print("Packing archive {}".format(archive_name))
        with tarfile.open(archive_name, "w:gz") as archive:
            print(install_dir)
            archive.add(install_dir, arcname=os.path.basename(install_dir))

    elif args.type == "appimage":
        # download the AppImage tool
        AppImageURL = "https://github.com/probonopd/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage"
        AppImageToolName = "appimagetool-x86_64.AppImage"
        AppImageToolPath = os.path.join(
            find_repo(),
            "deployment/download_cache/{}".format(AppImageToolName))

        if not os.path.exists(AppImageToolName):
            with urllib.request.urlopen(AppImageURL) as response, open(
                    AppImageToolPath, 'wb') as out_f:
                shutil.copyfileobj(response, out_f)
            subprocess.run(["chmod", "+x", AppImageToolPath])

        subprocess.run([AppImageToolPath, install_dir])


if __name__ == "__main__":
    main()
