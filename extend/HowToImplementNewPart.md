Adding a new Masterclass
========================

This document describes the central parts that connect all pieces and are
the first places where additions have to be made.

It is of course always helpful to see how the already implemented (most work
went into `Raa`) do their task.

Create a new subdirectory for everything
----------------------------------------

It is always good practice to organize code to have the file structure follow
the logical structure of your code.
That means the first step would be to create necessary subdirectories for
the new class in `data/`, `doc/`, `src/`, `test/`, `translation/`, `vsdData`.

Implementation-wise `src/` and `test/` are the most important for the coding
work.

Create the entrypoint and registration of the MasterClass
---------------------------------------------------------

The entrypoint of a MasterClass is just a function that will be called when
it is selected. Control flow is handed to that function and from there
you can do whatever you want. You should still use the provided utility
to reduce code-duplication and so on ;)

```
$ cd src/MyNewClass
$ touch ClassContent.h # this contains a list of exercises to register
$ touch TMyClass.h # this contains the entry points to the exercises
> now there is at least one file in your class
```

In the new file you add content similar to this (which is one example how
`J/Psi` looks/looked like):

```
// content of ClassContent.h


#ifndef CLASSCONTENT_H_D5CJU98B
#define CLASSCONTENT_H_D5CJU98B

#include "Jpsi/TJpsiClass.h"
#include "Utility/AbstractMasterClassContent.h"
#include "Utility/ContentTranslation.h"
#include "Utility/StdFixes.h" // IWYU pragma: keep
#include "Utility/Utilities.h" // IWYU pragma: keep

namespace Jpsi
{
struct TContentEnglish : Utility::TContentTranslation {
  TContentEnglish(Utility::ESupportedLanguages lang = Utility::English)
    : TContentTranslation(lang)
  {
    std::cerr << "Registering English Jpsi Class Content\n";
#include "Jpsi/keys_class_trans.txt.en"
  }
};

inline std::unique_ptr<Utility::TContentTranslation> LanguageFactory(
  Utility::ESupportedLanguages lang)
{
  switch (lang) {
    case Utility::English:
      return std_fix::make_unique<TContentEnglish>();
    case Utility::German:
      return std_fix::make_unique<TContentGerman>();
    case Utility::NLanguages:
      break;
  }
  throw std::runtime_error("Requested Translation for Jpsi does not exist");
}

struct TClassContent : Utility::TAbstractMasterClassContent {
  TClassContent(Utility::ESupportedLanguages lang)
    : TAbstractMasterClassContent(LanguageFactory(lang))
  {
    /// Here the exercise is added and registered.
    AddExercise(std_fix::make_unique<TJpsiClass>());
  }
};
} // namespace Jpsi

#endif /* end of include guard: CLASSCONTENT_H_D5CJU98B */
```

and

```
// Content of  TMyClass.h


#ifndef FULLCLASS_H_ZQNNWRO8
#define FULLCLASS_H_ZQNNWRO8

#include "Utility/AbstractMasterClassContent.h"

namespace Jpsi
{
class TJpsiClass : public Utility::TAbstractExercise
{
 public:
  TJpsiClass()
    : TAbstractExercise("Jpsi Analysis")
  {
  }

  void RunExercise(Bool_t test = false) override
  {
    std::cout << "I will start a new exercise\n";
  }
};
} // namespace Jpsi

#endif /* end of include guard: FULLCLASS_H_ZQNNWRO8 */
```

The boilerplate to create the `class` and the registration are necessary
to have a common interface to add new master classes.

This new entrypoint is then added to the GUI to choose between the classes
in `src/EntryPoint/MasterClassRegistry.cxx`

```
#include "MasterClassRegistry.h"

#include <iostream>

#include <TString.h>
#include "Raa/ClassContent.h"
#include "Strangeness/ClassContent.h"
#include "Jpsi/ClassContent.h"

namespace EntryPoint
{
void TMasterClassRegistry::ConstructClassContents(Utility::ESupportedLanguages lang)
{
  std::cerr << "Changing language for all register master classes to "
            << Utility::GetLanguageName(lang) << "\n";
  fClasses.clear();
  fClasses.emplace_back(new Raa::TClassContent(lang));
  fClasses.emplace_back(new Strangeness::TClassContent(lang));
  fClasses.emplace_back(new Jpsi::TClassContent(lang));

  // !!!!!! ADD HERE !!!!!!
  fClasses.emplace_back(new MyClass::TClassContent(lang));
}
} // namespace EntryPoint
```

You need to provide some text snippets to display as well, orient yourself on
the existing MasterClasses.

Once you compile the code and execute the MasterClass from the start window
you should see `I will start a new exercise` written to the terminal!

Event Display based Masterclasses
---------------------------------

Right now every master class has the first part that uses the event display
to show some collision data.

The central classes to check out are:

- `src/Utility/VSDReader.{h,cxx}`
- `src/Utility/TEventAnalyseGUI.{h,cxx}`
- `src/Utility/INavigation.{h,cxx}`

They provide hooks and extension points to build your own master class.
Please note, that

- `src/Utility/Renderables.{h,cxx}`
- `src/Utility/NavigationElements.{h,cxx}`

provide `LEGO` building blocks for navigation and renderable elements.
Orient yourself on the existing master classes, e.g. `Raa`.
