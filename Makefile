#
# Makefile 
#
PROJECT		:= ALICE_MasterClass
VERSION		:= 0.2
DOCKER_IMAGE	:= gitlab-registry.cern.ch/cholm/alice-masterclasses
DOCKER_X11	:= -i -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix
DOCKER_WEB	:= -p 6080:6080

DOCS		:= README.md
MISC		:= AliceMasterClass.desktop Makefile
SRCS		:= MasterClass.C

clean::
	rm -f  *~ *_C.* *.pcm
	rm -f  *.tar
	rm -rf html doc/
	rm -rf ALICE_Raa-* ALICE_Strangeness-* ALICE_MasterClass-*
	rm -f  RaaAnalyse.C
	rm -f  Result_*_*.pdf
	rm -f  RaaOutput.root
	rm -f  RaaResult.root
	rm -f  strangeness.pdf strangeness.root
	rm -f  unnamed.pdf unnamed.root
	rm -f  *.o

distdir nodatadistdir::	$(DOCS) $(MISC) $(SRCS)
	mkdir -p $(DISTDIR)/
	$(foreach f, $(DOCS) $(MISC) $(SRCS), cp $(f) $(DISTDIR)/;)

include Base/Makefile
ifeq ($(MAKECMDGOALS:raadist%=raadist), raadist)
SUBS		:= Raa
DISTDIR		:= ALICE_Raa-$(VERSION)
else ifeq ($(MAKECMDGOALS:strdist%=strdist), strdist)
SUBS		:= Strangeness
DISTDIR		:= ALICE_Strangeness-$(VERSION)
else
SUBS		:= Raa Strangeness
DISTDIR		:= $(PROJECT)-$(VERSION)
endif
include $(SUBS:%=%/Makefile)

datadistdir:	DISTDIR:=$(PROJECT)-data

raadist: dist
strdist: dist
raadistdir: distdir
strdistdir: distdir

raadistdir_nodata:	nodatadistdir
strdistdir_nodata:	nodatadistdir
distdir_nodata:		nodatadistdir

dist:	distdir
	tar -cvf $(DISTDIR).tar $(DISTDIR)
	rm -rf $(DISTDIR)


%/Documentation.pdf:%/Documentation.tex
	(cd $(dir $<) && pdflatex $(notdir $<))
	(cd $(dir $<) && pdflatex $(notdir $<))
	(cd $(dir $<) && pdflatex $(notdir $<))


DOC_SRCS	:= $(DOCS) $(BASE_SRCS) $(RAA_SRCS) $(STR_SRCS)	\
		   Base/doc/Implementation.md			\
		   Raa/Changes.md				\
		   Strangeness/Changes.md

html/index.html:Base/doc/Doxyfile $(DOC_SRCS)
	doxygen $<

doc:	html/index.html

.%_image:	.docker/Dockerfile.% $(DOC_SRCS)
	docker build -t $(DOCKER_IMAGE)$(TAG) -f $< .
	touch $@

.full_image:	TAG=
.base_image:	TAG=:nodata
.webbase_image:	TAG=:webnodata
.webfull_image:	TAG=:web

.full_image:	.base_image
.webbase_image:	.base_image
.webfull_image:	.webbase_image


base_image:	.base_image
full_image:	.full_image
webbase_image:	.webbase_image
webfull_image:	.webfull_image

images:		base_image full_image webbase_image webfull_image

run-%-image: .%_image
	-docker stop $*
	-docker rm $*
	docker run --name $* $(ARGS) $(DOCKER_IMAGE)$(TAG)

run-base-image: 	ARGS:=$(DOCKER_X11)
run-base-image:		TAG:=:nodata
run-full-image:		ARGS:=$(DOCKER_X11) 
run-webbase-image:	ARGS:=$(DOCKER_WEB)
run-webbase-image:	TAG:=:webnodata
run-webfull-image:	ARGS:=$(DOCKER_WEB)
run-webfull-image:	TAG:=:web


#
#
# 

