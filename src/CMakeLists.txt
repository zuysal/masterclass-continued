# -=Common functionality=-
add_subdirectory(EntryPoint)
add_subdirectory(Utility)

# -=Actual Masterclasses=-
add_subdirectory(Strangeness)
add_subdirectory(Raa)
add_subdirectory(Jpsi)

get_directory_property(incdirs INCLUDE_DIRECTORIES)
#generate_dictionary(MasterClass "MasterClassLinkDef.h" "MasterClass.h" "${incdirs}")

add_executable(MasterClass.x MasterClass.cxx)

#add_custom_command(
#        TARGET MasterClass.x
#        PRE_BUILD
#        COMMAND ${CMAKE_COMMAND}
#        -DLANGUAGE:STRING="C"
#        -DHEADER_DIR:PATH="${CMAKE_SOURCE_DIR}/src"
#        -DCACHE_DIR:PATH="${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}"
#        -P "$(CMAKE_SOURCE_DIR)/src/compilation_number.cmake"
#)

target_link_libraries(MasterClass.x
  PUBLIC
  ROOT::Core
  EntryPoint
  Utility
  Strangeness
  Raa
  Jpsi
  PRIVATE
  gsl
  )

# Install the executable
install(TARGETS MasterClass.x RUNTIME DESTINATION bin)
