/**
 * @file   MasterClass.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 19:53:56 2017
 *
 * @brief  Main entry point for the ALICE master classes
 *
 * @ingroup alice_masterclass
 */

#include "EntryPoint/ClassSelector.h"

int main(int argc, char** argv)
{
  std::cout << "ROOT_INCLUDE_PATH " << gSystem->Getenv("ROOT_INCLUDE_PATH") << std::endl;
  TApplication App("MasterClass", &argc, argv);

  EntryPoint::ClassSelector SelectorGUI(Utility::English);
  SelectorGUI.SetupGUI();

  App.Run();
  return EXIT_SUCCESS;
}
