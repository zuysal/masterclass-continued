/**
 * @file   VSDReader.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 10:00:52 2017
 *
 * @brief  The manager of events and the event display
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 */
#ifndef ALICEVSDREADER_C
#define ALICEVSDREADER_C

#include <RQ_OBJECT.h>
#include <Rtypes.h>
#include <RtypesCore.h>
#include <TDatabasePDG.h>
#include <TEveEventManager.h>
#include <TEveGeoShape.h>
#include <TEveGeoShapeExtract.h>
#include <TEveManager.h>
#include <TEvePointSet.h>
#include <TEveSelection.h>
#include <TEveTrack.h>
#include <TEveTrackPropagator.h>
#include <TEveVSD.h>
#include <TEveVSDStructs.h>
#include <TEveVector.h>
#include <TEveViewer.h>
#include <TEveWindowManager.h>
#include <TFile.h>
#include <TGMsgBox.h>
#include <TGStatusBar.h>
#include <TGTab.h>
#include <TKey.h>
#include <TObjArray.h>
#include <TPRegexp.h>
#include <TQObject.h>
#include <TString.h>
#include <TSystem.h>
#include <functional>
#include <gsl/gsl>
#include <memory>

#include "Utility/Details.h"
#include "Utility/GUITranslation.h"
#include "Utility/HelperPrimaryTracks.h"
#include "Utility/Renderables.h"

class TBuffer;
class TClass;
class TEveElement;
class TEvePointSet;
class TEveTrackList;
class TEveVSD;
class TFile;
class TMemberInspector;

namespace Utility
{
class EventDisplay;
class TEventVisualization;
struct TGUIEnglish;

/**
 * @defgroup alice_masterclass_base_eventdisplay Base event display classes.
 *
 * @ingroup  alice_masterclass_base
 */
//====================================================================
/**
 * Reader of VSD data to show in the event display. Also works as the
 * manager of the data and provides the call backs to the exercises
 *
 * @image html Raa/doc/EventDisplay.png
 *
 * @ingroup alice_masterclass_base_eventdisplay
 */
class VSDReader : public TQObject
{
  RQ_OBJECT("Utility::VSDReader")

 public:
  VSDReader();
  ~VSDReader() /*override*/;

  /// Build the GUI.
  void Setup(gsl::not_null<TFile*> DataFile);

  /// Load the data for the Event with idx 'ev' in the dataset.
  Bool_t LoadEvent(Int_t ev);

  /// Return the number of Events that are in this data set.
  Int_t GetNumberOfEvents() { return fEvDirKeys->GetEntriesFast(); }

  /// Return a handle to the internal VSD for Visualization.
  TEveVSD* GetVSD() const noexcept { return fVSD.get(); }

  /**
   * @{
   * @name Load specific Data, involves calculations as well.
   */

  /// Load the normal tracks of particles.
  void LoadTracks(Double_t MagneticField);
  /// Load V0s that are a decay?
  void LoadV0s(Double_t MagneticField);
  /// Load cascades coming from a particle decay (Strangeness stuff)
  void LoadCascades(Double_t MagneticField);
  /* @} */

  virtual Bool_t handleTrack(TEveTrack*);

  void SetVisualizePrimaries(Bool_t enable) {fVisualizeOnlyPrimaries = enable;}

  /// @{
  /// @name Signal that are emitted whenever data changes

  // Signals that occur when loading the Trackdata.

  void ClearingTracks();        //*SIGNAL*
  void LoadedTrack(TEveTrack*, Bool_t); //*SIGNAL*
  void FinishedTracks();        //*SIGNAL*

  // Signals that occur while loading the V0 data.

  void ClearingV0s();                //*SIGNAL*
  void LoadedV0Negative(TEveTrack*); //*SIGNAL*
  void LoadedV0Positive(TEveTrack*); //*SIGNAL*
  void LoadedV0();                   //*SIGNAL*
  void FinishedV0s();                //*SIGNAL*

  // Signals that occur while loading cascades.

  void ClearingCascades();                //*SIGNAL*
  void LoadedCascadeNegative(TEveTrack*); //*SIGNAL*
  void LoadedCascadePositive(TEveTrack*); //*SIGNAL*
  void LoadedCascadeBachelor(TEveTrack*); //*SIGNAL*
  void LoadedCascade();                   //*SIGNAL*
  void FinishedCascades();                //*SIGNAL*

  /// @}

 private:
  Bool_t LoadTreeCurrentEvent(Int_t EventIdx);

  void DropTracks();
  void DropV0s();
  void DropCascades();

  /// Unload the current event
  void DropEvent();

  /* @} */

  using TFileRAII = std::unique_ptr<TFile, std::function<void(TFile*)>>;
  TFileRAII fDataFile; ///< Must be a member, because closing the file invalidates pointers to the
                       ///< EventDisplay data.

  std::unique_ptr<TObjArray> fEvDirKeys;
  std::unique_ptr<TEveVSD> fVSD;

  const Int_t fMaxR{ 600 }; ///< Parameter to control the Radius of data to load.

  TEveTrackList* fTracks{ nullptr };

  TEveTrackList* fV0Neg{ nullptr };
  TEveTrackList* fV0Pos{ nullptr };
  TEveTrackList* fCascadeNeg{ nullptr };
  TEveTrackList* fCascadePos{ nullptr };
  TEveTrackList* fCascadeBachelor{ nullptr };

  Bool_t fVisualizeOnlyPrimaries;

  // All points the scene can have
  TEvePointSet* fV0s{ nullptr };
  TEvePointSet* fCascadeV0s{ nullptr };

  // This function creates the renderables and needs the locations of the
  // in this class stored data for that.
  friend void ConnectDataAndRenderables(VSDReader* /*Data*/, TEventVisualization& /*Vis*/);

  ClassDef(VSDReader, 0);
};
} // namespace Utility

#endif
