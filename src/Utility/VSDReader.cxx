#include "Utility/VSDReader.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TDatabasePDG.h>
#include <TDirectory.h>
#include <TError.h>
#include <TEveBrowser.h>
#include <TEveElement.h>
#include <TEveEventManager.h>
#include <TEveLine.h>
#include <TEveManager.h>
#include <TEvePointSet.h>
#include <TEveTrack.h>
#include <TEveTrackPropagator.h>
#include <TEveTreeTools.h>
#include <TEveVSD.h>
#include <TEveVSDStructs.h>
#include <TEveViewer.h>
#include <TFile.h>
#include <TGClient.h>
#include <TGLCamera.h>
#include <TGLViewer.h>
#include <TKey.h>
#include <TList.h>
#include <TMath.h>
#include <TMathBase.h>
#include <TObjArray.h>
#include <TObject.h>
#include <TPRegexp.h>
#include <TParticlePDG.h>
#include <TString.h>
#include <TSystem.h>
#include <TTree.h>
#include <iostream>
#include <stdexcept>
#include <utility>

#include "Utility/Details.h"
#include "Utility/ERenderElements.h"
#include "Utility/EventDisplay.h"
#include "Utility/StdFixes.h"

namespace Utility
{
VSDReader::VSDReader()
  : fVSD(std_fix::make_unique<TEveVSD>())
{
  // --- Set some defaults -----------------------------------------
  //TFile::SetCacheFileDir(".");
  TEveVSD::DisableTObjectStreamersForVSDStruct();
}

VSDReader::~VSDReader() { DropEvent(); }

void VSDReader::Setup(gsl::not_null<TFile*> DataFile)
{
  // Printf("Reading data from the file: %s", dataFileName);
  fDataFile = TFileRAII(DataFile, [](TFile* f) { f->Close(); });

  // Fetch the names of all events
  fEvDirKeys = std_fix::make_unique<TObjArray>();
  TPMERegexp name_re("Event\\d+");
  TObjLink* lnk = fDataFile->GetListOfKeys()->FirstLink();
  while (lnk != nullptr) {
    std::cerr << "Got Events\n";
    if (name_re.Match(lnk->GetObject()->GetName()) != 0) {
      fEvDirKeys->Add(lnk->GetObject());
    }
    lnk = lnk->Next();
  }
}

Bool_t VSDReader::LoadTreeCurrentEvent(Int_t EventIdx)
{
  // Because each event is an directory we have to set the directory for TEveVSD
  // to the new Event directory.
  std::cerr << "Begin Directory tour\n";

  auto* KeyObj = fEvDirKeys->At(EventIdx);
  std::cerr << "New directory ptr " << KeyObj << std::endl;
  auto* KeyCast = dynamic_cast<TKey*>(KeyObj);
  std::cerr << "New directory ptr " << KeyCast << std::endl;
  auto* DirObj = KeyCast->ReadObj();
  std::cerr << "New directory obj ptr " << DirObj << std::endl;
  auto* Directory = dynamic_cast<TDirectory*>(DirObj);
  std::cerr << "New directory obj ptr " << Directory << std::endl;

  fVSD->SetDirectory(Directory);

  std::cerr << "Load Trees from" << Directory->GetPath() << std::endl;
  // Attach event data from current directory.
  fVSD->LoadTrees();
  std::cerr << "Set addresses\n";
  fVSD->SetBranchAddresses();
  if(fVSD->fTreeR)
      return kTRUE;

  std::cerr << "ERROR! Tree loading failed! Skipping..." << std::endl;

  return kFALSE;
}

namespace
{
void ClearTrackList(TEveTrackList* l)
{
  if (l != nullptr)
    l->DestroyElements();
}

void ClearPointSet(TEvePointSet* s)
{
  if (s != nullptr)
    s->Reset();
}
} // namespace

void VSDReader::DropTracks()
{
  std::cerr << "Clear track lists\n";
  ClearTrackList(fTracks);
  delete fTracks;
  fTracks = nullptr;
}

void VSDReader::DropV0s()
{
  std::cerr << "Clear V0s\n";
  ClearTrackList(fV0Neg);
  delete fV0Neg;
  fV0Neg = nullptr;
  ClearTrackList(fV0Pos);
  delete fV0Pos;
  fV0Pos = nullptr;
  ClearPointSet(fV0s);
  delete fV0s;
  fV0s = nullptr;
}

void VSDReader::DropCascades()
{
  std::cerr << "Clear Cascades\n";
  ClearTrackList(fCascadeNeg);
  delete fCascadeNeg;
  fCascadeNeg = nullptr;
  ClearTrackList(fCascadePos);
  delete fCascadePos;
  fCascadePos = nullptr;
  ClearTrackList(fCascadeBachelor);
  delete fCascadeBachelor;
  fCascadeBachelor = nullptr;
  ClearPointSet(fCascadeV0s);
  delete fCascadeV0s;
  fCascadeV0s = nullptr;
}

void VSDReader::DropEvent()
{
  DropTracks();
  DropV0s();
  DropCascades();

  // Remove all the data that was loaded from disk.
  std::cerr << "Delete Trees\n";
  fVSD->DeleteTrees();
}

Bool_t VSDReader::LoadEvent(Int_t ev)
{
  if (ev < 0 || ev > GetNumberOfEvents())
    throw std::runtime_error("Event IDX out of bounds!");

  std::cerr << "Dropping Events\n";
  DropEvent();

  std::cerr << "Going to Event # " << ev << "\n";
  return LoadTreeCurrentEvent(ev);
}

void VSDReader::LoadTracks(Double_t MagneticField)
{
  Expects(fVSD->fTreeR != nullptr);

  using Utility::ERenderElements;
  using Utility::RenderElementToString;

  Emit("ClearingTracks()");
  DropTracks();

  std::cerr << "Loading Tracks\n";
  fTracks = new TEveTrackList;
  fTracks->IncDenyDestroy();

  // Set up properties to calculate the track path.
  TEveTrackPropagator* trkProp = fTracks->GetPropagator();
  trkProp->SetMagField(MagneticField);
  trkProp->SetMaxR(fMaxR); // R[cm]

  Int_t nTracks = fVSD->fTreeR->GetEntries();
  std::cerr << "nTracks = " << nTracks << "\n";

  for (Int_t n = 0; n < nTracks; ++n)
  {
    fVSD->fTreeR->GetEntry(n);
    auto *track = new TEveTrack(&fVSD->fR, trkProp);
    track->IncDenyDestroy();  //If event loading takes too long, the ROOT gcollector will start deleting first tracks...

    track->SetName(RenderElementToString(ERenderElements::kESDTrack));
    track->SetAttLineAttMarker(fTracks);
    track->SetLineColor(9);

    Bool_t include = handleTrack(track);

    Long_t args[2];
    args[0] = (Long_t)track;
    args[1] = (Long_t)include;

    Emit("LoadedTrack(TEveTrack*,Bool_t)", args);

    if(include)
    {
      fTracks->AddElement(track);
    }
  }

  // Recalculate all the track information that is changed by adding new tracks.
  fTracks->MakeTracks();
  Emit("FinishedTracks()");
}

void VSDReader::LoadV0s(Double_t MagneticField)
{
  Expects(fVSD->fTreeR != nullptr);
  Expects(fVSD->fTreeV0 != nullptr);

  using Utility::ERenderElements;
  using Utility::RenderElementToString;

  Emit("ClearingV0s()");
  DropV0s();

  std::cerr << "Loading V0s\n";
  fV0Neg = new TEveTrackList;
  fV0Neg->IncDenyDestroy();
  fV0Pos = new TEveTrackList;
  fV0Pos->IncDenyDestroy();
  fV0s = new TEvePointSet;
  fV0s->IncDenyDestroy();

  TEveTrackPropagator* prop = fV0Neg->GetPropagator();
  prop->SetMagField(MagneticField);
  prop->SetMaxR(fMaxR); // R[cm]

  Int_t nV0s = fVSD->fTreeV0->GetEntries();

  fVSD->fTreeR->GetEntry(0);

  TEvePointSelector ss(fVSD->fTreeV0, fV0s, "fVCa.fX:fVCa.fY:fVCa.fZ");
  ss.Select();
  fV0s->SetTitle("V0_CA_Points");

  Int_t nTracks = fVSD->fR.fIndex;

  for (Int_t n = nTracks; n < nV0s; n++) {
    fVSD->fTreeV0->GetEntry(n);

    Int_t pdg = fVSD->fV0.fPdg;
    Int_t pdgN = 0;
    Int_t pdgP = 0;
    switch (pdg) {
      case 310:
        pdgN = -211;
        pdgP = +211;
        break;
      case 3122:
        pdgN = -211;
        pdgP = +2212;
        break;
      case -3122:
        pdgN = -2212;
        pdgP = +211;
        break;
      default:
        // FIXME What is this case? It can occur.
        break;
    }

    TDatabasePDG* pdgDb = TDatabasePDG::Instance();
    Double_t momentum = 0;
    Double_t mass = 0;

    TEveRecTrack rcNeg;
    rcNeg.fP.Set(fVSD->fV0.fPNeg);
    rcNeg.fV.Set(fVSD->fV0.fVNeg);
    rcNeg.fStatus = fVSD->fV0.fStatus;
    rcNeg.fLabel = fVSD->fV0.fDLabel[0];
    rcNeg.fSign = -1;
    mass = pdgDb->GetParticle(pdgN)->Mass();
    momentum = fVSD->fV0.fPNeg.Mag();
    rcNeg.fBeta =
      momentum / TMath::Sqrt(momentum * momentum + TMath::C() * TMath::C() * mass * mass);

    auto* trackN = new TEveTrack(&rcNeg, prop);
    trackN->SetName(RenderElementToString(ERenderElements::kV0TrackNegative));
    trackN->SetAttLineAttMarker(fV0Neg);
    trackN->SetPdg(pdgN);
    trackN->SetUniqueID(n);
    Double_t pxNeg = trackN->GetMomentum().fX;
    Double_t pyNeg = trackN->GetMomentum().fY;
    Double_t pzNeg = trackN->GetMomentum().fZ;

    TEveRecTrack rcPos;
    rcPos.fP.Set(fVSD->fV0.fPPos);
    rcPos.fV.Set(fVSD->fV0.fVPos);
    rcPos.fStatus = fVSD->fV0.fStatus;
    rcPos.fLabel = fVSD->fV0.fDLabel[1];
    rcPos.fSign = 1;
    mass = pdgDb->GetParticle(pdgP)->Mass();
    momentum = fVSD->fV0.fPPos.Mag();
    rcPos.fBeta =
      momentum / TMath::Sqrt(momentum * momentum + TMath::C() * TMath::C() * mass * mass);
    auto* trackP = new TEveTrack(&rcPos, prop);
    trackP->SetName(RenderElementToString(ERenderElements::kV0TrackPositive));
    trackP->SetAttLineAttMarker(fV0Pos);
    trackP->SetPdg(pdgP);
    trackP->SetUniqueID(n);

    Double_t pxPos = trackP->GetMomentum().fX;
    Double_t pyPos = trackP->GetMomentum().fY;
    Double_t pzPos = trackP->GetMomentum().fZ;

    Double_t pcaX = fVSD->fV0.fVCa.fX;
    Double_t pcaY = fVSD->fV0.fVCa.fY;
    Double_t pcaZ = fVSD->fV0.fVCa.fZ;

    momentum = TMath::Sqrt((pxNeg + pxPos) * (pxNeg + pxPos) + (pyNeg + pyPos) * (pyNeg + pyPos) +
                           (pyNeg + pyPos) * (pyNeg + pyPos));
    Double_t dir[3] = { (pxNeg + pxPos) / momentum, (pyNeg + pyPos) / momentum,
                        (pzNeg + pzPos) / momentum };

    auto* pl = new TEveLine(RenderElementToString(ERenderElements::kV0PointingLine));
    pl->SetPoint(0, 0.0, pcaY - pcaX * dir[1] / dir[0], pcaZ - pcaX * dir[2] / dir[0]);
    pl->SetPoint(1, pcaX, pcaY, pcaZ);
    pl->SetLineColor(kSpring + 6);
    pl->SetLineWidth(2);
    pl->SetLineStyle(2);
    pl->SetUniqueID(n);
    pl->AddElement(trackN);
    pl->AddElement(trackP);

    fV0Pos->AddElement(trackP);
    fV0Neg->AddElement(trackN);

    Emit("LoadedV0Negative(TEveTrack*)", trackN);
    Emit("LoadedV0Positive(TEveTrack*)", trackP);
    Emit("LoadedV0()");
  }

  fV0Pos->MakeTracks();
  fV0Neg->MakeTracks();

  Emit("FinishedV0s()");
}

void VSDReader::LoadCascades(Double_t MagneticField)
{
  Expects(fVSD->fTreeV0 != nullptr);
  Expects(fVSD->fTreeR != nullptr);

  using Utility::ERenderElements;
  using Utility::RenderElementToString;

  Emit("ClearingCascades()");
  DropCascades();

  std::cerr << "Loading Cascades\n";

  fCascadeNeg = new TEveTrackList;
  fCascadeNeg->IncDenyDestroy();
  fCascadePos = new TEveTrackList;
  fCascadePos->IncDenyDestroy();
  fCascadeBachelor = new TEveTrackList;
  fCascadeBachelor->IncDenyDestroy();
  fCascadeV0s = new TEvePointSet;
  fCascadeV0s->IncDenyDestroy();

  TEveTrackPropagator* prop = fCascadeNeg->GetPropagator();
  prop->SetMagField(MagneticField);
  prop->SetMaxR(fMaxR); // R[cm]

  TEveTrackPropagator* propB = fCascadeBachelor->GetPropagator();
  propB->SetMagField(MagneticField);
  propB->SetMaxR(fMaxR); // R[cm]

  fVSD->fTreeR->GetEntry(0);
  Int_t nTracks = fVSD->fR.fIndex;

  TEvePointSelector ss(fVSD->fTreeV0, fCascadeV0s, "fVCa.fX:fVCa.fY:fVCa.fZ");
  ss.Select();
  fCascadeV0s->SetTitle("Cascade_CA_Points");

  for (Int_t n = 0; n < nTracks; ++n) {
      std::cerr << "-----------------------------------------------------------------------Cascade" << std::endl;
    fVSD->fTreeV0->GetEntry(n);
    fVSD->fTreeR->GetEntry(n);

    Int_t pdgN = -211; // pi-
    Int_t pdgP = 2212; // proton
    Int_t pdg = fVSD->fV0.fPdg;
    switch (pdg) {
      case 3312:
        pdgN = -211;
        pdgP = +2212;
        break;
      case -3312:
        pdgN = +211;
        pdgP = -2212;
        break;
#if 0
	// Omegas
      case -3332: pdgN = -2212; pdgP = +211; break;
      case  3332: pdgN = +2212; pdgP = -211; break;
#endif
      default:
        // FIXME What is this case? It can occur!
        break;
    }
    TDatabasePDG* pdgDb = TDatabasePDG::Instance();

    TEveRecTrack rcNeg;
    rcNeg.fP.Set(fVSD->fV0.fPNeg);
    rcNeg.fV.Set(fVSD->fV0.fVNeg);
    rcNeg.fStatus = fVSD->fV0.fStatus;
    rcNeg.fLabel = fVSD->fV0.fDLabel[0];
    rcNeg.fSign = -1;
    Double_t momentum = fVSD->fV0.fPNeg.Mag();
    Double_t mass = pdgDb->GetParticle(pdgN)->Mass();
    rcNeg.fBeta =
      momentum / TMath::Sqrt(momentum * momentum + TMath::C() * TMath::C() * mass * mass);

    auto* trackN = new TEveTrack(&rcNeg, prop);
    trackN->SetName(RenderElementToString(ERenderElements::kCascadeTrackNegative));
    trackN->SetAttLineAttMarker(fCascadeNeg);
    trackN->SetPdg(pdgN);
    trackN->SetUniqueID(n);

    Double_t pxNeg = trackN->GetMomentum().fX;
    Double_t pyNeg = trackN->GetMomentum().fY;
    Double_t pzNeg = trackN->GetMomentum().fZ;

    // -------
    TEveRecTrack rcPos;
    rcPos.fP.Set(fVSD->fV0.fPPos);
    rcPos.fV.Set(fVSD->fV0.fVPos);
    rcPos.fStatus = fVSD->fV0.fStatus;
    rcPos.fLabel = fVSD->fV0.fDLabel[1];
    rcPos.fSign = 1;
    momentum = fVSD->fV0.fPPos.Mag();
    mass = pdgDb->GetParticle(pdgP)->Mass();
    rcPos.fBeta =
      momentum / TMath::Sqrt(momentum * momentum + TMath::C() * TMath::C() * mass * mass);

    auto* trackP = new TEveTrack(&rcPos, prop);
    trackP->SetName(RenderElementToString(ERenderElements::kCascadeTrackPositive));
    trackP->SetAttLineAttMarker(fCascadePos);
    trackP->SetPdg(pdgP);
    trackP->SetUniqueID(n);

    Double_t pxPos = trackP->GetMomentum().fX;
    Double_t pyPos = trackP->GetMomentum().fY;
    Double_t pzPos = trackP->GetMomentum().fZ;

    auto* trackB = new TEveTrack(&fVSD->fR, propB);
    trackB->SetName(RenderElementToString(ERenderElements::kCascadeTrackBachelor));
    trackB->SetStdTitle();
    trackB->SetAttLineAttMarker(fCascadeBachelor);
    trackB->SetPdg(fVSD->fR.fSign * 211);
    trackB->SetUniqueID(n);

    Double_t pxBac = trackB->GetMomentum().fX;
    Double_t pyBac = trackB->GetMomentum().fY;
    Double_t pzBac = trackB->GetMomentum().fZ;

    Double_t pcaXCas = fVSD->fV0.fVCa.fX;
    Double_t pcaYCas = fVSD->fV0.fVCa.fY;
    Double_t pcaZCas = fVSD->fV0.fVCa.fZ;

    Double_t pcaXBac = trackB->GetVertex().fX;
    Double_t pcaYBac = trackB->GetVertex().fY;
    Double_t pcaZBac = trackB->GetVertex().fZ;

    momentum = TMath::Sqrt((pxNeg + pxPos + pxBac) * (pxNeg + pxPos + pxBac) +
                           (pyNeg + pyPos + pyBac) * (pyNeg + pyPos + pyBac) +
                           (pyNeg + pyPos + pzBac) * (pyNeg + pyPos + pzBac));
    Double_t dir[3] = { (pxNeg + pxPos + pxBac) / momentum, (pyNeg + pyPos + pyBac) / momentum,
                        (pzNeg + pzPos + pzBac) / momentum };

    /// FIXME No change visible in Strangeness
    auto* plb = new TEveLine(RenderElementToString(ERenderElements::kCascadeLine1));
    plb->SetPoint(0, 0.0, pcaYBac - pcaXBac * dir[1] / dir[0], pcaZBac - pcaXBac * dir[2] / dir[0]);
    plb->SetPoint(1, pcaXBac, pcaYBac, pcaZBac);
    plb->SetLineColor(kSpring + 6);
    plb->SetLineWidth(2);
    plb->SetLineStyle(2);
    trackB->AddElement(plb);

    /// FIXME No change visible in Strangeness
    auto* plc = new TEveLine(RenderElementToString(ERenderElements::kCascadeLine2));
    plc->SetPoint(0, pcaXBac, pcaYBac, pcaZBac);
    plc->SetPoint(1, pcaXCas, pcaYCas, pcaZCas);
    plc->SetLineColor(kSpring - 6);
    plc->SetLineWidth(2);
    plc->SetLineStyle(3);

    trackN->AddElement(plc);
    trackP->AddElement(plc);

    fCascadePos->AddElement(trackP);
    fCascadeNeg->AddElement(trackN);
    fCascadeBachelor->AddElement(trackB);

    Emit("LoadedCascadeNegative(TEveTrack*)", trackN);
    Emit("LoadedCascadePositive(TEveTrack*)", trackP);
    Emit("LoadedCascadeBachelor(TEveTrack*)", trackB);
    Emit("LoadedCascade()");
  }

  fCascadePos->MakeTracks();
  fCascadeNeg->MakeTracks();
  fCascadeBachelor->MakeTracks();

  Emit("FinishedCascades()");
}

Bool_t VSDReader::handleTrack(TEveTrack *track)
{
    Double_t dcaXY = fVSD->fR.fDcaXY;
    Double_t dcaZ = fVSD->fR.fDcaZ;

    track->SetLineColor(40);
    track->SetTitle(
            Form("p=(%5.3f,%5.3f,%5.3f)\n"
                 "dca xy=%5.3f z=%5.3f",
                 track->GetMomentum().fX, track->GetMomentum().fY, track->GetMomentum().fZ, dcaXY, dcaZ));
    ClearPrimary(track);
    Bool_t CurrentTrackIsPrimary = false;

    // Condition for Primary tracks
    if (TMath::Abs(dcaXY) < 0.5 && TMath::Abs(dcaZ) < 1.)
    {
        track->SetLineColor(9);
        MarkPrimary(track);

        // Mark this tracks as primary and count it as a primary.
        CurrentTrackIsPrimary = true;
    }

    // Decide if the track shall be added to the set of tracks that get
    // visualized. The option is to exclude secondary tracks.
    return !(fVisualizeOnlyPrimaries && !CurrentTrackIsPrimary);
}
} // namespace Utility
