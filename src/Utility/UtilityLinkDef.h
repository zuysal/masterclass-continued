#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

// AbstractMasterClassContent.h
#pragma link C++ class Utility::TAbstractExercise+;
#pragma link C++ class Utility::TAbstractMasterClassContent+;

#pragma link C++ class Utility::TEventAnalyseGUI+;
#pragma link C++ enum class Utility::EActiveTask+;
#pragma link C++ enum class Utility::ERenderables+;

// AliceBrowser.h
#pragma link C++ struct Utility::Browser+;

// AliceDetails.h
#pragma link C++ class Utility::Details+;

// AliceEventDisplay.h
#pragma link C++ class Utility::EventDisplay+;

// AliceExercise.h
#pragma link C++ class Utility::Exercise+;
// AliceInfo.h
#pragma link C++ class Utility::InfoBox+;

// AliceInstructions.h
#pragma link C++ class Utility::Instructions+;

// AliceLogoButtons.h
#pragma link C++ class Utility::LogoButtons+;

// AliceNavigation.h
#pragma link C++ class Utility::INavigation+;
#pragma link C++ struct Utility::internal::TInstructionsFrame+;
#pragma link C++ struct Utility::internal::TEventsFrame+;

// AliceVSDReader.h
#pragma link C++ class Utility::VSDReader+;

// ContentTranslation.h
#pragma link C++ class Utility::TContentTranslation+;

// LanguageProvider.h
#pragma link C++ class Utility::TLanguageProvider+;

// Dataset.h
#pragma link C++ class Utility::Dataset+;

#endif
