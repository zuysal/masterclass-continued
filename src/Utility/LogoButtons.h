/**
 * @file   LogoButtons.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Mar 16 23:41:44 2017
 *
 * @brief  The ALICE logo and some buttons
 * @ingroup  alice_masterclass_base
 */
#ifndef ALICELOGOBUTTONS_C
#define ALICELOGOBUTTONS_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGFileDialog.h>
#include <TGFont.h>
#include <TGFrame.h>

#include "Utility/Exercise.h"
#include "Utility/GUITranslation.h"

class TBuffer;
class TClass;
class TGPictureButton;
class TMemberInspector;
class TString;

namespace Utility
{
class Exercise;
struct TGUIEnglish;
} // namespace Utility

namespace Utility
{
/**
 * A widget with the ALICE logo and some buttons
 *
 * @ingroup  alice_masterclass_base
 */
class LogoButtons : public TGVerticalFrame
{
 private:
  Exercise* fExercise;
  TGPictureButton* fLogo;
  ULong_t fHighlight{};
  const TGUIEnglish& fTranslation;

 public:
  /**
   * Constructor
   *
   * @param p        Parent frame
   * @param e        Exercise
   * @param cheats   Whether we already have enabled cheats
   */
  LogoButtons(TGCompositeFrame* p, Exercise* e, Bool_t cheats = false);

  /**
   * Toggle cheats enabled.  Calls Exercise::ToggleCheat(Bool_t)
   * with an argument of either true if the cheats are enabled, or
   * false if not.
   */
  void ToggleCheat();

  /**
   * @{
   * @name Export functions
   */

  /**
   * Pop-up a dialog to select the output file
   *
   * @param types Types to enable in the selection
   * @param out   On return, the selected output name
   *
   * @return true if a file was selected
   */
  Bool_t SelectOutput(const char** types, TString& out);

  /**
   * TODO Remove hack
   *
   * Trampoline to exercise.  Note, we use the interpreter to forward
   * the call so that we do not need to know the definition of
   * Exercise here. Strictly speaking a little of a hack
   */
  void PrintPdf();

  /**
   * TODO Remove hack
   * Trampoline to exercise.  Note, we use the interpreter to forward
   * the call so that we do not need to know the definition of
   * Exercise here. Strictly speaking a little of a hack
   */
  void Export();

  /* @} */
  ClassDef(LogoButtons, 0);
};
} // namespace Utility

#endif
