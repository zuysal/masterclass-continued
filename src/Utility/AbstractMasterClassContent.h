#ifndef ABSTRACTMASTERCLASSCONTENT_H_FTOOI14P
#define ABSTRACTMASTERCLASSCONTENT_H_FTOOI14P

#include <RtypesCore.h>
#include <TString.h>
#include <memory>
#include <vector>

#include "Utility/ContentTranslation.h"

namespace Utility
{
/// This type contains a short description and entry point for a exercise
/// within a MasterClass.
/// TODO Provide better examples on usage
class TAbstractExercise
{
 private:
  TString fExerciseName;

 public:
  TAbstractExercise() = default;
  TAbstractExercise(TString ExerciseName);
  virtual ~TAbstractExercise() = default;

  /// Return the Name of the Exercise that can be displayed in the GUI.
  TString GetName() const { return fExerciseName; }

  /// Execute the EntryPoint of the exercise from where everything
  /// else evolves.
  virtual void RunExercise(Bool_t AllowAuto = false) = 0;
};

/// TODO Add description on how to use this abstract baseclass.
class TAbstractMasterClassContent
{
 public:
  TAbstractMasterClassContent(std::unique_ptr<TContentTranslation> lang);
  virtual ~TAbstractMasterClassContent() = default;

  /// Change the translation of for all Exercises and the Class itself.
  void ChangeLanguage(std::unique_ptr<TContentTranslation> lang);

  /// Name of the MasterClass, like "Strange Particles"
  TString GetName() const { return fTranslation->Name(); }

  /// Human-readable text that describes the content of the class.
  TString GetDescription() const { return fTranslation->Description(); }

  /// Return a translated list of all exercises to display in the GUI.
  std::vector<TString> GetExerciseNames() const;

  /// Start the Exercise according to the given Index.
  void RunExercise(Int_t Idx) { fExercises.at(Idx)->RunExercise(); }

 protected:
  void AddExercise(std::unique_ptr<TAbstractExercise> Exercise);

 private:
  /// Provide human readable information about the MasterClass that is being
  /// run.
  std::unique_ptr<TContentTranslation> fTranslation;

  /// This vector contains a list of exercise keys and entry point
  std::vector<std::unique_ptr<TAbstractExercise>> fExercises;
};
} // namespace Utility

#endif /* end of include guard: ABSTRACTMASTERCLASSCONTENT_H_FTOOI14P */
