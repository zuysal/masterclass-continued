#include "Browser.h"

#include <RtypesCore.h>
#include <TGClient.h>
#include <TGMsgBox.h>
#include <TRootBrowser.h>
#include <TString.h>

#include "Utility/Exercise.h"
#include "Utility/GUITranslation.h"
#include "Utility/Instructions.h"
#include "Utility/LanguageProvider.h"

namespace Utility
{
/**
 * A base class for ALICE browsers.
 *
 * @ingroup  alice_masterclass_base
 */
Browser::Browser(const TString& title)
  : TRootBrowser(nullptr, Form("ALICE - %s", title.Data()), 900, 700, "", false)
  , fExercise(nullptr)
  , fTranslation(TranslationFromEnv<TUtilityLanguage>())
{
  std::cerr << "Utility browser created\n";
  DontCallClose();
}

void Browser::Setup(Exercise* exercise, Bool_t cheat)
{
  fExercise = exercise;
  fExercise->Setup(this, cheat);
  StartEmbedding(TRootBrowser::kRight);
  new Instructions(nullptr, 900, 700, exercise->Instructions(), false);
  StopEmbedding("Instructions");
  GetTabRight()->GetTabTab("Instructions")->ShowClose(kFALSE);
  std::cerr << "Utility instructions created\n";

  MapWindow();
  std::cerr << "Show Utility Browser in setup\n";
}

void Browser::Auto()
{
  Int_t ret = 0;
  new TGMsgBox(gClient->GetRoot(), this, fTranslation.AutoAnalyseTitle(),
               fTranslation.AutoAnalyseText(), kMBIconQuestion, kMBYes | kMBNo, &ret);
  if (ret != kMBYes) {
    return;
  }
  fExercise->Auto();
}

} // namespace Utility
