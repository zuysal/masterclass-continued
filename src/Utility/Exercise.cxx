#include "Exercise.h"

#include "Utility/Utilities.h"
#include "StdFixes.h"

class TFile;

namespace Utility
{
TFile* Exercise::DataFile(const char* filename, const char* dir) const
{
  TString d(dir);
  return File((dir != nullptr ? d : DataDir()), filename);
}

std::unique_ptr<VSDReader> Exercise::CreateReader()
{
  return std_fix::make_unique<VSDReader>();
}

} // namespace Utility
