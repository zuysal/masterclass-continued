#ifndef MASTERCLASSES_DATASET_H
#define MASTERCLASSES_DATASET_H

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>
#include <TString.h>
#include <TGHtml.h>
#include <fstream>
#include <iostream>

#include "Utility/AbstractMasterClassContent.h"
#include "Utility/INavigation.h"
#include "Utility/Utilities.h"

class TBuffer;
class TClass;
class TGTextButton;
class TMemberInspector;

namespace Utility
{
    struct TGUIEnglish;
} // namespace Utility

namespace Utility
{
/** Data set selector
 * @image html Strangeness/doc/Part1Selector.png
 * @ingroup alice_masterclass_str_part1
 */
    class Dataset: public TGMainFrame
    {
    private:
        TGTextButton* fStart{ nullptr };
        TGHtml* fInstructions;

    protected:
        Int_t fDataset{ 0 };
        Bool_t fAllowAuto;
        Bool_t fHasDemoSet;
        TGUIEnglish& fTranslation;
        TGComboBox* fSelectionCombo;

    private:
        virtual void fillSelectionBox();

    protected:
        void setInstructions(const TString& instructionsHTML);

    public:
        Dataset(Bool_t allowAuto, Bool_t hasDemoSet);
        ~Dataset() override;
        void Choice(Int_t id);
        virtual void Start() = 0;
        void Init();

        ClassDefOverride(Dataset, 0)
    };

    class StandardDataset : public Dataset
    {
    private:
        TString fMainWindowTitle;
        Int_t fDatasetCount;

    protected:
        void setMainWindowTitle(const TString& windowTitle);

    private:
        void fillSelectionBox() override;
        virtual std::unique_ptr<Utility::EventDisplay> createEventDisplay(Bool_t demo) = 0;
        virtual std::unique_ptr<Utility::INavigation> createNavigation(EventDisplay* exercise, Bool_t cheat) = 0;
        void Code(const TString& data, const TString& geom, Bool_t cheat, Bool_t demo = kFALSE);
    public:
        StandardDataset(Int_t datasetCount, Bool_t allowAuto, Bool_t hasDemoSet) : Dataset(allowAuto, hasDemoSet), fDatasetCount(datasetCount) {};
        void Start() override;
    };
} // namespace Utility

#endif //MASTERCLASSES_DATASET_H