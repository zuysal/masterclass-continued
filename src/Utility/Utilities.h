/**
 * @file   Utilities.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 19:48:59 2017
 *
 * @brief  Various utilities
 *
 * @ingroup  alice_masterclass_utils
 *
 */

#ifndef ALICEUTILITIES_C
#define ALICEUTILITIES_C

#include <RtypesCore.h>
#include <TApplication.h>
#include <TError.h>
#include <TFile.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TGPicture.h>
#include <TGProgressBar.h>
#include <TString.h>
#include <TSystem.h>
#include <fstream>
#include <iostream>
#include <sstream>

class TFile;
class TGPicture;

namespace Utility
{
/// Sets visual style for the EventDisplay based Masterclasses.
void SetupStyleForClass();

/// Little helper function to retrieve the value of an environment variable
/// as string. Empty if non-existing.
TString GetEnvValue(const TString& env_var_name);

/**
 * @defgroup alice_masterclass_base Base code for the ALICE master classes
 * @ingroup alice_masterclass
 */
/**
 * @defgroup alice_masterclass_utils Utilities
 * @ingroup  alice_masterclass_base
 */
/**
 * Get a file path to a master class file.
 *
 * @param filename File name relative to the master class package.
 *
 * @return The absolute path to the file
 *
 * @ingroup  alice_masterclass_utils
 */
TString MasterClassPath(const TString& filename);

/// Return a full path to a resource. Uses CMake installation
/// directory information for that.
TString ResourcePath(const TString& file);

/**
 * Get an image as a graphics picture
 *
 * @param module Name of the module where the image resides
 * @param name Name of the image relative to the sub-directory @c
 * Pictures in the top-level directory of the master class package.
 *
 * @return Pointer to picture
 *
 * @ingroup  alice_masterclass_utils
 */
const TGPicture* Picture(const char* module, const char* name);

/**
 * Get the ALICE logo as an graphics picture
 *
 * @return Pointer to ALICE logo
 *
 * @ingroup  alice_masterclass_utils
 */
inline const TGPicture* Logo() { return Picture("Utility", "ALICE_logo.png"); }

/**
 * Export master class path to environment
 *
 * @param self  name of executing script
 * @param depth depth of this script
 *
 * @return Directory
 *
 * @ingroup  alice_masterclass_utils
 */
TString SetMasterClassPath(const char* self, Int_t depth);

/**
 * TODO This does a lot to magically work. Reduce the necessity for it.
 * Open a master class file in read-only mode
 *
 * @param dir  Directory (relative to top)
 * @param name File name
 *
 * @return Pointer to file handle or null
 *
 * @ingroup  alice_masterclass_utils
 */
TFile* File(const TString& dir, const TString& name);

/**
 * Clamp float value between min and max
 *
 * @param val  Value to clamp
 * @param min  Min allowed value
 * @param max  Max allowed value
 *
 * @return Clamped value
 *
 * @ingroup  alice_masterclass_utils
 */
Float_t Clamp(Float_t val, Float_t min, Float_t max);
} // namespace Utility

#endif
