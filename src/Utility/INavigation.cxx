#include "Utility/INavigation.h"

#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TString.h>
#include <vector>

#include "Utility/EventDisplay.h"
#include "Utility/GUITranslation.h"
#include "Utility/Info.h"
#include "Utility/LanguageProvider.h"
#include "Utility/LogoButtons.h"

namespace Utility
{
INavigation::INavigation(EventDisplay* exercise, Bool_t cheat, Bool_t extendedEventNavigation)
  : TGCompositeFrame(nullptr, 500, 600)
  , fTranslation(TranslationFromEnv<TUtilityLanguage>())
  , fLogoButton(new LogoButtons(this, exercise, cheat))
  , fInstructionButtonFrame(new internal::TInstructionsFrame(this, fTranslation.Instructions()))
  , fEventsFrame(new internal::TEventsFrame(this, fTranslation, extendedEventNavigation))
  , fEventDisplay(exercise)
{
  SetWindowName(fTranslation.Navigation());
  SetCleanup(kDeepCleanup);

  auto* LayoutExpandX = new TGLayoutHints(kLHintsExpandX);

  // --- Instructions ----------------------------------------------
  //this->AddFrame(fInstructionButtonFrame, LayoutExpandX);

  // Events are internal, so no need to delay the creation of them.
  // No entanglement created here.
  fEventsFrame->fButtonPrevious->Connect("Clicked()", "Utility::INavigation", this,
                                         "SignalPreviousEvent()");
  fEventsFrame->fButtonNext->Connect("Clicked()", "Utility::INavigation", this,
                                     "SignalNextEvent()");
  this->AddFrame(fEventsFrame, LayoutExpandX);
}

void INavigation::SetupSignalSlots(TEventAnalyseGUI* AnalysisGUI, TEventVisualization *EventVisualization, VSDReader* /*Reader*/)
{
  fInstructionButtonFrame->fButtonInstructions->Connect("Clicked()", "Utility::TEventAnalyseGUI",
                                                        AnalysisGUI, "OpenInstructions()");
  fEventsFrame->fButtonEventAnalyzed->Connect("Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                                              "FinishCurrentEvent()");
}

/// Show the detector information.
void INavigation::DetectorInfo()
{
  std::vector<TString> names = { "ALICE", "ITS", "TPC", "TRD+TOF", "PHOS", "HMPID" };
  new InfoBox("ALICE detectors", names, gClient->GetRoot(), 100, 100);
}

void INavigation::SetEventNumber(Int_t Num, Int_t Max)
{
  fEventsFrame->fLabelEventNumber->SetText(TString::Format("%d / %d", Num, Max));
}

void INavigation::SetEventsAnalysed(Int_t count)
{
  fEventsFrame->fEventAnalysed->SetText(TString::Format("%d", count));
}

void SetupNavigationButton(TGButton* Button, const TString& ToolTip, Bool_t Toggle)
{
  Button->AllowStayDown(true);
  Button->SetToolTipText(ToolTip);

  (void)Toggle;
  // if (Toggle)
  // Button->Toggle(false);
}

} // namespace Utility
