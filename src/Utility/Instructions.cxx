#include <GuiTypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFont.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGLayout.h>
#include <TString.h>
#include <stdexcept>

#include "Utility/GUITranslation.h"
#include "Utility/Instructions.h"
#include "Utility/LanguageProvider.h"
#include "Utility/Utilities.h"

class TGWindow;

namespace Utility
{
TGTextButton* MakeInstructionsButton(TGCompositeFrame* p)
{
  ULong_t red;
  gClient->GetColorByName("red", red);
  const TGFont* gfont = gClient->GetFont("-*-times-bold-r-*-*-16-*-*-*-*-*-*-*");
  FontStruct_t font = gfont->GetFontStruct();

  Utility::TGUIEnglish& Translation = Utility::TranslationFromEnv<Utility::TUtilityLanguage>();
  auto* b = new TGTextButton(p, Translation.Instructions());
  b->SetTextColor(red);
  b->SetFont(font);

  p->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  return b;
}

Instructions::Instructions(const TGWindow* p, Int_t w, Int_t h, const TString& Content, Bool_t exit)
  : TGMainFrame(gClient->GetRoot(), w, h, kHorizontalFrame)
  , fTranslation(TranslationFromEnv<TUtilityLanguage>())
{
  auto* htmlView = new TGHtml(this, w - 4, h - 20);
  htmlView->ParseText(const_cast<char*>(Content.Data()));
  AddFrame(htmlView, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 2, 2, 2));

  //if (exit) {
  //  TGButton* ok = new TGTextButton(this, "  &OK  ");
 //   ok->Connect("Clicked()", "Utility::Instructions", this, "UnmapWindow()");
  //  AddFrame(ok, new TGLayoutHints(kLHintsBottom | kLHintsCenterX, 0, 0, 5, 5));
 // }

  //SetWindowName(fTranslation.Instructions());
  //SetIconName(fTranslation.Instructions());

  MapSubwindows();
  //Resize(GetDefaultSize());
  //CenterOnParent();
  MapWindow();
}
} // namespace Utility
