/**
 * @file   Details.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 09:03:58 2017
 *
 * @brief  A widget to show information about a single detector
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 */
#ifndef ALICEDETAILS_C
#define ALICEDETAILS_C
#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>
#include <TString.h>

class TBuffer;
class TClass;
class TGButton;
class TGWindow;
class TMemberInspector;
class TRootEmbeddedCanvas;

namespace Utility
{
//====================================================================
/**
 * Show information about a specific detector
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 */
class Details : public TGMainFrame
{
 private:
  const TGPicture* fPicture;
    TRootEmbeddedCanvas* fCanvas;
 public:
  /**
   * Constructor
   *
   * @param what   What to show
   * @param module Module name
   * @param title  The title to show
   * @param p      Parent
   * @param w      Width
   * @param h      Height
   */
  Details(TString what, TString module, const TString& title, const TGWindow* p, UInt_t w, UInt_t h);

  ClassDef(Details, 0);

  virtual ~Details();
};
} // namespace Utility

#endif
