/**
 * @file   EventDisplay.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 10:22:41 2017
 *
 * @brief  Base class for eventdisplays
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 */
#ifndef ALICEEVENTDISPLAY_C
#define ALICEEVENTDISPLAY_C

#include <RtypesCore.h>
#include <TString.h>

#include "Utility/Exercise.h"
#include "Utility/Renderables.h"

class TEveElement;
class TEveTrack;
class TRootBrowser;

namespace Utility
{
/**
 * This defines the interface for eventdisplays.  A specific exercise
 * should implement this interface to set up the GUI elements needed
 * by the eventdisplay
 *
 * @ingroup alice_masterclass_base_eventdisplay
 */
class EventDisplay : public Exercise
{
 public:
  EventDisplay() = default;
  ~EventDisplay() override = default;

  /**
   * Set-up the elements needed by the exercise - e.g., GUI elements
   * or the like
   * @param browser The browser we're using
   * @param cheat   If true, enable cheats
   */
  void Setup(TRootBrowser* browser, Bool_t cheat) override = 0;

  /**
   * Called after reading an event.  Should reset what is needed.
   * @param evNo The current event number
   * @return Should return a text string describing the event
   */
  virtual TString NewEvent(Int_t /*evNo*/) { return ""; }

  /**
   * Can be overloaded to set the magnetic field
   * @return
   */
  virtual Double_t GetMagneticField() const { return 0.5; }

  /**
   * Called when the student finish with an event.
   */
  virtual void EventDone() = 0;

  /**
   * Automatically process an event
   */
  virtual void AutoEvent() {}

  /**
   * Return bit mask of what to initially load
   * @return Bit mask
   */
  virtual UInt_t InitialLoad() const
  {
    return static_cast<UInt_t>(ERenderables::kIntersectionPoint) |
           static_cast<UInt_t>(ERenderables::kTracks) |
           static_cast<UInt_t>(ERenderables::kGeometry);
  }

  /**
   * Set rendering options on element
   * @param el        Element
   * @param visSelf   Whether to show self
   * @param opacity   Opacity
   */
  virtual void FixRender(TEveElement* el, Bool_t visSelf = true, Int_t opacity = 80);

  /// Fix which volumes to show
  virtual void FixGeometry(gsl::not_null<TEveElement*> el);

  /**
   * Print an element and all it's children (optionally limited to
   * some level) *
   * @param el  Element
   * @param lvl Current level (top is 0)
   * @param max Maximum recursion level
   */
  void PrintElement(TEveElement* el, Int_t lvl, Int_t max = -1);

  /// TODO: fix this
  virtual Bool_t TryChooseTask(Int_t NTask) { return kTRUE; }
  virtual Int_t GetCurrentTask() const noexcept { return 0; }

};
} // namespace Utility

#endif
