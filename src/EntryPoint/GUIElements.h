#ifndef GUIELEMENTS_C_GT8IHC7W
#define GUIELEMENTS_C_GT8IHC7W

#include <RtypesCore.h>

#include "EntryPoint/GUITranslation.h"
#include "EntryPoint/MasterClassRegistry.h"
#include "Utility/LanguageProvider.h"

class TGComboBox;
class TGGroupFrame;
class TGHtml;
class TGLayoutHints;
class TGPicture;
class TGPictureButton;
class TGTextButton;
class TGWindow;

namespace EntryPoint
{
struct ITranslateable {
  virtual void Translate(const TGUIEnglish& /*TextProvider*/) {}
  virtual ~ITranslateable() = default;
};

struct TGUILogo : ITranslateable {
  const TGPicture* fLogoPicture;
  TGPictureButton* fLogoButton;

  TGUILogo(TGWindow* paren);
};

struct TGUISettings : ITranslateable {
  TGGroupFrame* fContentFrame;
  TGComboBox* fLanguageSelectionBox;
  TGComboBox* fDataSource;

  TGUISettings(TGWindow* paren, TGLayoutHints* lh);

  void Translate(const TGUIEnglish& TextProvider) override;

 private:
  void SetupSources(const TGUIEnglish& TextProvider);
};

struct TGUIDescription : ITranslateable {
  TGHtml* fDesc;

  TGUIDescription(TGWindow* paren);
  void Translate(const TGUIEnglish& TextProvider) override;
};

struct TGUIClasses : ITranslateable {
  TMasterClassRegistry fClassRegistry;
  TGGroupFrame* fContentFrame;
  TGComboBox* fClassesBox;
  TGComboBox* fExerciseBox;

  TGUIClasses(TGWindow* paren, TGLayoutHints* lh, Utility::ESupportedLanguages lang);

  void Translate(const TGUIEnglish& TextProvider) override;

  /// Fill all entries for the masterclass selection including the exercises
  /// if an actual class is selected.
  void FillClassesSelection(const TGUIEnglish& TextProvider, Int_t SelectedClassIdx);

  void FillExercises(const TGUIEnglish& TextProvider, Int_t SelectedClass);
};

struct TGUIControlArea : ITranslateable {
  TGTextButton* fStartButton;

  TGUIControlArea(TGWindow* paren);
  void Translate(const TGUIEnglish& TextProvider) override;
};
} // namespace EntryPoint

#endif /* end of include guard: GUIELEMENTS_C_GT8IHC7W */
