/**
 * @file   MasterClass.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 19:53:56 2017
 *
 * @brief  Main entry point for the ALICE master classes
 *
 * @ingroup alice_masterclass
 */

#include "ClassSelector.h"

#include <RtypesCore.h>
#include <TApplication.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGLayout.h>
#include <TGTextEntry.h>
#include <TString.h>
#include <TSystem.h>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <vector>

#include "MasterClassRegistry.h"
#include "Utility/AbstractMasterClassContent.h"
#include "compilation_number.h"

namespace EntryPoint
{
ClassSelector::ClassSelector(Utility::ESupportedLanguages lang)
  : fTranslation(TEntryPointLanguage::create(lang))
  , fLayoutHints(new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2))
  , fMainFrame(new TGMainFrame(gClient->GetRoot(), 0, 0, kVerticalFrame))
  , fLogoArea(fMainFrame)
  , fSettingsArea(fMainFrame, fLayoutHints)
  , fDescriptionArea(fMainFrame)
  , fClassesArea(fMainFrame, fLayoutHints, lang)
  , fControlArea(fMainFrame)
  , fAllowAuto(false)
  , fLastClassSelected(0)
{
}

/// Create the whole GUI.
void ClassSelector::SetupGUI()
{
  std::cerr << "Contructing GUI for MasterClass\n";

  std::ostringstream app_name;
  app_name << "MasterClass v0.8 build " << BUILD_NUMBER;
  fMainFrame->SetWindowName(app_name.str().c_str());

  // ALICE logo.  Note, the logo is a "secret" button to allow
  // auto-analysos-mode on the exercises.
  fMainFrame->AddFrame(fLogoArea.fLogoButton, fLayoutHints);
  fLogoArea.fLogoButton->Connect("Clicked()", "EntryPoint::ClassSelector", this,
                                 "ToggleAutoAnalysis()");

  // Settings Area to choose language and the data source.
  fMainFrame->AddFrame(fSettingsArea.fContentFrame, fLayoutHints);
  fSettingsArea.fLanguageSelectionBox->Connect("Selected(Int_t)", "EntryPoint::ClassSelector", this,
                                               "LanguageSelected(Int_t)");
//  fSettingsArea.fDataSource->Connect("Selected(Int_t)", "EntryPoint::ClassSelector", this,
//                                     "SourceSelected(Int_t)");

  // The description text area that is style with HTML.
  fMainFrame->AddFrame(fDescriptionArea.fDesc,
                       new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 2));

  // Add the Area containing the available classes and corresponding exercises.
  fMainFrame->AddFrame(fClassesArea.fContentFrame, fLayoutHints);
  fClassesArea.fClassesBox->Connect("Selected(Int_t)", "EntryPoint::ClassSelector", this,
                                    "ClassSelected(Int_t)");
  fClassesArea.fExerciseBox->Connect("Selected(Int_t)", "EntryPoint::ClassSelector", this,
                                    "ExerciseSelected(Int_t)");

  // Control area with start button
  fMainFrame->AddFrame(fControlArea.fStartButton, new TGLayoutHints(kLHintsExpandX, 3, 3, 3, 4));
  fControlArea.fStartButton->SetEnabled(false);
  fControlArea.fStartButton->Connect("Clicked()", "EntryPoint::ClassSelector", this, "ExerciseStarted()");

  // Instead of closing the window, terminate the whole app
  fMainFrame->DontCallClose();
  fMainFrame->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");

  std::cerr << "Setting Language to English as default\n";
  // Set the language to English. 1 is used, because that method is used
  // as a slot for the combo box selection. 1 is the place for english.
  LanguageSelected(1);

  std::cerr << "Show windows\n";

  fMainFrame->MapSubwindows();
  fMainFrame->Resize(fMainFrame->GetDefaultSize());
  fMainFrame->MapWindow();

  fSettingsArea.fLanguageSelectionBox->Select(1, kFALSE);
}

void ClassSelector::ToggleAutoAnalysis()
{
  fAllowAuto = !fAllowAuto;
  std::cerr << "Allow auto: " << fAllowAuto << "\n";

  // TODO Maybe the color handling should be refactored out.
  // Low priority
  ULong_t logoColor = TGFrame::GetDefaultFrameBackground();
  if (fAllowAuto) {
    gClient->GetColorByName("red", logoColor);
  }

  fLogoArea.fLogoButton->ChangeBackground(logoColor);
}

void ClassSelector::ClassSelected(Int_t SelectionIdx)
{
  if (fLastClassSelected == SelectionIdx)
    return;

  if (SelectionIdx > 0)
  {
    std::cerr << "Master class selection is: " << SelectionIdx << "\n";
    fClassesArea.fExerciseBox->SetEnabled(true);
    fClassesArea.FillExercises(*fTranslation, SelectionIdx - 1);
    fClassesArea.fExerciseBox->Select(0, kFALSE);
  } else {
    fClassesArea.fExerciseBox->Select(0, kFALSE);
    fClassesArea.fExerciseBox->SetEnabled(false);
  }

  fLastClassSelected = SelectionIdx;

  checkIsSelectionCorrect();
}

void ClassSelector::ExerciseSelected(Int_t /*SelectionIdx*/)
{
  checkIsSelectionCorrect();
}

void ClassSelector::checkIsSelectionCorrect()
{
  Int_t ClassSelectionIdx = fClassesArea.fClassesBox->GetSelected();
  Int_t ExcersizeSelectionIdx = fClassesArea.fExerciseBox->GetSelected();

  bool isCorrect = (ClassSelectionIdx > 0 && ExcersizeSelectionIdx > 0);
  fControlArea.fStartButton->SetEnabled(isCorrect);
}

void ClassSelector::ExerciseStarted()
{
  // Minus one, because the first element is a help string to understand
  // what to select.
  Int_t ClassIdx = fClassesArea.fClassesBox->GetSelected() - 1;
  Int_t ExerciseIdx = fClassesArea.fExerciseBox->GetSelected() - 1;

  std::cerr << "You selected exercise # " << ExerciseIdx << "\n";
  std::cerr << "Current checked in class # " << ClassIdx << "\n";
  std::cerr << "Starting Script Exercise\n";
  fMainFrame->UnmapWindow();

  fClassesArea.fClassRegistry.GetClasses()[ClassIdx]->RunExercise(ExerciseIdx);
}

void ClassSelector::SourceSelected(Int_t i)
{
  std::cerr << "You selected data source # " << i << "\n";
  if (i == 0) {
    gSystem->Setenv("ALICE_MASTERCLASS_DATA", "");
    return;
  }

  if (fSettingsArea.fDataSource->GetTextEntry() == nullptr)
    return;

  TString src(fSettingsArea.fDataSource->GetTextEntry()->GetText());
  std::cerr << "Selected source is " << src.Data() << "\n";
  gSystem->Setenv("ALICE_MASTERCLASS_DATA", src.Data());
}

void ClassSelector::LanguageSelected(Int_t i)
{
  // No real language has been selected
  if (i <= 0)
    return;

  // The language is decreased by one, because the first option to select
  // is the "Select a language" text.
  auto lang = static_cast<Utility::ESupportedLanguages>(i - 1);

  std::cerr << "Language is " << GetLanguageName(lang) << " " << GetLanguageNameShort(lang) << "\n";

  // Propagate the choosen language to each MasterClass via a environment
  // variable.
  gSystem->Setenv("ALICE_MASTERCLASS_LANG", GetLanguageNameShort(lang));

  // Update the Translation of the GUI
  fTranslation = TEntryPointLanguage::create(lang);
  std::cerr << "Translating all GUI areas\n";

  fLogoArea.Translate(*fTranslation);
  fSettingsArea.Translate(*fTranslation);
  fDescriptionArea.Translate(*fTranslation);
  fControlArea.Translate(*fTranslation);

  std::cerr << "Translating Master Class selections\n";
  fClassesArea.fClassRegistry.ChangeLanguage(lang);
  fClassesArea.Translate(*fTranslation);
}

} // namespace EntryPoint
