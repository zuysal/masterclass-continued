#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Jpsi::TNavigation+;
#pragma link C++ struct Jpsi::internal::TAnalysis+;
#pragma link C++ struct Jpsi::internal::TDisplay+;
#pragma link C++ struct Jpsi::internal::TQuickAnalysis+;
#pragma link C++ struct Jpsi::internal::THistogram+;
#pragma link C++ struct Jpsi::internal::TParticleInfo+;

#pragma link C++ class Jpsi::JpsiDataset+;
#pragma link C++ class Jpsi::TrackCuts+;
#pragma link C++ class Jpsi::SignalExtraction+;
#pragma link C++ class Jpsi::EventDisplay+;

#endif
