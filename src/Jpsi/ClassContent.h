#ifndef CLASSCONTENT_H_D5CJU98B
#define CLASSCONTENT_H_D5CJU98B

#include <memory>
#include <stdexcept>

#include "Jpsi/TJpsiClass.h"
#include "Utility/AbstractMasterClassContent.h"
#include "Utility/ContentTranslation.h"
#include "Utility/StdFixes.h" // IWYU pragma: keep
#include "Utility/Utilities.h" // IWYU pragma: keep

namespace Jpsi
{
struct TContentEnglish : Utility::TContentTranslation {
  TContentEnglish(Utility::ESupportedLanguages lang = Utility::English)
    : TContentTranslation(lang)
  {
    std::cerr << "Registering English Jpsi Class Content\n";
#include "Jpsi/keys_class_trans.txt.en"
  }
};

LANG_TRANSLATION(TContent, German)
{
  std::cerr << "Registering German Jpsi Class Content\n";
#include "Jpsi/keys_class_trans.txt.de"
}

inline std::unique_ptr<Utility::TContentTranslation> LanguageFactory(
  Utility::ESupportedLanguages lang)
{
  switch (lang) {
    case Utility::English:
      return std_fix::make_unique<TContentEnglish>();
    case Utility::German:
      return std_fix::make_unique<TContentGerman>();
    case Utility::NLanguages:
      break;
  }
  throw std::runtime_error("Requested Translation for Jpsi does not exist");
}

struct TClassContent : Utility::TAbstractMasterClassContent {
  TClassContent(Utility::ESupportedLanguages lang)
    : TAbstractMasterClassContent(LanguageFactory(lang))
  {
    AddExercise(std_fix::make_unique<TJpsiClass>());
  }
};
} // namespace Jpsi

#endif /* end of include guard: CLASSCONTENT_H_D5CJU98B */
