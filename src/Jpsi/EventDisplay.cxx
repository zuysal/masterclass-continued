#include <TRootBrowser.h>
#include <TGTab.h>

#include "EventDisplay.h"
#include "Utility/LanguageProvider.h"
#include "JpsiVSDReader.h"

#include <TGSplitter.h>

namespace Jpsi {
    void Jpsi::EventDisplay::Setup(TRootBrowser *browser, Bool_t /*cheat*/) {

      TGTab* bot = browser->GetTabBottom();
      auto* up = reinterpret_cast<TGFrame*>(const_cast<TGWindow*>(bot->GetParent()));
      TGDimension d = up->GetDefaultSize();
      up->Resize(d.fWidth, 170);
      browser->StartEmbedding(TRootBrowser::kBottom);
      fTrackCuts = new TrackCuts(gClient->GetRoot(), 0, 200);
      browser->StopEmbedding(fTranslation.SelectorTitle());

      browser->StartEmbedding(TRootBrowser::kBottom);
      fSignalExtraction = new SignalExtraction(gClient->GetRoot(), 0, 200);
      browser->StopEmbedding("Signal Extraction");

      browser->StartEmbedding(TRootBrowser::kRight);
      fParticleIdentification.Setup();
      browser->StopEmbedding(fTranslation.ParticleIdentification());
      browser->GetTabRight()->GetTabTab(fTranslation.ParticleIdentification())->ShowClose(kFALSE);

      browser->GetTabBottom()->SetMinHeight(300);
      browser->GetTabBottom()->SetHeight(300);

      browser->StartEmbedding(TRootBrowser::kRight);
      fInvariantMass.Setup();
      browser->StopEmbedding(fTranslation.InvariantMass());
      browser->GetTabRight()->GetTabTab(fTranslation.InvariantMass())->ShowClose(kFALSE);

      bot->SetTab(0, kFALSE);
    }

    TString Jpsi::EventDisplay::NewEvent(Int_t /*evNo*/) {
      return "";
    }

    Double_t Jpsi::EventDisplay::GetMagneticField() const {
      return 0.5;
    }

    void Jpsi::EventDisplay::EventDone() {

    }

    Jpsi::EventDisplay::EventDisplay()
      : fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
      , fEnabledQuickAnalysis(kFALSE)
      , fTrackCuts(nullptr)
    {

    }

    void EventDisplay::OnTrackCutChanged(Bool_t cutEnabled, Float_t *p1, Float_t *p2, Float_t *d1, Float_t *d2) {
        fParticleIdentification.SetPIDCutValues(cutEnabled, *p1, *p2, *d1, *d2);
        fParticleIdentification.Update();
    }

    void EventDisplay::OnExtractSignal(Float_t *minm, Float_t *maxm) {
        Float_t numJpsi, signalBackground, significance;
        fInvariantMass.CalculateIntegral(*minm, *maxm, numJpsi, signalBackground, significance);
        fInvariantMass.SetMassLineValues(kTRUE, *minm, *maxm);
        fInvariantMass.Update();
        fSignalExtraction->SetResults(numJpsi, signalBackground, significance);

//        ofstream myresult;
//        myresult.open ("masterclass.save", std::ofstream::out | std::ofstream::app);
//        myresult << "Momentum region: " << p1 <<" < p < "  << p2 << endl;
//        myresult << "Specific energy loss: " << de1 <<" < dE/dx < " << de2 << endl;
//        myresult << "Invariant mass window: " << minm <<"< m <" << maxm << endl;
//        myresult << "Number of events = " << nEvents << endl;
//        myresult << "Number of Jpsi's: " << numJpsi << endl;
//        myresult << "Signal/Background: " << sb << endl;
//        myresult << "Significance: " << significance << endl;
//        myresult << endl;
//        myresult.close();
    }

    void EventDisplay::OnTrackLoaded(TEveTrack *track, Bool_t included) {
      Float_t px = track->GetMomentum().fX;
      Float_t py = track->GetMomentum().fY;
      Float_t pz = track->GetMomentum().fZ;

      Float_t dedx = track->GetStatus();
      Int_t charge = track->GetCharge();
      Float_t pSquared = px * px + py * py + pz * pz;

      if(included)
      {
        fParticleIdentification.AddParticle(TMath::Sqrt(pSquared), dedx);
        
        if(fParticleIdentification.IsEnabled())
        {
          Float_t massSquared = dedx > 62 ? 0. : 0.019;
          Float_t e = TMath::Sqrt(pSquared + massSquared);
          if (charge > 0) {
              fInvariantMass.AddPositron(e, px, py, pz);
          } else {
              fInvariantMass.AddElectron(e, px, py, pz);
          }
        }
      }
    }

    void EventDisplay::TracksCleared()
    {
      fInvariantMass.ClearData();
      fInvariantMass.ClearTemp();
      fParticleIdentification.ClearData();
    }

    void EventDisplay::FillEnergyLossHisto()
    {
      fParticleIdentification.Fill();
      fParticleIdentification.Update();
    }

    void EventDisplay::FillInvariantMassHistos()
    {
      fInvariantMass.Fill();
      fInvariantMass.ClearData();
      fInvariantMass.Update();
    }

    void EventDisplay::ClearHisto() {
      fParticleIdentification.ClearHisto();
      fInvariantMass.ClearHisto();
    }

    void EventDisplay::ToggleQuickAnalysis() {
      fEnabledQuickAnalysis = !fEnabledQuickAnalysis;
    }

    void EventDisplay::FinishedTracks() {
      if(fEnabledQuickAnalysis) {
        FillEnergyLossHisto();
        FillInvariantMassHistos();
      }
    }

    std::unique_ptr<Utility::VSDReader> EventDisplay::CreateReader() {
        return std_fix::make_unique<JpsiVSDReader>();
    }
}