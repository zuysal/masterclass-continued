#ifndef MASTERCLASSES_JPSIVSDREADER_H
#define MASTERCLASSES_JPSIVSDREADER_H

#include "Utility/VSDReader.h"

namespace Jpsi {
    class JpsiVSDReader : public Utility::VSDReader
    {
    private:
        Bool_t fApplyTrackCuts;
        Float_t fP1,fP2,fD1,fD2;
    public:
        void LoadJpsiParameters(Bool_t active, Float_t* p1, Float_t* p2, Float_t* d1, Float_t* d2);
        Bool_t handleTrack(TEveTrack*) override;
    };
}


#endif //MASTERCLASSES_JPSIVSDREADER_H
