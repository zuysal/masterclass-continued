#ifndef CLASSCONTENT_C_TPDPU8OK
#define CLASSCONTENT_C_TPDPU8OK

#include <memory>
#include <stdexcept>

#include "Raa/instructors/Collect.h"
#include "Raa/instructors/Combine.h"
#include "Raa/part1/Part1.h"
#include "Raa/part2/Part2.h"
#include "Utility/AbstractMasterClassContent.h"
#include "Utility/ContentTranslation.h"
#include "Utility/StdFixes.h"

namespace Raa
{
struct TContentEnglish : Utility::TContentTranslation {
  TContentEnglish(Utility::ESupportedLanguages lang = Utility::English)
    : TContentTranslation(lang)
  {
    std::cerr << "Registering English Raa Class Content\n";
#include "Raa/keys_class_trans.txt.en" // IWYU pragma: keep

    RegisterText("Description",
#include "Raa/Description.en.html" // IWYU pragma: keep
    );
  }
};

LANG_TRANSLATION(TContent, German)
{
  std::cerr << "Registering German Raa Class Content\n";
#include "Raa/keys_class_trans.txt.de" // IWYU pragma: keep

    RegisterText("Description",
#include "Raa/Description.en.html" // IWYU pragma: keep
    );
}

inline std::unique_ptr<Utility::TContentTranslation> LanguageFactory(
  Utility::ESupportedLanguages lang)
{
  switch (lang) {
    case Utility::English:
      return std_fix::make_unique<TContentEnglish>();
    case Utility::German:
      return std_fix::make_unique<TContentGerman>();
    case Utility::NLanguages:
      break;
  }
  throw std::runtime_error("Requested Translation for Raa does not exist");
}

struct TClassContent : Utility::TAbstractMasterClassContent {
  TClassContent(Utility::ESupportedLanguages lang)
    : TAbstractMasterClassContent(LanguageFactory(lang))
  {
    AddExercise(std_fix::make_unique<TInspectPP>());
    AddExercise(std_fix::make_unique<TPeakBackground>());
    AddExercise(std_fix::make_unique<TCollectResults>());
    AddExercise(std_fix::make_unique<TCombineResults>());
  }
};
} // namespace Raa

#endif /* end of include guard: CLASSCONTENT_C_TPDPU8OK */
