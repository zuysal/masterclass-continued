#include "Collect.h"

#include <RtypesCore.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TCanvasImp.h>
#include <TClass.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TGNumberEntry.h>
#include <TGraph.h>
#include <TH1.h>
#include <TLatex.h>
#include <TList.h>
#include <TMultiGraph.h>
#include <TObject.h>
#include <TRandom.h>
#include <TRootCanvas.h>
#include <TString.h>
#include <TVirtualPad.h>
#include <TRootBrowser.h>
#include <TGTab.h>

#include "Raa/ContentGUITranslation.h"
#include "Raa/Utilities.h"
#include "Utility/LanguageProvider.h"

class TGWindow;

namespace Raa
{
StudentValue::StudentValue(Int_t id)
  : fP(nullptr)
  , fPMinPt(nullptr)
  , fS(nullptr)
  , fSMinPt(nullptr)
  , fC(nullptr)
  , fCMinPt(nullptr)
  , fOK(false)
  , fId(id)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  fP = MakeGraph("periph", id);
  fPMinPt = MakeGraph("periphMinPt", id);
  fS = MakeGraph("semi", id);
  fSMinPt = MakeGraph("semiMinPt", id);
  fC = MakeGraph("central", id);
  fCMinPt = MakeGraph("centralMinPt", id);
}

TGraph* StudentValue::MakeGraph(const char* prefix, Int_t id)
{
  TString name;
  name.Form("%s_%02d", prefix, id);
  TString title;
  title.Form("%s %d", fTranslation.TitleGroup().Data(), id);
  Color_t c = ChooseColor(id);
  Style_t s = 20 + (id % 10);
  Style_t t = 1 + (id % 8);

  auto* g = new TGraph(1);
  g->SetName(name);
  g->SetTitle(title);
  g->SetLineColor(c);
  g->SetLineStyle(t);
  g->SetMarkerColor(c);
  g->SetMarkerStyle(s);
  g->SetMarkerSize(2);
  return g;
}

void StudentValue::Set(Double_t p, Double_t pMin, Double_t s, Double_t sMin, Double_t c,
                       Double_t cMin)
{
  fP->SetPoint(0, fId, p);
  fPMinPt->SetPoint(0, fId, pMin);
  fS->SetPoint(0, fId, s);
  fSMinPt->SetPoint(0, fId, sMin);
  fC->SetPoint(0, fId, c);
  fCMinPt->SetPoint(0, fId, cMin);
  fOK = true;
}

ValuesInput::ValuesInput()
  : TGTransientFrame(gClient->GetRoot(), gClient->GetRoot(), 100, 100)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  auto* lf = new TGLayoutHints(kLHintsExpandX, 3, 3, 0, 0);
  auto* vf = new TGVerticalFrame(this);

  auto* gf = new TGGroupFrame(vf, fTranslation.Peripheral().Data());
  auto* hf = new TGHorizontalFrame(gf);
  hf->AddFrame(new TGLabel(hf, fTranslation.All().Data()), lf);
  hf->AddFrame(fP = new TGNumberEntryField(hf), lf);
  hf->AddFrame(new TGLabel(hf, "pT>1GeV/c"), lf);
  hf->AddFrame(fPMinPt = new TGNumberEntryField(hf), lf);
  gf->AddFrame(hf, lf);
  vf->AddFrame(gf);

  gf = new TGGroupFrame(vf, fTranslation.SemiCentral());
  hf = new TGHorizontalFrame(gf);
  hf->AddFrame(new TGLabel(hf, fTranslation.All()), lf);
  hf->AddFrame(fS = new TGNumberEntryField(hf), lf);
  hf->AddFrame(new TGLabel(hf, "pT>1GeV/c"), lf);
  hf->AddFrame(fSMinPt = new TGNumberEntryField(hf), lf);
  gf->AddFrame(hf, lf);
  vf->AddFrame(gf);

  gf = new TGGroupFrame(vf, fTranslation.Central().Data());
  hf = new TGHorizontalFrame(gf);
  hf->AddFrame(new TGLabel(hf, fTranslation.All()), lf);
  hf->AddFrame(fC = new TGNumberEntryField(hf), lf);
  hf->AddFrame(new TGLabel(hf, "pT>1GeV/c"), lf);
  hf->AddFrame(fCMinPt = new TGNumberEntryField(hf), lf);
  gf->AddFrame(hf, lf);
  vf->AddFrame(gf);

  hf = new TGHorizontalFrame(vf);
  auto* ca = new TGTextButton(hf, fTranslation.ButtonCancel().Data(), kMBCancel);
  auto* ok = new TGTextButton(hf, "OK", kMBOk);
  hf->AddFrame(ca, lf);
  hf->AddFrame(ok, lf);
  vf->AddFrame(hf, lf);
  ca->Connect("Clicked()", "TGTransientFrame", this, "UnmapWindow()");
  ok->Connect("Clicked()", "Raa::ValuesInput", this, "Take()");
  ca->Associate(this);
  ok->Associate(this);
  AddFrame(vf);

  MapSubwindows();
  Resize(GetDefaultSize());
}

void ValuesInput::Popup(StudentValue* set, Bool_t rndm)
{
  fSet = set;
  fP->SetNumber(rndm ? gRandom->Uniform() : 0);
  fPMinPt->SetNumber(rndm ? gRandom->Uniform() : 0);
  fS->SetNumber(rndm ? gRandom->Uniform() : 0);
  fSMinPt->SetNumber(rndm ? gRandom->Uniform() : 0);
  fC->SetNumber(rndm ? gRandom->Uniform() : 0);
  fCMinPt->SetNumber(rndm ? gRandom->Uniform() : 0);
  MapRaised();
  gClient->WaitForUnmap(this);
}

void ValuesInput::Take()
{
  Printf("Propagating to student set");
  fSet->Set(fP->GetNumber(), fPMinPt->GetNumber(), fS->GetNumber(), fSMinPt->GetNumber(),
            fC->GetNumber(), fCMinPt->GetNumber());
  UnmapWindow();
}

void TCollectResults::CollectDrawMG(TMultiGraph* mg, TVirtualPad* p, Int_t sub, const char* txt)
{
  TVirtualPad* pad = p->cd(sub);
  pad->Clear();
  pad->SetTicks();
  pad->SetRightMargin(0.01);
  mg->Draw("ap"); // , (right ? "Y+" : "")));
  // mg->Print();
  mg->GetHistogram()->SetYTitle(txt);
  mg->GetHistogram()->SetXTitle(Utility::TranslationFromEnv<ContentLanguage>().StudentGroup());
  // mg->GetHistogram()->SetMinimum(0);
  // mg->GetHistogram()->SetMaximum(1.1);
  Double_t tbase = 0.03 / pad->GetHNDC();
  mg->GetHistogram()->GetXaxis()->SetLabelSize(tbase);
  mg->GetHistogram()->GetXaxis()->SetTitleSize(tbase);
  mg->GetHistogram()->GetXaxis()->SetNdivisions(10);
  mg->GetHistogram()->GetYaxis()->SetLabelSize(tbase);
  mg->GetHistogram()->GetYaxis()->SetTitleSize(tbase);
  mg->GetHistogram()->GetYaxis()->SetNdivisions(210);
  mg->GetHistogram()->GetYaxis()->SetTitleOffset(.8); // 1-pad->GetHNDC());
  pad->Modified();
  pad->Update();
  pad->cd();
}

void TCollectResults::CollectAdd(TMultiGraph* mg, TGraph* g)
{
  mg->Add(g);
  if (mg->GetHistogram() != nullptr) {
    // Force re-build histogram
    auto* dummy = new TObject;
    mg->GetListOfGraphs()->Add(dummy);
    mg->RecursiveRemove(dummy);
  }
}

Bool_t TCollectResults::CollectMore(TCanvas* c)
{
  Printf("Check if we need more input");
  const TGWindow* f = gClient->GetRoot();
  if (c->GetCanvasImp()->IsA()->InheritsFrom(TRootCanvas::Class())) {
    f = dynamic_cast<TRootCanvas*>(c->GetCanvasImp());
  }
  Int_t ret = 0;
  const TGUIEnglish& Translation = Utility::TranslationFromEnv<ContentLanguage>();
  new TGMsgBox(gClient->GetRoot(), f, Translation.DiagMoreDataTitle(),
               Translation.DiagMoreDataText(), kMBIconQuestion, kMBYes | kMBNo, &ret);
  return ret == kMBYes;
}

namespace
{
template <typename T>
void SetMargins(T* c, Bool_t Divide)
{
  c->SetTopMargin(0.01);
  c->SetBottomMargin(0.15);
  c->SetRightMargin(0.01);
  c->SetLeftMargin(0.15);

  if (Divide)
    c->Divide(1, 3, 0, 0);
}
} // namespace

void TCollectResults::RunExercise(Bool_t test)
{
  fBrowser = new TRootBrowser(nullptr, "ALICE", 900, 700, "", false);
  fBrowser->DontCallClose();
  fBrowser->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");

  fBrowser->StartEmbedding(TRootBrowser::kRight);
  auto* c = new TCanvas("c", "c", 800, 800);
  SetMargins(c, false);

  c->Divide(2, 1);
  TVirtualPad* left = c->cd(1);
  SetMargins(left, true);

  TVirtualPad* right = c->cd(2);
  SetMargins(right, true);

  left->cd();

  const TGUIEnglish& Translation = Utility::TranslationFromEnv<ContentLanguage>();
  fBrowser->StopEmbedding(Translation.All());
  fBrowser->GetTabRight()->GetTabTab(Translation.All())->ShowClose(kFALSE);

  //Hide left and bottom tabs of the browser
  auto *fV1 = (TGCompositeFrame*)(fBrowser->GetTabLeft()->GetParent());
  auto *fHf = (TGCompositeFrame*)(fV1->GetParent());
  auto *fH2 = (TGCompositeFrame*)(fBrowser->GetTabBottom()->GetParent());
  auto *fV2 = (TGCompositeFrame*)(fH2->GetParent());
  fHf->HideFrame(fV1);
  fV2->HideFrame(fH2);

  fBrowser->MapWindow();

  auto* ltx = new TLatex(.5, .995, Translation.All());
  ltx->SetNDC();
  ltx->SetTextAlign(23);
  ltx->SetTextSize(0.05);
  ltx->SetTextFont(42);
  ltx->Draw();
  right->cd();
  ltx = new TLatex(.5, .995, "#it{p}_{T} > 1 GeV/c");
  ltx->SetNDC();
  ltx->SetTextAlign(23);
  ltx->SetTextSize(0.05);
  ltx->SetTextFont(42);
  ltx->Draw();

  auto* gP = new TMultiGraph("p", "");
  auto* gPMinPt = new TMultiGraph("pMinPt", "");
  auto* gS = new TMultiGraph("s", "");
  auto* gSMinPt = new TMultiGraph("sMinPt", "");
  auto* gC = new TMultiGraph("c", "");
  auto* gCMinPt = new TMultiGraph("cMinPt", "");

  auto* si = new ValuesInput();
  Int_t count = 0;
  do {
    auto* s = new StudentValue(count);
    si->Popup(s, test);

    if (!s->fOK) {
      // Printf("Student set not made");
      delete s;
      // if (!CollectMore(c)) break;
      continue;
    }
    count++;
    Printf("Got student set # %d", count);
    CollectAdd(gP, s->fP);
    CollectAdd(gPMinPt, s->fPMinPt);
    CollectAdd(gS, s->fS);
    CollectAdd(gSMinPt, s->fSMinPt);
    CollectAdd(gC, s->fC);
    CollectAdd(gCMinPt, s->fCMinPt);

    CollectDrawMG(gP, left, 1, Translation.Peripheral());
    CollectDrawMG(gPMinPt, right, 1, Translation.Peripheral());
    CollectDrawMG(gS, left, 2, Translation.SemiCentral());
    CollectDrawMG(gSMinPt, right, 2, Translation.SemiCentral());
    CollectDrawMG(gC, left, 3, Translation.Central());
    CollectDrawMG(gCMinPt, right, 3, Translation.Central());

    c->Modified();
    c->Update();
    c->cd();
    // Printf("wait for canvas");
    // c->WaitPrimitive();
  } while (CollectMore(c));
}
} // namespace Raa
