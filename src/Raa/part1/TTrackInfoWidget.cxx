#include "TTrackInfoWidget.h"

#include <TEveTrack.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <TGTextEntry.h>
#include <TGWindow.h>
#include <gsl/gsl>

#include "Raa/ContentGUITranslation.h"
#include "Raa/part1/TrackClassification.h"

namespace
{
gsl::owner<TGNumberEntryField*> NumberEntryFactory(const TGWindow* parent,
                                                   TGNumberFormat::EStyle style)
{
  auto* tmp = new TGNumberEntryField(parent, -1, 0., style);
  tmp->SetEnabled(false);
  return tmp;
}
} // namespace

namespace Raa
{
void TTrackInfoWidget::SetCounter(const TEveVectorD& p, Double_t pt, Int_t q)
{
  fNumberPX->SetNumber(p.fX);
  fNumberPY->SetNumber(p.fY);
  fNumberPZ->SetNumber(p.fZ);
  fNumberPT->SetNumber(pt);
  fNumberQ->SetIntNumber(q);
}

TTrackInfoWidget::TTrackInfoWidget(const TGWindow* parent)
  : TGMainFrame(parent, 400, 200)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
  , fGroup(new TGGroupFrame(this, fTranslation.ParticleMomentumCharge()))
  , fHeadline(new TGHorizontalFrame(fGroup, 350, 15, kFixedWidth))
  , fLabelPX(new TGLabel(fHeadline, "px"))
  , fLabelPY(new TGLabel(fHeadline, "py"))
  , fLabelPZ(new TGLabel(fHeadline, "pz"))
  , fLabelPT(new TGLabel(fHeadline, "pt"))
  , fLabelQ(new TGLabel(fHeadline, "q"))
  , fInfoLine(new TGHorizontalFrame(fGroup, 350, 15, kFixedWidth))
  , fNumberPX(NumberEntryFactory(fInfoLine, TGNumberFormat::kNESReal))
  , fNumberPY(NumberEntryFactory(fInfoLine, TGNumberFormat::kNESReal))
  , fNumberPZ(NumberEntryFactory(fInfoLine, TGNumberFormat::kNESReal))
  , fNumberPT(NumberEntryFactory(fInfoLine, TGNumberFormat::kNESReal))
  , fNumberQ(NumberEntryFactory(fInfoLine, TGNumberFormat::kNESInteger))
{
  auto* LayoutHints = new TGLayoutHints(kLHintsExpandX | kLHintsCenterX);
  fHeadline->AddFrame(fLabelPX, LayoutHints);
  fHeadline->AddFrame(fLabelPY, LayoutHints);
  fHeadline->AddFrame(fLabelPZ, LayoutHints);
  fHeadline->AddFrame(fLabelPT, LayoutHints);
  fHeadline->AddFrame(fLabelQ, LayoutHints);

  fInfoLine->AddFrame(fNumberPX, LayoutHints);
  fInfoLine->AddFrame(fNumberPY, LayoutHints);
  fInfoLine->AddFrame(fNumberPZ, LayoutHints);
  fInfoLine->AddFrame(fNumberPT, LayoutHints);
  fInfoLine->AddFrame(fNumberQ, LayoutHints);
  // fQ = AddField(cf, lh, true, true);

  auto* ExpandX = new TGLayoutHints(kLHintsExpandX);
  fGroup->AddFrame(fHeadline, ExpandX);
  fGroup->AddFrame(fInfoLine, ExpandX);

  this->AddFrame(fGroup, ExpandX);
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}

void TTrackInfoWidget::UpdateInformation(const TEveTrack* track)
{
  SetCounter(track->GetMomentum(), GetPt(track), track->GetCharge());
}
} // namespace Raa
