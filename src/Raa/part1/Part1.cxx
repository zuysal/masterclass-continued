#include "Part1.h"

#include <RtypesCore.h>
#include <TApplication.h>
#include <TEveManager.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TString.h>
#include <iostream>
#include <memory>
#include <utility>

#include "Raa/ContentGUITranslation.h"
#include "Raa/part1/EventDisplay.h"
#include "Raa/part1/TNavigation.h"
#include "Utility/EventDisplay.h"
#include "Utility/INavigation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h"
#include "Utility/TEventAnalyseGUI.h"
#include "Utility/Utilities.h"

class TGPicture;
class TGWindow;

namespace Raa
{
    RaaDataset::RaaDataset(Bool_t allowAuto)
            : StandardDataset(10, allowAuto, kFALSE)
            , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
    {
      setInstructions(fTranslation.DataSetInstruction());
      setMainWindowTitle(fTranslation.Title());
    }

    std::unique_ptr<Utility::EventDisplay> RaaDataset::createEventDisplay(Bool_t /*demo*/)
    {
      //RAA doesn't have a demo set
      return std_fix::make_unique<Raa::EventDisplay>();
    }

    std::unique_ptr<Utility::INavigation> RaaDataset::createNavigation(Utility::EventDisplay *exercise,
                                                                               Bool_t cheat)
    {
      return std_fix::make_unique<Raa::TNavigation>(exercise, cheat);
    }
} // namespace Raa
