/**
 * @file   /scripts/Part1.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 18:49:26 2017
 *
 * @brief  First part of the master class on RAA
 * @ingroup alice_masterclass_raa_part1
 */

#ifndef PART1_H_R7VSWVUA
#define PART1_H_R7VSWVUA

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TApplication.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGMsgBox.h>
#include <TGTextEntry.h>
#include <TInterpreter.h>
#include <TROOT.h>
#include <TString.h>
#include <TStyle.h>
#include <Utility/Dataset.h>

#include "Raa/ContentGUITranslation.h"
#include "Utility/AbstractMasterClassContent.h"

class TBuffer;
class TClass;
class TGTextButton;
class TGWindow;
class TMemberInspector;

namespace Raa
{
struct TGUIEnglish;
} // namespace Raa

namespace Raa
{
/** This defines the interface to choose the data set to use
 * @image html Raa/doc/Part1Selector.png
 * @ingroup alice_masterclass_raa_part1
 */
class RaaDataset : public Utility::StandardDataset
{
private:
    TGUIEnglish& fTranslation;

    std::unique_ptr<Utility::EventDisplay> createEventDisplay(Bool_t demo) override;
    std::unique_ptr<Utility::INavigation> createNavigation(Utility::EventDisplay* exercise, Bool_t cheat) override;
public:
    RaaDataset(Bool_t allowAuto = false);
};

class TInspectPP : public Utility::TAbstractExercise
{
 public:
  TInspectPP()
    : TAbstractExercise("inspect p-p")
  {
  }

  /// Starts the GUI for selection of the Raa-Dataset which will continue
  /// with the class itself.
  void RunExercise(Bool_t AllowAuto) override { (new RaaDataset(AllowAuto))->Init(); }
};
} // namespace Raa

#endif /* end of include guard: PART1_H_R7VSWVUA */
