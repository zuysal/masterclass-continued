#include "TrackClassification.h"

namespace Raa
{
Bool_t IsPrimaryPositive(const TEveTrack* T) noexcept
{
  Expects(IsPrimary(T));
  return T->GetCharge() > 0;
}

Double_t GetPt(const TEveTrack* T) noexcept
{
  return TMath::Sqrt(T->GetMomentum().fX * T->GetMomentum().fX +
                     T->GetMomentum().fY * T->GetMomentum().fY);
}
} // namespace Raa
