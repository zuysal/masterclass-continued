#include "TStatisticsWidget.h"

#include <TCanvas.h>

namespace Raa
{
TStatisticsWidget::TStatisticsWidget(const TGWindow* parent, const TRaaStatistics& autoStats,
                                     const TRaaStatistics& manualStats)
  : TGMainFrame(parent)
  , fAutomaticStats(autoStats)
  , fManualStats(manualStats)
  , fCanvas(new TRootEmbeddedCanvas("Statistics Canvas", this))
{
  this->AddFrame(fCanvas, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  fCanvas->GetCanvas()->Divide(2, 3);
  RedrawAll();

  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}

void TStatisticsWidget::RedrawAll() noexcept
{
  gsl::not_null<TCanvas*> c = fCanvas->GetCanvas();
  TVirtualPad* p = nullptr;

#define REDRAW_HIST(PadId, HistMethod)        \
  p = c->cd((PadId));                         \
  fAutomaticStats.HistMethod().DrawCopy();    \
  fManualStats.HistMethod().DrawCopy("same"); \
  p->Modified();

  REDRAW_HIST(1, HistMultiplicity)
  REDRAW_HIST(2, HistMultiplicityMinPt)
  REDRAW_HIST(3, HistMultiplicity)
  REDRAW_HIST(4, HistCharge)
  REDRAW_HIST(5, HistMultiplicitySecondaries)
  REDRAW_HIST(6, HistPhi)

#undef REDRAW_HIST
  c->Modified();
  c->Update();
  c->cd();
}

} // namespace Raa
