/**
 * @file   Log.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 17 14:22:02 2017
 *
 * @brief  Log widget for second part of the exercise
 *
 * @ingroup alice_masterclass_raa_part2
 */
#ifndef RAALOG_C
#define RAALOG_C
#include <Rtypes.h>
#include <TGFrame.h>
#include <TGTextView.h>
#include <TString.h>
#include <TSystem.h>

class TBuffer;
class TClass;
class TGTextView;
class TMemberInspector;

namespace Raa
{
/**
 * Log view widget.
 *
 * @ingroup alice_masterclass_raa_part2
 */
struct Log : public TGMainFrame {
  TGTextView* fView;
  TString fFile;
  Log();

  ClassDef(Log, 0);
};

/**
 * Guard for writing to log widget.
 *
 * @ingroup alice_masterclass_raa_part2
 */
struct Guard {
  Log* fLog;

  Guard(Log* log);
  ~Guard();
};
} // namespace Raa
#endif
