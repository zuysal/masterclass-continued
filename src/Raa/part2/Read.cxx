#include "Read.h"

#include <GuiTypes.h>
#include <RtypesCore.h>
#include <TArrayD.h>
#include <TAxis.h>
#include <TCollection.h>
#include <TColor.h>
#include <TFile.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGColorSelect.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TGNumberEntry.h>
#include <TGTextEntry.h>
#include <TH1.h>
#include <THStack.h>
#include <TList.h>
#include <TNamed.h>
#include <TObjArray.h>
#include <TObject.h>
#include <TString.h>
#include <gsl/gsl>

#include "Raa/ContentGUITranslation.h"
#include "Raa/Utilities.h"
#include "Utility/LanguageProvider.h"
#include "Utility/Utilities.h"

namespace Raa
{
Row::Row(TGCompositeFrame* p, THStack* stack)
  : TGGroupFrame(p, "", kHorizontalFrame)
  , fStack(stack)
  , fCBin(nullptr)
  , fPtBin(nullptr)
  , fValue(nullptr)
  , fError(nullptr)
  , fOK(false)
  , fPt(-1)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  auto* lh = new TGLayoutHints(kLHintsExpandX, 2, 0, 2, 2);
  TGGroupFrame* cf = this;

  fCBin = new TGComboBox(cf);
  fCBin->AddEntry(fTranslation.SelectBin(), 0);
  SetupBinComboBox(*fCBin);
  fCBin->Select(0, kFALSE);
  cf->AddFrame(fCBin);

  fPtBin = new TGComboBox(cf);
  fPtBin->AddEntry(fTranslation.SelectpTBin(), 0);
  TArrayD ptBins = PtBins();
  // Note, we start from bin 3 since the lower 3 bins are not filled!
  for (Int_t i = 3; i < ptBins.GetSize() - 1; i++) {
    TString bin;
    bin.Form("%5.2f-%5.2f", ptBins[i], ptBins[i + 1]);
    fPtBin->AddEntry(bin.Data(), i + 1);
  }
  fPtBin->Resize(150, 20);
  fPtBin->Select(0, kFALSE);
  cf->AddFrame(fPtBin);

  TGNumberFormat::EStyle sty = TGNumberFormat::kNESRealFour;
  cf->AddFrame(new TGLabel(cf, Form("%s:", fTranslation.Value().Data())), lh);
  cf->AddFrame(fValue = new TGNumberEntryField(cf, -1, 0, sty));
  cf->AddFrame(new TGLabel(cf, "+/-:"));
  cf->AddFrame(fError = new TGNumberEntryField(cf, -1, 0, sty));
  fValue->SetEnabled(false);
  fError->SetEnabled(false);

  auto* but = new TGTextButton(cf, fTranslation.ButtonFetch());
  but->Connect("Clicked()", "Raa::Row", this, "Fetch()");
  cf->AddFrame(but, new TGLayoutHints(kLHintsRight, 2, 2, 2, 2));
  // MapSubwindows();
  // Resize(GetDefaultSize());
  p->AddFrame(this, lh);
}

void Row::Fetch()
{
  Int_t cBin = fCBin->GetSelected();
  Int_t ptBin = fPtBin->GetSelected();
  if (cBin < 1 || ptBin < 1) {
    new TGMsgBox(gClient->GetRoot(), this, fTranslation.DiagSelectBinFirstTitle(),
                 fTranslation.DiagSelectBinFirstText());
    return;
  }
  auto* h = dynamic_cast<TH1*>(fStack->GetHists()->At(cBin - 1));
  if (h == nullptr) {
    new TGMsgBox(gClient->GetRoot(), this, fTranslation.DiagNoHistogramTitle(),
                 Form("%s %d", fTranslation.DiagNoHistogramText().Data(), cBin));
    return;
  }
  Double_t val = h->GetBinContent(ptBin);
  Double_t err = h->GetBinError(ptBin);
  fPt = h->GetXaxis()->GetBinCenter(ptBin);
  fValue->SetNumber(val);
  fError->SetNumber(err);
  Pixel_t wht;
  gClient->GetColorByName("lightgreen", wht);
  fValue->SetBackgroundColor(wht);
  fError->SetBackgroundColor(wht);
  Printf("Centrality bin %2d, pT bin %2d (%5.2f): %5.2f +/- %5.2f", cBin, ptBin, fPt, val, err);
  fOK = true;
  gClient->NeedRedraw(this);
}

Values::Values(THStack* stack)
  : TGMainFrame(gClient->GetRoot(), 100, 100)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  auto* vf = new TGVerticalFrame(this);
  auto* gf = new TGGroupFrame(vf, fTranslation.Who());
  auto* hf = new TGHorizontalFrame(gf);
  fWho = new TGTextEntry(hf, "");
  hf->AddFrame(fWho, new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2));
  fColor = new TGColorSelect(hf);
  hf->AddFrame(fColor, new TGLayoutHints(kLHintsNormal, 2, 2, 2, 2));
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX));
  vf->AddFrame(gf, new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2));

  for (Int_t i = 0; i < 10; i++) {
    fRows.Add(new Row(vf, stack));
  }
  auto* ok = new TGTextButton(vf, fTranslation.ButtonSave());
  ok->Connect("Clicked()", "Raa::Values", this, "Save()");
  vf->AddFrame(ok, new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2));

  auto* cl = new TGTextButton(vf, fTranslation.ButtonClose());
  cl->Connect("Clicked()", "TGMainFrame", this, "UnmapWindow()");
  vf->AddFrame(cl, new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2));

  AddFrame(vf, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}
/**
 * Saves read values to a file
 */
void Values::Save()
{
  TString who(fWho->GetText());
  TString out = EscapeWho(who);
  if (out.IsNull()) {
    new TGMsgBox(gClient->GetRoot(), this, fTranslation.DiagNoGroupNameTitle(),
                 fTranslation.DiagNoGroupNameText());
    return;
  }
  out.Append(".root");
  Color_t color = TColor::GetColor(fColor->GetColor());
  Printf("Will write to %s for %s with color %d", out.Data(), who.Data(), color);
  TList histos;
  Row* row = nullptr;
  TIter next(&fRows);
  TObjArray cb;
  while ((row = dynamic_cast<Row*>(next())) != nullptr) {
    if (!row->fOK) {
      continue;
    }
    Int_t cBin = row->fCBin->GetSelected();
    Int_t ptBin = row->fPtBin->GetSelected();
    Double_t val = row->fValue->GetNumber();
    Double_t err = row->fError->GetNumber();
    Double_t pt = row->fPt;
    TString nme = EncodePtHistName(pt);
    Printf("Row centrality %2d, pT %2d (%5.2f): %5.2f +/- %5.2f", cBin, ptBin, pt, val, err);
    auto* hist = dynamic_cast<TH1*>(histos.FindObject(nme));
    if (hist == nullptr) {
      histos.Add(hist = MakeCentHistogram(color, pt, who));
    }
    hist->SetBinContent(cBin, val);
    hist->SetBinError(cBin, err);
    TIter nextC(&cb);
    TObject* oc = nullptr;
    while ((oc = nextC()) != nullptr) {
      if (oc->GetUniqueID() == gsl::narrow<UInt_t>(cBin)) {
        break;
      }
      if (oc == nullptr) {
        oc = new TObject;
        oc->SetUniqueID(cBin);
        cb.Add(oc);
      }
    }
    TFile* output = TFile::Open(out, "RECREATE");
    auto* owho = new TNamed("who", who.Data());
    owho->Write();
    histos.Write();
    cb.Write("cbins", TObject::kSingleKey);
    output->Write();

    Printf("Output written in %s", out.Data());
  }
}

InstructionsGUI::InstructionsGUI()
  : TGMainFrame(gClient->GetRoot(), 1, 1, kVerticalFrame)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  auto* hf = new TGHtml(this, 400, 450);
  AddFrame(hf, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 2, 2, 2));
  hf->ParseText(const_cast<char*>(fTranslation.PathReadValues().Data()));

  auto* tb = new TGTextButton(this, "Close");
  AddFrame(tb, new TGLayoutHints(kLHintsExpandX, 2, 2, 0, 2));
  tb->Connect("Clicked()", "TGMainFrame", this, "UnmapWindow()");
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}

void Read()
{
  TFile* file = OpenResult(true);
  auto* raa = Get<THStack>(file, "raa");
  if ((file == nullptr) || (raa == nullptr)) {
    auto Translation = Utility::TranslationFromEnv<ContentLanguage>();
    new TGMsgBox(gClient->GetRoot(), gClient->GetRoot(), Translation.DiagMissingInputTitle(),
                 Translation.DiagMissingInputText());
    return;
  }
  new Values(raa);
  new InstructionsGUI();
}
} // namespace Raa
