#include "Exercise.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGClient.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TInterpreter.h>
#include <TROOT.h>
#include <TRootBrowser.h>
#include <TString.h>
#include <TSystem.h>
#include <iostream>
#include <fstream>

#include "Raa/ContentGUITranslation.h"
#include "Raa/part2/Control.h"
#include "Raa/part2/Editor.h"
#include "Raa/part2/Log.h"
#include "Utility/LanguageProvider.h"

namespace Raa
{

void copy_file(TString from, TString to) {
 std::ifstream source(from.Data(), std::ios::binary);
 std::ofstream dest(to.Data(), std::ios::binary);

 dest << source.rdbuf();

 source.close();
 dest.close();
}

Exercise::Exercise()
  : fPath(Utility::ResourcePath("macros/Analyse.C"))
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
    //Check if we are sandboxed in an AppImage
    if(gSystem->Getenv("APPIMAGE") != nullptr) {
        fIsSandboxed = kTRUE;
        std::cout << "AppImage Sandbox detected." << std::endl;
    } else {
        fIsSandboxed = kFALSE;
    }

    if(fIsSandboxed) {
        TString base = "macros";
        TString homepath = TString(gSystem->Getenv("HOME")) + "/";
        gSystem->mkdir(homepath + "Raa");
        copy_file(Utility::ResourcePath(base + "Raa/Final.C"), homepath + "Raa/Final.C");
        copy_file(Utility::ResourcePath(base + "Raa/AnalyseFull.C"), homepath + "Raa/AnalyseFull.C");
        copy_file(Utility::ResourcePath(base + "Raa/Utilities.C"), homepath + "Raa/Utilities.C");
        copy_file(Utility::ResourcePath(base + "ResourceDir.h"), homepath + "ResourceDir.h");
    }
}

void Exercise::Setup(TRootBrowser* browser, Bool_t cheat)
{
  browser->StartEmbedding(TRootBrowser::kLeft);
  fControl = new Control(this, cheat);
  browser->StopEmbedding(fTranslation.Control());

  browser->StartEmbedding(TRootBrowser::kRight);
  fEditor = new Editor(OrigFileName(), browser->GetStatusBar());
  fEditor->Connect("Compile()", "Raa::Exercise", this, "Compile(=1)");
  fEditor->Connect("Run()", "Raa::Exercise", this, "Run(=1)");
  if (cheat) {
    ToggleCheat(cheat);
  }
  browser->StopEmbedding(fTranslation.Editor(), new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  browser->StartEmbedding(TRootBrowser::kBottom);
  fLog = new Log();
  browser->StopEmbedding("Log");

  browser->SetTab(TRootBrowser::kRight, 0);

  // Instead of closing the window, terminate the whole app
  browser->DontCallClose();
  browser->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");
}

void Exercise::Compile(Int_t mode)
{
  if (mode < 0) {
    mode = fControl->fMode;
  }

  TString prefix;

  if(fIsSandboxed) {
    prefix = TString(gSystem->Getenv("HOME")) + "/";
  } else {
    prefix = Utility::ResourcePath("macros") + "/";
  }

  TString script;
  if (mode == 2) {
    script = "Raa/Final.C";
  } else if (fControl->fCent == 9) {
    script = "Raa/AnalyseFull.C";
  } else {
    script = FileName();
  }

  script = prefix + script;

  Printf("Compiling %s", script.Data());
  if(fIsSandboxed)
      gSystem->AddIncludePath("-I\"" + TString(gSystem->Getenv("HOME")) + "\"");

  Bool_t ret = false;
  {
    Guard g(fLog);
    Printf("Compiling \"%s\"", script.Data());
    gDebug = 5;
    ret = (gSystem->CompileMacro(script) != 0);
    gDebug = 0;
  }
  if (!ret) {
    new TGMsgBox(gClient->GetRoot(), fControl, "TGTextEditor",
                 Form("Compilation of script %s failed", script.Data()), kMBIconExclamation);
  }
}

void Exercise::Run(Int_t mode)
{
  Printf("Running %s", FileName().Data());
  if (mode < 0) {
    mode = fControl->fMode;
  }
  if (mode != 1 && mode != 2) {
    new TGMsgBox(gClient->GetRoot(), fControl, fTranslation.DiagNoModeTitle(),
                 fTranslation.DiagNoModeText());
    return;
  }
  TString func = (mode == 1 ? "Analyse" : "Final");
  TString call(func);
  call.Append("()");
  Int_t minC = 0;
  Int_t maxC = 100;
  if (mode == 1) {
    switch (fControl->fCent) {
      case 100:
        new TGMsgBox(gClient->GetRoot(), fControl, fTranslation.DiagNoBinTitle(),
                     fTranslation.DiagNoBinText());
        return;
      case 0:
        minC = 0;
        maxC = 5;
        break;
      case 1:
        minC = 5;
        maxC = 10;
        break;
      case 2:
        minC = 10;
        maxC = 20;
        break;
      case 3:
        minC = 20;
        maxC = 30;
        break;
      case 4:
        minC = 30;
        maxC = 40;
        break;
      case 5:
        minC = 40;
        maxC = 50;
        break;
      case 6:
        minC = 50;
        maxC = 60;
        break;
      case 7:
        minC = 60;
        maxC = 70;
        break;
      case 8:
        minC = 70;
        maxC = 80;
        break;
      case 9:
        func = "AnalyseFull";
        break;
    }
    if (fControl->fCent == 9) {
      call = func;
      call.Append("()");
    } else {
      call.ReplaceAll("()", Form("(%d,%d)", minC, maxC));
    }
  }

  Printf("Executing %s", call.Data());
  Guard g(fLog);
  gInterpreter->SaveContext();

  gROOT->SetExecutingMacro(kTRUE);
  std::cerr << "Running Raa Exercise with following Call:\n";
  std::cerr << call << std::endl;
  gROOT->ProcessLine(call);
  gROOT->SetExecutingMacro(kFALSE);

  TString file(func);
  file.Append(".C");
  if (gInterpreter->IsLoaded(file.Data())) {
    gInterpreter->UnloadFile(file.Data());
  }

  gInterpreter->Reset();
}

void Exercise::Go()
{
  Compile(-1);
  Run(-1);
}
} // namespace Raa
