#include "Editor.h"

#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGDimension.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TGMenu.h>
#include <TGMsgBox.h>
#include <TGStatusBar.h>
#include <TGText.h>
#include <TGTextEdit.h>
#include <TGTextEditDialogs.h>
#include <TGToolBar.h>
#include <TString.h>
#include <TSystem.h>
#include <WidgetMessageTypes.h>

#include "Raa/ContentGUITranslation.h"
#include "Utility/LanguageProvider.h"

namespace Raa
{
Editor::Editor(const char* filename, TGStatusBar* status)
  : TGMainFrame(gClient->GetRoot(), 10, 10, kVerticalFrame)
  , fFileMenu(nullptr)
  , fEditMenu(nullptr)
  , fSearchMenu(nullptr)
  , fToolMenu(nullptr)
  , fToolBar(nullptr)
  , fEdit(nullptr)
  , fStatus(status)
  , fFileName(gSystem->BaseName(filename))
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  //If we are sandboxed in an AppImage, save the file in the home directory
  if(gSystem->Getenv("APPIMAGE") != nullptr)
    fFileName = TString(gSystem->Getenv("HOME")) + "/" + fFileName;

  TGClient* client = GetClient();
  auto* lh = new TGLayoutHints(kLHintsTop | kLHintsExpandX, 1, 1, 1, 1);
  fFileMenu = new TGPopupMenu(client->GetDefaultRoot());
  fFileMenu->AddEntry(fTranslation.MenuSave().Data(), kSave);
  fFileMenu->Connect("Activated(Int_t)", "Raa::Editor", this, "Menu(Int_t)");
  fFileMenu->DisableEntry(kSave);

  fEditMenu = new TGPopupMenu(client->GetDefaultRoot());
  fEditMenu->AddEntry(fTranslation.MenuCut(), kCut);
  fEditMenu->AddEntry(fTranslation.MenuCopy(), kCopy);
  fEditMenu->AddEntry(fTranslation.MenuPaste(), kPaste);
  fEditMenu->AddEntry(fTranslation.MenuDelete(), kDelete);
  fEditMenu->Connect("Activated(Int_t)", "Raa::Editor", this, "Menu(Int_t)");
  fEditMenu->DisableEntry(kCut);
  fEditMenu->DisableEntry(kCopy);
  // fEditMenu->DisableEntry(kPaste);
  fEditMenu->DisableEntry(kDelete);

  fSearchMenu = new TGPopupMenu(client->GetDefaultRoot());
  fSearchMenu->AddEntry(fTranslation.MenuFind(), kFind);
  fSearchMenu->AddEntry(fTranslation.MenuFindNext(), kFindNext);
  fSearchMenu->AddSeparator();
  fSearchMenu->AddEntry(fTranslation.MenuGotoLine(), kGoto);
  fSearchMenu->Connect("Activated(Int_t)", "Raa::Editor", this, "Menu(Int_t)");

  fToolMenu = new TGPopupMenu(client->GetDefaultRoot());
  fToolMenu->AddEntry(fTranslation.MenuCompileMacro(), kCompile);
  fToolMenu->AddEntry(fTranslation.MenuExecuteMacro(), kExecute);
  fToolMenu->AddEntry(fTranslation.MenuInterrupt(), kInterrupt);
  fToolMenu->Connect("Activated(Int_t)", "Raa::Editor", this, "Menu(Int_t)");
  fToolMenu->DisableEntry(kInterrupt);

  auto* mlh = new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
  auto* menuBar = new TGMenuBar(this, 60, 1, kHorizontalFrame);
  menuBar->SetCleanup(kDeepCleanup);
  menuBar->AddPopup(fTranslation.ToolBarFile(), fFileMenu, mlh);
  menuBar->AddPopup(fTranslation.ToolBarEdit(), fEditMenu, mlh);
  menuBar->AddPopup(fTranslation.ToolBarSearch(), fSearchMenu, mlh);
  menuBar->AddPopup(fTranslation.ToolBarTools(), fToolMenu, mlh);
  AddFrame(menuBar, lh);

  ToolBarData_t tbData[] = {
    { "ed_save.png", fTranslation.ToolBarDataSave(), false, kSave, nullptr },
    { "", nullptr, false, -1, nullptr },
    { "ed_cut.png", fTranslation.ToolBarDataCut(), false, kCut, nullptr },
    { "ed_copy.png", fTranslation.ToolBarDataCopy(), false, kCopy, nullptr },
    { "ed_paste.png", fTranslation.ToolBarDataPaste(), false, kPaste, nullptr },
    { "ed_delete.png", fTranslation.ToolBarDataDelete(), false, kDelete, nullptr },
    { "", nullptr, false, -1, nullptr },
    { "ed_find.png", fTranslation.ToolBarDataFind(), false, kFind, nullptr },
    { "ed_findnext.png", fTranslation.ToolBarDataFindNext(), false, kFindNext, nullptr },
    { "ed_goto.png", fTranslation.ToolBarDataGoto(), false, kGoto, nullptr },
    { "", nullptr, false, -1, nullptr },
    { "ed_compile.png", fTranslation.ToolBarDataCompile(), false, kCompile, nullptr },
    { "ed_execute.png", fTranslation.ToolBarDataExecute(), false, kExecute, nullptr },
    { "ed_interrupt.png", fTranslation.ToolBarDataInterrupt(), false, kInterrupt, nullptr },
    { nullptr, nullptr, false, 0, nullptr }
  };

  fToolBar = new TGToolBar(this, 60, 1, kHorizontalFrame);
  fToolBar->SetCleanup(kDeepCleanup);
  fToolBar->Connect("Clicked(Int_t)", "Raa::Editor", this, "Menu(Int_t)");
  Int_t spacing = 8;
  for (Int_t i = 0; tbData[i].fPixmap != nullptr; i++) {
    if (tbData[i].fPixmap[0] == '\0') {
      spacing = 8;
      continue;
    }
    fToolBar->AddButton(this, &tbData[i], spacing);
    switch (tbData[i].fId) {
      case kCut:
      case kCopy:
        // case kPaste:
      case kDelete:
      case kSave:
      case kInterrupt:
        fToolBar->GetButton(tbData[i].fId)->SetEnabled(false);
        break;
    }
    spacing = 0;
  }
  AddFrame(fToolBar, lh);

  fEdit = new TGTextEdit(this, 10, 10, 1);
  AddFrame(fEdit, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  fEdit->SetFocus();
  fEdit->GetMenu()->DisableEntry(TGTextEdit::kM_FILE_SAVE);
  fEdit->GetMenu()->DeleteEntry(TGTextEdit::kM_FILE_NEW);
  fEdit->GetMenu()->DeleteEntry(TGTextEdit::kM_FILE_OPEN);
  fEdit->GetMenu()->DeleteEntry(TGTextEdit::kM_FILE_CLOSE);
  fEdit->GetMenu()->DeleteEntry(TGTextEdit::kM_FILE_SAVEAS);
  fEdit->GetMenu()->DeleteEntry(TGTextEdit::kM_FILE_PRINT);
  fEdit->GetMenu()->DeleteEntry(TGTextEdit::kM_EDIT_SELECTALL);
  fEdit->MapWindow();
  Load(filename, false);
  fEdit->Connect("DataChanged()", "Raa::Editor", this, "Changed()");
  fEdit->Connect("Marked(Bool_t)", "Raa::Editor", this, "Marked(Bool_t)");

  ShowPos();

  MapSubwindows();
  Resize(GetDefaultWidth() + 50, GetDefaultHeight() > 500 ? GetDefaultHeight() : 500);
  Layout();
  MapWindow();
}

void Editor::Load(const char* file, Bool_t cheat)
{
  const char* toLoad = (file != nullptr ? file : fFileName.Data());
  Printf("Loading file %s (%s)", toLoad, file);
  fEdit->LoadFile(toLoad);
  if (!cheat) {
    EditForStudent();
  }
  Save(); // Save to working directory
}

void Editor::EditForStudent()
{
  fEdit->SetInsertMode(TGTextEdit::kReplace);
  Int_t lines[] = { 74, 77, 79, -1 };
  Int_t* pline = lines;
  while (*pline > 0) {
    fEdit->Goto(*pline);
    TGTextLine* lne = fEdit->GetText()->GetCurrentLine();
    lne->DelText(4, lne->GetLineLength() - 4);
    pline++;
  }
  fEdit->Goto(0, 0);
  fEdit->Goto(0, 0);
  fEdit->SetInsertMode(TGTextEdit::kInsert);
}

void Editor::Menu(Int_t id)
{
  switch (id) {
    case kSave:
      Save();
      break;
    case kCut:
      fEdit->Cut();
      break;
    case kCopy:
      fEdit->Copy();
      break;
    case kPaste:
      fEdit->Paste();
      break;
    case kDelete:
      fEdit->Delete();
      break;
    case kFind:
      Search(false);
      break;
    case kFindNext:
      Search(true);
      break;
    case kGoto:
      Goto();
      break;
    case kCompile:
      Compile();
      break;
    case kExecute:
      Run();
      break;
    case kInterrupt:
      Interrupt();
      break;
  }
}

void Editor::Compile()
{
  Bool_t ok = IsSaved();
  if (!ok) {
    return;
  }
  if (fEdit->ReturnLineCount() < 3) {
    return;
  }
  Emit("Compile()");
  // fExercise->Compile();
}

void Editor::Run()
{
  Bool_t ok = IsSaved();
  if (!ok) {
    return;
  }
  fToolMenu->EnableEntry(kInterrupt);
  fToolBar->GetButton(kInterrupt)->SetEnabled(true);

  Emit("Run()");
  // fExercise->Run();

  fToolMenu->DisableEntry(kInterrupt);
  fToolBar->GetButton(kInterrupt)->SetEnabled(false);
}

void Editor::Interrupt()
{
  fToolMenu->DisableEntry(kInterrupt);
  fToolBar->GetButton(kInterrupt)->SetEnabled(false);
}

void Editor::Search(Bool_t again)
{
  if (again) {
    SendMessage(fEdit, MK_MSG(kC_COMMAND, kCM_MENU), TGTextEdit::kM_SEARCH_FINDAGAIN, 0);
  } else {
    fEdit->Search(kFALSE);
  }
}

void Editor::Changed()
{
  fEdit->GetMenu()->EnableEntry(TGTextEdit::kM_FILE_SAVE);
  fToolBar->GetButton(kSave)->SetEnabled(true);
  fFileMenu->EnableEntry(kSave);
  fStatus->SetText(Form("%s*", fFileName.Data()), 0);
  ShowPos();
}

Bool_t Editor::IsSaved()
{
  if (!fFileMenu->IsEntryEnabled(kSave)) {
    return true;
  }
  Int_t ret;
  new TGMsgBox(gClient->GetRoot(), this, "TGTextEditor", fTranslation.DiagSaveChanges(),
               kMBIconExclamation, (kMBYes | kMBNo | kMBCancel), &ret);
  if (ret == kMBYes) {
    Save();
  }
  return ret != kMBCancel;
}

void Editor::Save()
{
  if (!fEdit->SaveFile(fFileName)) {
    new TGMsgBox(gClient->GetRoot(), this, "TGTextEditor",
                 Form("%s \"%s\"", fTranslation.DiagErrorSaving().Data(), fFileName.Data()),
                 kMBIconExclamation, kMBOk);
    return;
  }
  fEdit->GetMenu()->DisableEntry(TGTextEdit::kM_FILE_SAVE);
  fToolBar->GetButton(kSave)->SetEnabled(false);
  fFileMenu->DisableEntry(kSave);
  if (fStatus != nullptr) {
    fStatus->SetText(Form("%s", fFileName.Data()), 0);
  }
}

void Editor::Goto()
{
  Long_t ret;
  new TGGotoDialog(gClient->GetRoot(), this, 400, 150, &ret);
  if (ret >= 0) {
    fEdit->Goto(ret - 1);
  }
}

void Editor::Marked(Bool_t on)
{
  if (on) {
    fEdit->GetMenu()->EnableEntry(TGTextEdit::kM_EDIT_CUT);
    fEdit->GetMenu()->EnableEntry(TGTextEdit::kM_EDIT_COPY);
    fFileMenu->EnableEntry(kCut);
    fFileMenu->EnableEntry(kCopy);
    fFileMenu->EnableEntry(kDelete);
  } else {
    fEdit->GetMenu()->DisableEntry(TGTextEdit::kM_EDIT_CUT);
    fEdit->GetMenu()->DisableEntry(TGTextEdit::kM_EDIT_COPY);
    fFileMenu->DisableEntry(kCut);
    fFileMenu->DisableEntry(kCopy);
    fFileMenu->DisableEntry(kDelete);
  }
  fToolBar->GetButton(kCut)->SetEnabled(on);
  fToolBar->GetButton(kCopy)->SetEnabled(on);
  fToolBar->GetButton(kDelete)->SetEnabled(on);
  ShowPos();
}

void Editor::ShowPos()
{
  if (fStatus == nullptr) {
    return;
  }
  TGLongPosition p = fEdit->GetCurrentPos();
  fStatus->SetText(Form("L%ld/%ld, C%ld", p.fY + 1, fEdit->ReturnLineCount(), p.fX), 2);
  fStatus->SetText(fEdit->GetInsertMode() == TGTextEdit::kReplace ? "INS" : "", 1);
}
} // namespace Raa
