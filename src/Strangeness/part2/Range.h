/**
 * @file   StrangeRange.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 17 15:28:29 2017
 *
 * @brief  A range widget
 *
 * @ingroup alice_masterclass_str_part2
 */
#ifndef STRANGERANGE_C
#define STRANGERANGE_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGDoubleSlider.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>

class TBuffer;
class TClass;
class TGDoubleSlider;
class TGWindow;
class TMemberInspector;

namespace Strangeness
{
/**
 * A widget to set a range.  It contains
 *
 * - A label
 * - Input widget for lower bound
 * - A slider
 * - Input widget for upper bound
 *
 * @ingroup alice_masterclass_str_part2
 */
struct Range : public TGHorizontalFrame {
  TGDoubleSlider* fRange;
  TGNumberEntry* fLow;
  TGNumberEntry* fHigh;
  Range(const char* name, Double_t min, Double_t max, TGWindow* p = nullptr, Int_t w = 1,
        Int_t h = 1);

  /**
   * @return Least value
   */
  Float_t Min() const { return fLow->GetNumber(); }

  /**
   * @return Largest value
   */
  Float_t Max() const { return fHigh->GetNumber(); }

  /**
   * @param min The new least value
   */
  void SetMin(Float_t min);

  /**
   * @param max The new larges value
   */
  void SetMax(Float_t max);

  /**
   * @param min The new least value
   * @param max The new largest value
   */
  void Set(Float_t min, Float_t max);

  /**
   * Update when least value is changed
   */
  void MinChanged();

  /**
   * Update when largest value is changed
   */
  void MaxChanged();

  /**
   * Update when range is changed
   */
  void Changed();
  ClassDef(Range, 0);
};
} // namespace Strangeness

#endif
