#include "Range.h"

#include <RtypesCore.h>
#include <TGDoubleSlider.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGNumberEntry.h>

class TGWindow;

namespace Strangeness
{
Range::Range(const char* name, Double_t min, Double_t max, TGWindow* p, Int_t w, Int_t h)
  : TGHorizontalFrame(p, w, h)
  , fRange(nullptr)
  , fLow(nullptr)
  , fHigh(nullptr)
{
  auto* lh = new TGLayoutHints(kLHintsLeft | kLHintsExpandX, 2, 2, 0, 0);
  auto* hf = new TGHorizontalFrame(this, 120, 1, kFixedWidth);
  hf->AddFrame(new TGLabel(hf, name));
  AddFrame(hf, new TGLayoutHints(kLHintsLeft));
  AddFrame(fLow = new TGNumberEntry(this, min), lh);
  AddFrame(fRange = new TGDoubleHSlider(this, .5 * GetWidth(), kDoubleScaleBoth), lh);
  AddFrame(fHigh = new TGNumberEntry(this, max), lh);
  fLow->SetFormat(TGNumberEntry::kNESRealFour);
  fHigh->SetFormat(TGNumberEntry::kNESRealFour);
  fLow->SetLimits(TGNumberEntry::kNELLimitMinMax, min, max);
  fHigh->SetLimits(TGNumberEntry::kNELLimitMinMax, min, max);
  fRange->SetRange(min, max);
  fRange->SetPosition(min, max);
  fLow->Connect("ValueSet(Long_t)", "Strangeness::Range", this, "MinChanged()");
  fHigh->Connect("ValueSet(Long_t)", "Strangeness::Range", this, "MaxChanged()");
  fRange->Connect("PositionChanged()", "Strangeness::Range", this, "Changed()");
}

void Range::SetMin(Float_t min)
{
  fLow->SetNumber(min);
  MinChanged();
}

void Range::SetMax(Float_t max)
{
  fHigh->SetNumber(max);
  MaxChanged();
}

void Range::Set(Float_t min, Float_t max)
{
  SetMin(min);
  SetMax(max);
}

void Range::MinChanged()
{
  Float_t low = Min();
  Float_t high = Max();
  if (low > high) {
    high = low;
    fHigh->SetNumber(high);
  }
  fRange->SetPosition(low, high);
}

void Range::MaxChanged()
{
  Float_t low = Min();
  Float_t high = Max();
  if (high < low) {
    low = high;
    fLow->SetNumber(low);
  }
  fRange->SetPosition(low, high);
}

void Range::Changed()
{
  Float_t min, max;
  fRange->GetPosition(min, max);
  fLow->SetNumber(min);
  fHigh->SetNumber(max);
}
} // namespace Strangeness
