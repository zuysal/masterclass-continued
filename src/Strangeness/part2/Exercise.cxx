#include "Exercise.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TError.h>
#include <TF1.h>
#include <TFile.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <TGButton.h>
#include <TGListTree.h>
#include <TGraphErrors.h>
#include <TH1.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TList.h>
#include <TMath.h>
#include <TMathBase.h>
#include <TMatrixDSymfwd.h>
#include <TMatrixTSym.h>
#include <TMultiGraph.h>
#include <TNtuple.h>
#include <TObject.h>
#include <TPaveStats.h>
#include <TROOT.h>
#include <TRootBrowser.h>
#include <TSeqCollection.h>
#include <TString.h>
#include <TVirtualPad.h>
#include <TStyle.h>
#include <TRootEmbeddedCanvas.h>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part2/Fitter.h"
#include "Strangeness/part2/Picker.h"
#include "Strangeness/part2/Range.h"
#include "Utility/LanguageProvider.h"

namespace Strangeness
{
Exercise::Exercise()
  : fTranslation(Utility::TranslationFromEnv<ClassLanguage>())
{
  fFile = DataFile("InvariantMass.root");

  fRaw = new TMultiGraph("raw", fTranslation.RawMeasurements());
  fYield = new TMultiGraph("yield", fTranslation.KeyYield());
  fEnhancement = new TMultiGraph("enhancement", fTranslation.StrangenessEnhancement());
  fMass = new TMultiGraph("mass", Form("#LT%s#GT", fTranslation.InvMass().Data()));
  fWidth = new TMultiGraph("width", Form("#sigma_{%s}", fTranslation.InvMass().Data()));
}

void Exercise::Setup(TRootBrowser* browser, Bool_t cheat)
{
  fAutoRange = cheat;
  fBrowser = browser;
  browser->StartEmbedding(TRootBrowser::kLeft);
  fPicker = new Picker(fFile, this, cheat);
  browser->StopEmbedding(fTranslation.Control());

  browser->StartEmbedding(TRootBrowser::kRight);
  fPlot = new TCanvas();
  fPlot->SetRightMargin(0.01);
  fPlot->SetTopMargin(0.01);
  browser->StopEmbedding(fTranslation.InvMass());
  browser->GetTabRight()->GetTabTab(fTranslation.InvMass())->ShowClose(kFALSE);

  browser->StartEmbedding(TRootBrowser::kRight);
  fStats = new TCanvas();
  fStats->SetRightMargin(0.01);
  fStats->SetTopMargin(0.01);
  fStats->SetBottomMargin(0.14);
  fStats->Divide(1, 3, 0, 0);
  browser->StopEmbedding(fTranslation.Summary());
  browser->GetTabRight()->GetTabTab(fTranslation.Summary())->ShowClose(kFALSE);

  browser->StartEmbedding(TRootBrowser::kRight);
  fProp = new TCanvas(fTranslation.MassAWidth(), fTranslation.MassAWidth());
  fProp->SetRightMargin(0.01);
  fProp->SetTopMargin(0.01);
  fProp->SetBottomMargin(0.14);
  fProp->Divide(1, 2, 0, 0);
  browser->StopEmbedding(fTranslation.MassAWidth());
  browser->GetTabRight()->GetTabTab(fTranslation.MassAWidth())->ShowClose(kFALSE);

  browser->StartEmbedding(TRootBrowser::kBottom);
  fFitter = new Fitter(this);
  browser->StopEmbedding(fTranslation.Fitter());
  browser->SetTab(TRootBrowser::kRight, 0);

  browser->DontCallClose();
  browser->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");
}

void Exercise::HistSelect(TGListTreeItem* i)
{
  // Printf("Selected item %p", i);
  if (i->GetUserData() == nullptr) {
    fHist = nullptr;
    fItem = nullptr;
    fFitter->SetHistogram(fHist);
    return;
  }
  fItem = i;
  fHist = reinterpret_cast<TH1*>(i->GetUserData());
  fHist->SetDirectory(0);
  fFitter->SetHistogram(fHist, fAutoRange);
  // Printf("Selected the histogram: %p", fHist);
  if (fPlot != nullptr) {
    fPlot->cd();
    fPlot->SetLogy();
    gStyle->SetOptStat(0);
    fHist->DrawCopy();
    fPlot->Modified();
    fPlot->Update();
    fPlot->cd();
    fBrowser->SetTab(TRootBrowser::kRight, 1);
  } else {
    Warning("Select", "No canvas!");
  }
}

void Exercise::Fit()
{
  if (fHist == nullptr) {
    return;
  }
  if (fItem == nullptr) {
    return;
  }

  fTotal = 0;
  fTotalE = 0;
  fBack = 0;
  fBackE = 0;
  Float_t minBck = fFitter->fBackground->Min();
  Float_t maxBck = fFitter->fBackground->Max();
  Float_t minSig = fFitter->fSignal->Min();
  Float_t maxSig = fFitter->fSignal->Max();
  fFitter->fSignal->Set(minSig, maxSig);
  fFitter->fBackground->Set(minBck, maxBck);

  fHist->GetListOfFunctions()->Clear();
  fPlot->Modified();
  fPlot->Update();
  fPlot->cd();

  if (fFit == nullptr) {
    fFit = new TF1("fit", "gausn(0)+pol2(3)", 0, 2);
    fFit->SetParNames("Y", "#mu", "#sigma", "A", "B", "C");
    fBg = new TF1("bg", "pol2", 0, 2);
    fBg->SetParNames("A", "B", "C");
  }
  // Printf("Fitting in the range %f to %f", minBck, maxBck);
  fBg->SetRange(minBck, maxBck);
  fFit->SetRange(minBck, maxBck);
  fFit->SetParameters(80, (minSig + maxSig) / 2, (maxSig - minSig) / 4);
  fFit->SetParLimits(0, 0, 1e9);
  fFit->SetParLimits(1, minSig, maxSig);
  fFit->SetParLimits(2, 0, (maxSig - minSig) / 2);
  fFit->SetLineColor(kGreen + 1);

  TFitResultPtr r = fHist->Fit(fFit, "NQSR", "", minBck, maxBck);
  int status = r.operator int();
  if (status < 0) {
    return;
  }

  fBg->SetLineColor(kBlue + 1);
  fBg->SetParameters(fFit->GetParameter(3), fFit->GetParameter(4), fFit->GetParameter(5));
  fBg->SetParError(0, fFit->GetParError(3));
  fBg->SetParError(1, fFit->GetParError(4));
  fBg->SetParError(2, fFit->GetParError(5));

  auto* red = new TLatex(.98, .8, Form("#chi^{2}/#nu=%6.3f", r->Chi2() / r->Ndf()));
  red->SetTextAlign(32);
  red->SetTextFont(42);
  red->SetNDC();
  fHist->GetListOfFunctions()->Add(red);

  fPlot->cd();
  fFit->Draw("same");
  fPlot->Modified();
  fPlot->Update();
  fPlot->cd();

  const TMatrixDSym tCov = r->GetCovarianceMatrix();
  TMatrixDSym bCov(3);
  for (Int_t i = 0; i < 3; i++) {
    for (Int_t j = 0; j < 3; j++) {
      bCov(i, j) = tCov(3 + i, 3 + j);
    }
  }

  Double_t dx = fHist->GetXaxis()->GetBinWidth(1);
  fTotal = fFit->Integral(minSig, maxSig);
  fTotalE = fFit->IntegralError(minSig, maxSig, r->GetParams(), tCov.GetMatrixArray());
  fBack = fBg->Integral(minSig, maxSig);
  fBackE = fBg->IntegralError(minSig, maxSig, &(r->GetParams()[3]), bCov.GetMatrixArray());
  fTotal /= dx;
  fTotalE /= dx;
  fBack /= dx;
  fBackE /= dx;

  // Printf("Total  intergal %8.2f +/- %5.2f", fTotal, fTotalE);
  // Printf("Backg. intergal %8.2f +/- %5.2f", fBack,  fBackE);
  // Printf("Signal integral %8.2f +/- %5.2f", fTotal-fBack,
  //        TMath::Sqrt(fTotalE*fTotalE+fBackE*fBackE));

  fFitter->fAccept->SetEnabled(true);
}

void Exercise::Accept()
{
  if (fHist == nullptr) {
    // Printf("No histogram");
    return;
  }
  if (fItem == nullptr) {
    // Printf("No item");
    return;
  }

  fPlot->Clear();
  fHist->SetStats(false);
  if (fHist->GetListOfFunctions() != nullptr) {
    fHist->GetListOfFunctions()->Clear();
  }
  fItem->Toggle();
  fItem->GetParent()->UpdateState();
  if (fItem->GetParent()->GetParent() != nullptr) {
    fItem->GetParent()->GetParent()->UpdateState();
  }

  // Check if this is pp, and retrieve the centrality bin (stored in
  // under- and overflow bins)
  auto c1 = Int_t(fHist->GetBinContent(0));
  auto c2 = Int_t(fHist->GetBinContent(fHist->GetNbinsX() + 1));
  Int_t type = fHist->GetUniqueID();
  //Summarize(type, c1, c2, 0, 0, 0, 0, 0, 0);

  if (!fItem->IsChecked()) {
    fPlot->cd();
    fHist->Draw();
    fFitter->SetHistogram(fHist, fAutoRange);
    return;
  }

  // Printf("Now clone stuff: %p, %p", fHist, fHist->GetListOfFunctions());
  fHist->GetListOfFunctions()->Add(fFit->Clone("sig"));
  fHist->GetListOfFunctions()->Add(fBg->Clone("bg"));

  Double_t s = fTotal - fBack;
  Double_t es = TMath::Sqrt(fTotalE * fTotalE + fBackE * fBackE);
  Double_t m = fFit->GetParameter(1);
  Double_t em = fFit->GetParError(1);
  Double_t w = fFit->GetParameter(2);
  Double_t ew = fFit->GetParError(2);
  auto* info = new TPaveStats(.65, .65, .99, .99, "NDC");
  info->SetOptStat(11111110);
  info->SetBorderSize(0);
  info->SetFillStyle(0);
  info->SetTextFont(42);
  info->AddText(Form("%s=%d", fTranslation.Entries().Data(), int(fHist->GetEntries())));
  info->AddText(Form("%s=%6.0f #pm %3.0f", fTranslation.Total().Data(), fTotal, fTotalE));
  info->AddText(Form("%s=%6.0f #pm %3.0f", fTranslation.Background().Data(), fBack, fBackE));
  info->AddText(Form("%s=%6.0f #pm %3.0f", fTranslation.Signal().Data(), s, es));
  info->AddText(Form("%s=%6.4f #pm %6.4f", fTranslation.Mean().Data(), fFit->GetParameter(1),
                     fFit->GetParError(1)));
  info->AddText(Form("#sigma=%6.4f #pm %6.4f", fFit->GetParameter(2), fFit->GetParError(2)));
  fHist->GetListOfFunctions()->Add(info);

  fPlot->cd();
  fHist->Draw();
  fPlot->Modified();
  fPlot->Update();
  fPlot->cd();

  Summarize(type, c1, c2, s, es, m, em, w, ew);
  fFitter->SetHistogram(fHist, fAutoRange);
}

TGraphErrors* Exercise::Graph(TMultiGraph* m, Int_t type)
{
  TGraphErrors* g = (m->GetListOfGraphs() != nullptr
                       ? dynamic_cast<TGraphErrors*>(m->GetListOfGraphs()->At(type - 1))
                       : nullptr);
  if (g != nullptr) {
    return g;
  }

  g = new TGraphErrors;
  g->SetName(type == 1 ? "k0" : type == 2 ? "lambda" : type == 3 ? "antilambda" : "unkown");
  g->SetTitle(type == 1 ? "K_{0}" : type == 2 ? "#Lambda" : type == 3 ? "#bar{#Lambda}" : "Unkown");
  g->SetMarkerStyle(type == 1 ? 20 : type == 2 ? 21 : type == 3 ? 47 : 24);
  Color_t c = (type == 1 ? kRed + 2 : type == 2 ? kGreen + 2 : type == 3 ? kBlue + 2 : kGray + 2);
  g->SetLineColor(c);
  g->SetMarkerColor(c);
  g->SetMarkerSize(1.5);
  g->SetLineWidth(2);
  g->SetFillColor(0);
  g->SetFillStyle(0);

  if (m->GetListOfGraphs() == nullptr) {
    m->Add(g);
    m->RecursiveRemove(g);
  }
  m->GetListOfGraphs()->AddAt(g, type - 1);

  return g;
}

void Exercise::SetPoint(TMultiGraph* m, Int_t type, Int_t i, Double_t nPart, Double_t y, Double_t e)
{
  TGraphErrors* g = Graph(m, type);
  SetPoint(g, i, nPart, y, e);
}

void Exercise::SetPoint(TGraphErrors* g, Int_t i, Double_t nPart, Double_t y, Double_t e)
{
  if (g == nullptr) {
    return;
  }
  Int_t l = g->GetN() - 1;
  if (l < i) {
    for (Int_t j = l + 1; j < i; j++) {
      g->SetPoint(j, 0, 0);
    }
  }
  g->SetPoint(i, nPart, y);
  g->SetPointError(i, 0, e);
}

void Exercise::Summarize(Int_t type, Int_t c1, Int_t c2, Double_t s, Double_t es, Double_t m,
                         Double_t em, Double_t w, Double_t ew)
{
  if (type <= 0) {
    return;
  }

  Int_t bin = CentBin(c1, c2);
  Double_t nPart = NPart(bin);
  Int_t nEv = NEvents(bin);
  Double_t eff = Efficiency(bin, type);
  // Printf("Centrality %3d-%3d%% -> bin %2d, Npart %3.0f",c1,c2,bin,nPart);
  if (bin < 0) {
    return; // Ignore Pb-Pb MB
  }

  // Printf("Setting raw point %2d to %4f %f+/-%f", bin, nPart, s, es);
  SetPoint(fRaw, type, bin, nPart, s, es);
  DrawMG(fRaw, fStats, 1, fTranslation.Measurement());
  TLegend* l = fStats->GetPad(1)->BuildLegend(.13, .6, .4, .90);
  l->SetBorderSize(0);
  l->SetFillStyle(0);
  l->SetTextFont(42);

  SetPoint(fMass, type, bin, nPart, m, em);
  DrawMG(fMass, fProp, 1, "#LTm#GT");

  SetPoint(fWidth, type, bin, nPart, w, ew);
  DrawMG(fWidth, fProp, 2, "#sigma_{m}");

  Double_t yy = 0, ey = 0;
  if (eff > 0 && nEv > 0) {
    yy = s / (eff * nEv);
    ey = es / (eff * nEv);
    SetPoint(fYield, type, bin, nPart, yy, ey);
    // Printf("Setting yie point %2d to %4f %f+/-%f", bin, nPart, yy, ey);
    DrawMG(fYield, fStats, 2, fTranslation.KeyYield());
  }

  if (bin > 0) {
    Double_t epp, pp = PPYield(type, epp);
    Double_t yh = yy / nPart / (pp / 2);
    Double_t eh = TMath::Sqrt(ey * ey / yy / yy + epp * epp / pp / pp) * yh;
    SetPoint(fEnhancement, type, bin - 1, nPart, yh, eh);
    // Printf("Setting sup point %2d to %4f %f+/-%f", bin, nPart, yh, eh);
    DrawMG(fEnhancement, fStats, 3, fTranslation.Ratio2pp());
  }

  fProp->Modified();
  fProp->Update();
  fProp->cd();

  fStats->Modified();
  fStats->Update();
  fStats->cd();
}

void Exercise::DrawMG(TMultiGraph* mg, TVirtualPad* p, Int_t sub, const char* txt)
{
  if (mg->GetHistogram() != nullptr) {
    // Force re-build histogram
    auto* dummy = new TObject;
    mg->GetListOfGraphs()->Add(dummy);
    mg->RecursiveRemove(dummy);
  }

  TVirtualPad* pad = p->cd(sub);
  pad->Clear();
  pad->SetTicks();
  pad->SetRightMargin(0.01);
  mg->Draw("ap");
  mg->GetHistogram()->SetYTitle(txt);
  mg->GetHistogram()->SetXTitle("#it{N}_{part}");
  mg->GetHistogram()->GetXaxis()->Set(100, 0.5, 395);
  mg->GetHistogram()->Rebuild();
  Double_t tbase = 0.03 / pad->GetHNDC();
  mg->GetHistogram()->GetXaxis()->SetLabelSize(tbase);
  mg->GetHistogram()->GetXaxis()->SetTitleSize(tbase);
  mg->GetHistogram()->GetXaxis()->SetNdivisions(10);
  mg->GetHistogram()->GetYaxis()->SetLabelSize(tbase);
  mg->GetHistogram()->GetYaxis()->SetTitleSize(tbase);
  mg->GetHistogram()->GetYaxis()->SetNdivisions(210);
  mg->GetHistogram()->GetYaxis()->SetTitleOffset(.6);
  pad->Modified();
  pad->Update();
  pad->cd();
}

Int_t Exercise::CentBin(Int_t c1, Int_t c2) const
{
  if (c1 == 0 && c2 == 100) {
    return -2;
  }
  if (c1 == -1) {
    return 0;
  }
  return c1 / 10 + 1;
}

Double_t Exercise::NPart(Int_t bin) const
{
  if (bin < 0 || bin > 8) {
    return -1;
  }
  const Double_t nPart[] = { 2, 360, 260, 186, 129, 85, 52, 30, 16 };
  return nPart[bin];
}

Int_t Exercise::NEvents(Int_t bin) const
{
  if (bin < 0 || bin > 8) {
    return -1;
  }
  const Int_t nEv[] = { 5260, 213, 290, 302, 310, 302, 300, 315, 350 };
  return nEv[bin];
}

Double_t Exercise::Efficiency(Int_t bin, Int_t type) const
{
  switch (type) {
    case 1: // K0
      switch (bin) {
        case 0:
          return 0.26; // pp
        case 1:
        case 2:
          return 0.26;
        case 3:
        case 4:
        case 5:
        case 6:
          return 0.29;
        case 7:
          return 0.35;
        case 8:
          return 0.26;
      }
      break;
    case 2:
    case 3: // Lambda and anti-lambda
      switch (bin) {
        case 0:
          return (type == 2 ? 0.665 : 0.438); // pp
        case 1:
          return 0.2;
        case 2:
          return 0.21;
        case 3:
        case 4:
        case 5:
          return 0.22;
        case 6:
        case 7:
        case 8:
          return 0.2;
      }
      break;
  }
  return -1;
}

Double_t Exercise::PPYield(Int_t type, Double_t& e)
{
  TGraphErrors* y = Graph(fYield, type);
  Double_t r = ((y != nullptr) && y->GetN() > 0 ? y->GetY()[0] : 0);
  if ((y != nullptr) && r > 0) {
    e = y->GetEY()[0];
  } else {
    e = 0;
    r = -1;
    switch (type) {
      case 1:
        r = 0.25;
        e = 0.045 * r;
        break;
      case 2:
      case 3:
        r = 0.0615;
        e = 0.085 * r;
        break;
    }
  }
  // Printf("pp Yield for %d:  %f+/-%f", type, r, e);
  return r;
}

TNtuple* Exercise::NTuple()
{
  auto* tuple = new TNtuple("meas", fTranslation.Measurements(),
                            "cmin:cmax:nPart:nEv:"
                            "nKaon:eKaon:fKaon:"
                            "nLambda:eLambda:fLambda:"
                            "nAntiLambda:eAntiLambda:fAntiLambda");
  tuple->SetDirectory(nullptr);
  tuple->SetAlias("yKaon", "nKaon/(nEv*fKaon)");
  tuple->SetAlias("eyKaon", "eKaon/(nEv*fKaon)");
  tuple->SetAlias("yLambda", "nLambda/(nEv*fLambda)");
  tuple->SetAlias("eyLambda", "eLambda/(nEv*fLambda)");
  tuple->SetAlias("yAntiLambda", "nAntiLambda/(nEv*fAntiLambda)");
  tuple->SetAlias("eyAntiLambda", "eAntiLambda/(nEv*fAntiLambda)");

  TGraphErrors* k = Graph(fRaw, 1);
  TGraphErrors* la = Graph(fRaw, 2);
  TGraphErrors* al = Graph(fRaw, 3);

  Int_t n = TMath::Max(TMath::Max(k->GetN(), la->GetN()), al->GetN());
  for (Int_t i = 0; i < n; i++) {
    Float_t c1 = (i == 0 ? -1 : (i - 1) * 10);
    Float_t c2 = (i == 0 ? -1 : (i + 0) * 10);
    Float_t np = NPart(i);
    Float_t nE = NEvents(i);
    Float_t nk = (i >= k->GetN() ? 0 : k->GetY()[i]);
    Float_t ek = (i >= k->GetN() ? 0 : k->GetEY()[i]);
    Float_t fk = Efficiency(i, 1);
    Float_t nl = (i >= la->GetN() ? 0 : la->GetY()[i]);
    Float_t el = (i >= la->GetN() ? 0 : la->GetEY()[i]);
    Float_t fl = Efficiency(i, 2);
    Float_t na = (i >= al->GetN() ? 0 : al->GetY()[i]);
    Float_t ea = (i >= al->GetN() ? 0 : al->GetEY()[i]);
    Float_t fa = Efficiency(i, 3);
    Printf(
      "%3.0f-%3.0f: np=%3.0f ne=%4.0f "
      "nk=%7.2f+/-%7.3f nl=%7.2f/%7.3f na=%7.2f+/-%7.3f",
      c1, c2, np, nE, nk, ek, nl, el, na, ea);
    tuple->Fill(c1, c2, np, nE, nk, ek, fk, nl, el, fl, na, ea, fa);
  }
  return tuple;
}

void Exercise::Export(const TString& fn)
{
  // FIXME I think tuple is just dead here?
  TNtuple* tuple = NTuple();
  TFile* out = TFile::Open(fn, "RECREATE");
  fRaw->Write();
  fYield->Write();
  fEnhancement->Write();
  fMass->Write();
  fWidth->Write();
  fStats->Write();
  fProp->Write();
  tuple->Write();
  out->Write();
  out->Close();
  delete tuple;
}

void Exercise::Auto()
{
  TGListTreeItem* top = fPicker->fTop;
  TGListTreeItem* typ = top->GetFirstChild();
  while (typ != nullptr) {
    Printf("Processing %s", typ->GetText());
    TGListTreeItem* his = typ->GetFirstChild();
    while (his != nullptr) {
      Printf(" Processing %s", his->GetText());
      HistSelect(his);
      if (fHist->GetBinContent(0) != 80) {
        Fit();
        Accept();
      }
      his = his->GetNextSibling();
    }
    typ = typ->GetNextSibling();
  }
  Export("strangeness.root");
  PrintPdf("strangeness.pdf");
}
} // namespace Strangeness
