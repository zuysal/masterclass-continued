#include "Calculator.h"

#include <TEveTrack.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <TGDimension.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TString.h>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part1/Histograms.h"
#include "Utility/Instructions.h"
#include "Utility/LanguageProvider.h"

class TGWindow;

namespace Strangeness
{
namespace Tools
{
Double_t E(Double_t m, Double_t px, Double_t py, Double_t pz)
{
  return TMath::Sqrt(m * m + px * px + py * py + pz * pz);
}
Double_t E(Double_t m, const TEveVector& p) { return E(m, p.fX, p.fY, p.fZ); }

Double_t M(Int_t pdg)
{
  if (pdg == 0) {
    return 0;
  }
  return TDatabasePDG::Instance()->GetParticle(pdg)->Mass();
}

Double_t InvMass(Double_t m1, Double_t px1, Double_t py1, Double_t pz1, Double_t m2, Double_t px2,
                 Double_t py2, Double_t pz2, Double_t m3, Double_t px3, Double_t py3, Double_t pz3)
{
  Double_t se = E(m1, px1, py1, pz1) + E(m2, px2, py2, pz2) + E(m3, px3, py3, pz3);
  Double_t sx = px1 + px2 + px3;
  Double_t sy = py1 + py2 + py3;
  Double_t sz = pz1 + pz2 + pz3;
  return TMath::Sqrt(se * se - sx * sx - sy * sy - sz * sz);
}
Double_t InvMass(Double_t m1, const TEveVector& p1, Double_t m2, const TEveVector& p2, Double_t m3,
                 const TEveVector& p3)
{
  return InvMass(m1, p1.fX, p1.fY, p1.fZ, m2, p2.fX, p2.fY, p2.fZ, m3, p3.fX, p3.fY, p3.fZ);
}
Double_t InvMass(Int_t i1, const TEveVector& p1, Int_t i2, const TEveVector& p2, Int_t i3,
                 const TEveVector& p3)
{
  return InvMass(M(i1), p1, M(i2), p2, M(i3), p3);
}
} // namespace Tools

Particle::Particle(TGCompositeFrame* p, const char* type, Pixel_t color)
  : TGHorizontalFrame(p, 100, 30)
{
  auto* lh = new TGLayoutHints(kLHintsExpandX);
  auto* label = new TGLabel(this, type);
  AddFrame(label, lh);
  AddFrame(fPx = new TGNumberEntryField(this), lh);
  AddFrame(fPy = new TGNumberEntryField(this), lh);
  AddFrame(fPz = new TGNumberEntryField(this), lh);
  AddFrame(fM = new TGNumberEntryField(this), lh);
  fPx->SetEnabled(false);
  fPy->SetEnabled(false);
  fPz->SetEnabled(false);
  fM->SetEnabled(false);
  fPx->SetFormat(TGNumberFormat::kNESRealFour);
  fPy->SetFormat(TGNumberFormat::kNESRealFour);
  fPz->SetFormat(TGNumberFormat::kNESRealFour);
  fM->SetFormat(TGNumberFormat::kNESRealFour);

  label->SetTextColor(color);
  //fPx->SetTextColor(color);
  //fPy->SetTextColor(color);
  //fPz->SetTextColor(color);
  //fM->SetTextColor(color);

  p->AddFrame(this, lh);
}
void Particle::Reset()
{
  fPx->SetNumber(0);
  fPy->SetNumber(0);
  fPz->SetNumber(0);
  fM->SetNumber(0);
}

void Particle::Set(Double_t px, Double_t py, Double_t pz, Double_t m)
{
  fPx->SetNumber(px);
  fPy->SetNumber(py);
  fPz->SetNumber(pz);
  fM->SetNumber(m);
}

Calculator::Calculator(Histograms* hists, const TGWindow* p, UInt_t w, UInt_t h)
  : TGMainFrame(p, w, h, kFixedHeight | kHorizontalFrame)
  , fH(hists)
  , fTranslation(Utility::TranslationFromEnv<ClassLanguage>())
{
  DontCallClose();

  auto* lh = new TGLayoutHints(kLHintsExpandX);
  // Make two columns
  TGHorizontalFrame* hf = nullptr;
  TGVerticalFrame* vf = nullptr;
  TGGroupFrame* gf = nullptr;

  // Make Left hand side for particles
  gf = new TGGroupFrame(this, fTranslation.Particles(), kFixedWidth | kVerticalFrame);
  vf = new TGVerticalFrame(gf);

  // Make header frames
  hf = new TGHorizontalFrame(vf);
  hf->AddFrame(new TGLabel(hf, "    "), lh);
  hf->AddFrame(new TGLabel(hf, "px"), lh);
  hf->AddFrame(new TGLabel(hf, "py"), lh);
  hf->AddFrame(new TGLabel(hf, "pz"), lh);
  hf->AddFrame(new TGLabel(hf, "m"), lh);
  vf->AddFrame(hf, lh);

  fN = new Particle(vf, "(-)", 0x00FF00);
  fP = new Particle(vf, "(+)", 0xFF0000);
  fB = new Particle(vf, "(b)", 0x0000FF);

  gf->AddFrame(vf, lh);
  gf->Resize(300, 150);
  AddFrame(gf, new TGLayoutHints(kLHintsLeft | kLHintsExpandY, 4, 2, 2, 4));

  lh = new TGLayoutHints(kLHintsExpandX, 2, 2, 3, 3);
  TDatabasePDG* pdgDb = TDatabasePDG::Instance();

  // Make operations
  gf = new TGGroupFrame(this, fTranslation.Operations(), kVerticalFrame);

  // --- Instructions button ---------------------------------------
  auto* InstButton = Utility::MakeInstructionsButton(gf);
  InstButton->Connect("Clicked()", "Strangeness::Calculator", this, "Instructions()");

  hf = new TGHorizontalFrame(gf);
  auto *label = new TGLabel(hf, fTranslation.InvMass(), TGLabel::GetDefaultGC()(), TGLabel::GetDefaultFontStruct(), kFixedWidth);
  label->SetWidth(100);
  hf->AddFrame(label);
  fI = new TGNumberEntryField(hf);
  fI->SetEnabled(false);
  hf->AddFrame(fI, new TGLayoutHints(kLHintsExpandX, 5));
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 0));

  hf = new TGHorizontalFrame(gf);
  label = new TGLabel(hf, fTranslation.Choice(), TGLabel::GetDefaultGC()(), TGLabel::GetDefaultFontStruct(), kFixedWidth);
  label->SetWidth(100);
  hf->AddFrame(label);
  fT = new TGComboBox(hf);
  fT->AddEntry(fTranslation.SelectParticleType(), 0);
  fT->AddEntry(fTranslation.Background(), 0);
  fT->AddEntry(Form("Kaon (m=%5.3f)", pdgDb->GetParticle(310)->Mass()), 310);
  fT->AddEntry(Form("Lambda (m=%5.3f)", pdgDb->GetParticle(3122)->Mass()), 3122);
  fT->AddEntry(Form("Anti-Lambda (m=%5.3f)", pdgDb->GetParticle(-3122)->Mass()), -3122);
  fT->AddEntry(Form("Xi (m=%5.3f)", pdgDb->GetParticle(3312)->Mass()), 3312);
  fT->Resize(300, 20);
  fT->Select(0, false);
  hf->AddFrame(fT, new TGLayoutHints(kLHintsExpandX, 5));
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX, 0, 0, 2, 0));

  hf = new TGHorizontalFrame(gf);
  auto* ca = new TGTextButton(hf, fTranslation.Clear());
  ca->Connect("Clicked()", "Strangeness::Calculator", this, "Reset()");
  hf->AddFrame(ca, lh);
  fSubmit = new TGTextButton(hf, fTranslation.Submit());
  fSubmit->Connect("Clicked()", "Strangeness::Calculator", this, "Take()");
  hf->AddFrame(fSubmit, lh);
  fSubmit->SetEnabled(false);
  fUndo = new TGTextButton(hf, fTranslation.Undo());
  fUndo->Connect("Clicked()", "Strangeness::Calculator", this, "Undo()");
  hf->AddFrame(fUndo, lh);
  fUndo->SetEnabled(kFALSE);
  auto *clearhist = new TGTextButton(hf, fTranslation.ClearHisto());
  clearhist->Connect("Clicked()", "Strangeness::Calculator", this, "ClearHisto()");
  hf->AddFrame(clearhist, lh);
  gf->AddFrame(hf, new TGLayoutHints(kLHintsExpandX | kLHintsBottom, 0, 0, 2, 0));
  AddFrame(gf, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 4, 2, 4));

  MapSubwindows();
  TGDimension d = GetDefaultSize();
  Resize(d.fWidth, 400);
  MapWindow();
}

void Calculator::Set(Particle* p, TEveTrack* t)
{
  Double_t m = Tools::M(t->GetPdg());
  p->Set(t->GetMomentum().fX, t->GetMomentum().fY, t->GetMomentum().fZ, m);
}

void Calculator::SetN(TEveTrack* t)
{
  Set(fN, t);
  Calc();
}

void Calculator::SetP(TEveTrack* t)
{
  Set(fP, t);
  Calc();
}

void Calculator::SetB(TEveTrack* t)
{
  Set(fB, t);
  Calc();
}

void Calculator::Calc()
{
  CheckSubmit();
  Double_t m = Tools::InvMass(fN->M(), fN->Px(), fN->Py(), fN->Pz(), fP->M(), fP->Px(), fP->Py(),
                              fP->Pz(), fB->M(), fB->Px(), fB->Py(), fB->Pz());
  fI->SetNumber(m);
}

void Calculator::Auto()
{
  if (fN->M() < .01 || fP->M() < .01) {
    return;
  }
  Double_t invMass = fI->GetNumber() * 1000;
  Int_t pdg = 0;
  if (invMass >= 484 && invMass <= 510) {
    pdg = 310; // K0s
  } else if (invMass >= 1110 && invMass <= 1120) {
    if (fN->M() < .8) {
      pdg = 3122; // lambda
    } else {
      pdg = -3122; // anti-lambda
    }
  } else if (fB->M() > .01 && invMass >= 1311 && invMass <= 1331) {
    pdg = 3312; // Xi-
  }
  if (pdg != 0) {
    fT->Select(pdg);
  } else {
    Warning("Auto", "Couldn't get particle w/inv. mass=%f", invMass);
  }
}

void Calculator::Take()
{
  Int_t pdg = fT->GetSelected();
  Double_t m = fI->GetNumber();
  fH->Fill(pdg, m);
  fUndo->SetEnabled(kTRUE);
  Reset();
}

void Calculator::Reset()
{
  fN->Reset();
  fP->Reset();
  fB->Reset();
  fI->SetNumber(0);
  fT->Select(0, false);
  CheckSubmit();
}

void Calculator::Instructions()
{
  const TString& FName = fTranslation.FileCalculator();
  new Utility::Instructions(gClient->GetRoot(), 700, 400, FName);
}

void Calculator::Undo()
{
  fH->Undo();
  fH->Update();
  fUndo->SetEnabled(kFALSE);
}

void Calculator::ClearHisto()
{
  fH->Clear();
  fUndo->SetEnabled(kFALSE);
}

void Calculator::CheckSubmit()
{
    fSubmit->SetEnabled(!(fN->IsEmpty() || fP->IsEmpty()));
}
} // namespace Strangeness
