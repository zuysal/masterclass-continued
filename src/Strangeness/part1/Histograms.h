/**
 * @file   StrangeHistograms.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 19:50:14 2017
 *
 * @brief  Container of histograms
 *
 * @ingroup alice_masterclass_str_part1
 */
#ifndef STRANGEHISTOGRAMS_C
#define STRANGEHISTOGRAMS_C

#include <RtypesCore.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1.h>

#include "Strangeness/GUITranslation.h"

class TH1;

namespace Strangeness
{
struct TGUIEnglish;

/**
 * Container of histograms
 *
 *
 * @ingroup alice_masterclass_str_part1
 */
TH1* MakeInvMass(const char* name, const char* title, Color_t color, Double_t min, Double_t max);

struct Histograms {
  const TGUIEnglish& fTranslation;
  TH1* fInvMassK;
  TH1* fInvMassXi;
  TH1* fInvMassLambda;
  TH1* fInvMassAntiLambda;
  TH1* fInvMass;
  TH1* fPreviousHisto;
  Int_t fPreviousParticle;

  TCanvas* fCanvas{};

  Histograms();

  void Setup();
  void Fill(Int_t pdg, Double_t m);
  void Clear();
  void Update();
  void Undo();
  void Export(const char* name);
  void PrintPdf(const char* name) { fCanvas->SaveAs(name); }
};
} // namespace Strangeness

#endif
