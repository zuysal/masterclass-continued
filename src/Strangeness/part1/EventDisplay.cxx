#include "EventDisplay.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TEveTrack.h>
#include <TEveVector.h>
#include <TGClient.h>
#include <TGDimension.h>
#include <TGFrame.h>
#include <TGTab.h>
#include <TH1.h>
#include <TRootBrowser.h>
#include <TString.h>
#include <cassert>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part1/Calculator.h"
#include "Strangeness/part1/Histograms.h"
#include "StrangenessVSDReader.h"
#include "Utility/ERenderElements.h"
#include "Utility/EventDisplay.h"
#include "Utility/LanguageProvider.h"

class TGWindow;

namespace Strangeness
{
EventDisplay::EventDisplay(Bool_t isDemo)
  : fInvMassK(MakeInvMass("p0321", "Kaons", kRed + 2, 0.4, 0.6))
  , fInvMassXi(MakeInvMass("p3312", "Xis", kGreen + 2, 1.2, 1.4))
  , fInvMassLambda(MakeInvMass("p3122", "Lambas", kBlue + 2, 1.0, 1.2))
  , fInvMassAntiLambda(MakeInvMass("a3122", "Anti-Lambdas", kMagenta + 2, 1.0, 1.2))
  , fIsDemo(isDemo)
  , fAutoGuess(false)
  , fTranslation(Utility::TranslationFromEnv<ClassLanguage>())
{
}

void EventDisplay::Setup(TRootBrowser* browser, Bool_t cheat)
{
  // Set-up the counter interface
  fAutoGuess = cheat;
  TGTab* bot = browser->GetTabBottom();
  auto* up = reinterpret_cast<TGFrame*>(const_cast<TGWindow*>(bot->GetParent()));
  TGDimension d = up->GetDefaultSize();
  up->Resize(d.fWidth, 170);
  // bot->SetHeight(300);
  browser->StartEmbedding(TRootBrowser::kBottom);
  fCalculator = new Calculator(&fHistograms, gClient->GetRoot(), 0, 200);
  browser->StopEmbedding(fTranslation.Calculator());
  browser->GetTabBottom()->SetMinHeight(300);
  browser->GetTabBottom()->SetHeight(300);

  // Create histogram tab
  browser->StartEmbedding(TRootBrowser::kRight);
  fHistograms.Setup();
  browser->StopEmbedding(fTranslation.EventCharacteristics());
  browser->GetTabRight()->GetTabTab(fTranslation.EventCharacteristics())->ShowClose(kFALSE);
}

TString EventDisplay::NewEvent(Int_t evNo)
{
  if (!fIsDemo) {
    return "pp @ sqrt{s}=2.76TeV";
  }
  switch (evNo) {
    case 0:
      return "K0S -> pi- + pi+";
    case 1:
      return "Lambda -> proton + pi-";
    case 2:
      return "anti-Lambda -> anti-proton + pi+";
    case 3:
      return "Xi- -> pi- + Lambda (proton + pi-)";
  }
  return "pp @ sqrt{s}=2.76TeV";
}

void EventDisplay::ClearV0s()
{
  fInvMassK->Reset();
  fInvMassLambda->Reset();
  fInvMassAntiLambda->Reset();
}

void EventDisplay::V0NegativeLoaded(TEveTrack* Track)
{
  Expects(Track != nullptr);
  fV0Negative = Track;
}

/// FIXME Make them gsl::not_null
void EventDisplay::V0PositiveLoaded(TEveTrack* Track)
{
  Expects(Track != nullptr);
  fV0Positive = Track;
}

void EventDisplay::V0Loaded()
{
  Expects(fV0Negative != nullptr);
  Expects(fV0Positive != nullptr);

  Int_t in = fV0Negative->GetPdg();
  Int_t ip = fV0Positive->GetPdg();
  TH1* h = nullptr;
  if (in == -211 && ip == +211) {
    h = fInvMassK;
  } else if (in == -211 && ip == +2212) {
    h = fInvMassLambda;
  } else if (in == -2212 && ip == +211) {
    h = fInvMassAntiLambda;
  } else {
    // FIXME Is this a valid thing? Otherwise throw an exception here
    return;
  }
  TEveVector pb(0, 0, 0);
  Double_t m =
    Tools::InvMass(in, fV0Negative->GetMomentum(), ip, fV0Positive->GetMomentum(), 0, pb);
  h->Fill(m);

  // Clear the cache to symbol that these tracks have been handled.
  fV0Negative = nullptr;
  fV0Positive = nullptr;
}

void EventDisplay::FinishedV0s() { Expects(true && "Placeholder"); }

void EventDisplay::CascadeNegativeLoaded(TEveTrack* Track)
{
  Expects(Track != nullptr);
  fCascadeNegative = Track;
}
void EventDisplay::CascadePositiveLoaded(TEveTrack* Track)
{
  Expects(Track != nullptr);
  fCascadePositive = Track;
}
void EventDisplay::CascadeBachelorLoaded(TEveTrack* Track)
{
  Expects(Track != nullptr);
  fCascadeBachelor = Track;
}

/// FIXME Make args gsl::not_null
void EventDisplay::CascadeLoaded()
{
  Expects(fCascadeNegative != nullptr);
  Expects(fCascadePositive != nullptr);
  Expects(fCascadeBachelor != nullptr);

  Int_t in = fCascadeNegative->GetPdg();
  Int_t ip = fCascadePositive->GetPdg();
  Int_t ib = fCascadeBachelor->GetPdg();
  TH1* h = nullptr;
  if (in == -211 && ip == +2212 && ib == -211) {
    h = fInvMassXi;
  } else if (in == -2212 && ip == +211 && ib == +211) {
    h = fInvMassXi;
  }
  // Printf("Cascasde neg: %d  pos: %d  bachelor: %d hist: %p",
  //        in, ip, ib, h);
  if (h == nullptr) {
    return;
  }
  Double_t m = Tools::InvMass(in, fCascadeNegative->GetMomentum(), ip,
                              fCascadePositive->GetMomentum(), ib, fCascadeBachelor->GetMomentum());
  h->Fill(m);

  // Clear the cache variables to ensure these are handled only once.
  fCascadeNegative = nullptr;
  fCascadePositive = nullptr;
  fCascadeBachelor = nullptr;
}

void EventDisplay::FinishedCascades() { Expects(true && "Placeholder"); }

void EventDisplay::TrackSelected(TEveTrack* Track)
{
  using Utility::ERenderElements;
  using Utility::StringToRenderElement;
  Expects(
    StringToRenderElement(Track->GetElementName()) == ERenderElements::kV0TrackNegative ||
    StringToRenderElement(Track->GetElementName()) == ERenderElements::kV0TrackPositive ||
    StringToRenderElement(Track->GetElementName()) == ERenderElements::kV0PointingLine ||
    StringToRenderElement(Track->GetElementName()) == ERenderElements::kCascadeLine1 ||
    StringToRenderElement(Track->GetElementName()) == ERenderElements::kCascadeLine2 ||
    StringToRenderElement(Track->GetElementName()) == ERenderElements::kCascadeTrackNegative ||
    StringToRenderElement(Track->GetElementName()) == ERenderElements::kCascadeTrackPositive ||
    StringToRenderElement(Track->GetElementName()) == ERenderElements::kCascadeTrackBachelor);

  TString nme(Track->GetElementName());
  Printf("Got the track %s", nme.Data());

  /// TODO Easily dispatchable via the signal slot mechanism, the knowledge
  /// exists already.
  if (nme.Contains("Neg")) {
    fCalculator->SetN(Track);
  } else if (nme.Contains("Pos")) {
    fCalculator->SetP(Track);
  } else if (nme.Contains("Bachelor")) {
    fCalculator->SetB(Track);
  }
  if (fAutoGuess) {
    fCalculator->Auto();
  }
}

void EventDisplay::AutoEvent()
{
  fHistograms.fInvMassK->Add(fInvMassK);
  fHistograms.fInvMassXi->Add(fInvMassXi);
  fHistograms.fInvMassLambda->Add(fInvMassLambda);
  fHistograms.fInvMassAntiLambda->Add(fInvMassAntiLambda);
  fHistograms.Update();
}

std::unique_ptr<Utility::VSDReader> EventDisplay::CreateReader()
{
    return std_fix::make_unique<StrangenessVSDReader>();
}
} // namespace Strangeness
