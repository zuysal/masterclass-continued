#include "Combine.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TCanvasImp.h>
#include <TChain.h>
#include <TClass.h>
#include <TCollection.h>
#include <TDirectory.h>
#include <TError.h>
#include <TFile.h>
#include <TGClient.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TGraphErrors.h>
#include <TH1.h>
#include <THStack.h>
#include <TList.h>
#include <TMultiGraph.h>
#include <TNtuple.h>
#include <TObject.h>
#include <TProfile.h>
#include <TRootCanvas.h>
#include <TString.h>
#include <TGTab.h>

#include "Strangeness/GUITranslation.h"
#include "Utility/LanguageProvider.h"

class TGWindow;

namespace Strangeness
{

namespace
{
void SetupProfile(TProfile* P, Color_t c, Style_t s)
{
  P->SetMarkerColor(c);
  P->SetMarkerStyle(s);
  P->SetMarkerSize(2);
  P->SetLineColor(c);
  P->SetFillStyle(0);
  P->SetDirectory(nullptr);
}
} // namespace

Obs::Obs(const char* name, const char* title, Color_t c, Style_t s)
{
  fHI = new TProfile(Form("hi_%s", name), title, 8, 0, 80, "S");
  SetupProfile(fHI, c, s);

  fHF = new TProfile(Form("hf_%s", name), title, 8, 0, 80, "S");
  SetupProfile(fHF, c, s);

  fPP = new TProfile(Form("pp_%s", name), title, 1, -1.5, -.5, "S");
  SetupProfile(fPP, c, s);
}

void Obs::Fill(Float_t c1, Float_t c2, Float_t y, Float_t e, Float_t f)
{
  if (y < 1e-6) {
    return;
  }
  // if (e < 1e-9) return; // Ignore empty values
  TProfile* h = (c1 < 0 ? fPP : fHI);
  Printf("%-15s filling at %3.0f-%3.0f: %7.2f +/- %7.4f", h->GetName(), c1, c2, y, e);
  e = 1;
  h->Fill((c1 + c2) / 2, y, e);
  if (c1 >= 0) {
    fHF->Fill((c1 + c2) / 2, f, 1);
  }
}

TGraphErrors* Obs::Graph(UShort_t mode, TProfile* cvsnp, TProfile* cvsne, Double_t pp)
{
  auto* g = new TGraphErrors;
  g->SetName(fHI->GetName());
  g->SetTitle(fHI->GetTitle());
  g->SetMarkerColor(fHI->GetMarkerColor());
  g->SetMarkerStyle(fHI->GetMarkerStyle());
  g->SetMarkerSize(fHI->GetMarkerSize());
  g->SetLineStyle(fHI->GetLineStyle());
  g->SetFillStyle(0);

  Int_t nx = fHI->GetNbinsX();
  for (Int_t i = 1; i <= nx; i++) {
    Double_t n = fHI->GetBinContent(i);
    Double_t e = fHI->GetBinError(i);
    Double_t c = fHI->GetXaxis()->GetBinCenter(i);
    Double_t f = fHF->GetBinContent(i);
    Double_t np = cvsnp->GetBinContent(cvsnp->GetXaxis()->FindBin(c));
    Double_t ne = cvsne->GetBinContent(cvsne->GetXaxis()->FindBin(c));

    if (mode == 1) {
      g->SetPoint(i - 1, np, n);
      g->SetPointError(i - 1, 0, e);
      continue;
    }

    Double_t yi = n / (f * ne);
    Double_t ey = e / (f * ne);

    if (mode == 2) {
      Printf("%-15s: c=%2.0f (%3.0f) yield=%7.2f/(%4.3f*%4.0f)=%5.2f", fHI->GetName(), c, np, n, f,
             ne, yi);
      g->SetPoint(i - 1, np, yi);
      g->SetPointError(i - 1, 0, ey);
      continue;
    }

    Double_t su = yi / np / (pp / 2);
    Double_t es = ey / np / (pp / 2);
    g->SetPoint(i - 1, np, su);
    g->SetPointError(i - 1, 0, es);
  }

  if (mode == 1) {
    g->SetPoint(nx, 2, fPP->GetBinContent(1));
    g->SetPointError(nx, 0, fPP->GetBinError(1));
  }
  return g;
}

TNtuple* TCombine::CombineGetN(TDirectory* d, const char* name)
{
  if (d == nullptr) {
    return nullptr;
  }
  TObject* o = d->Get(name);
  if (o == nullptr) {
    Warning("GetH1", "Failed to get object %s from %s", name, d->GetName());
    return nullptr;
  }
  if (!o->IsA()->InheritsFrom(TNtuple::Class())) {
    Warning("GetH1", "Object %s from %s is not a TH1, but a %s", o->GetName(), d->GetName(),
            o->ClassName());
    return nullptr;
  }
  return dynamic_cast<TNtuple*>(o);
}

Bool_t TCombine::CombineTest(const char* filename, TCanvas* c)
{
  if ((filename == nullptr) || filename[0] == '\0') {
    return false;
  }
  TFile* file = TFile::Open(filename, "READ");
  TGUIEnglish& Translation = Utility::TranslationFromEnv<ClassLanguage>();
  if (file == nullptr) {
    new TGMsgBox(gClient->GetRoot(), fBrowser, Translation.DiagFailedOpenFileTitle(),
                 Form("%s \"%s\"", Translation.DiagFailedOpenFileText().Data(), filename),
                 kMBIconExclamation);
    return false;
  }
  TNtuple* nt = CombineGetN(file);
  if (nt == nullptr) {
    new TGMsgBox(gClient->GetRoot(), fBrowser, Translation.DiagIncompleteInputTitle(),
                 Form("%s %s", Translation.DiagIncompleteInputText().Data(), filename),
                 kMBIconExclamation);
    file->Close();
    return false;
  }
  file->Close();
  return true;
}

void TCombine::CombineOpen(TCanvas* c, TChain* chain)
{
  const char* types[] = { "ROOT File", "*.root", nullptr, nullptr };
  TGFileInfo info;
  info.fFileTypes = types;
  info.fMultipleSelection = true;
  new TGFileDialog(gClient->GetRoot(), fBrowser, kFDOpen, &info);

  TIter next(info.fFileNamesList);
  TObject* ofn = nullptr;
  while ((ofn = next()) != nullptr) {
    if (CombineTest(ofn->GetName(), c)) {
      chain->AddFile(ofn->GetName());
      Printf("Got %s", ofn->GetName());
    }
  }
}

Bool_t TCombine::CombineMore(TCanvas* c)
{
  Printf("Check if we need more input");
  Int_t ret = 0;
  TGUIEnglish& Translation = Utility::TranslationFromEnv<ClassLanguage>();
  new TGMsgBox(gClient->GetRoot(), fBrowser, Translation.DiagMoreDataTitle(),
               Translation.DiagMoreDataText(), kMBIconQuestion, kMBYes | kMBNo, &ret);
  return ret == kMBYes;
}

void TCombine::RunExercise(Bool_t /*cheat*/)
{
  UShort_t mode = 3;
  const TGUIEnglish& Translation = Utility::TranslationFromEnv<ClassLanguage>();

  fBrowser = new TRootBrowser(nullptr, "ALICE", 900, 700, "", false);
  fBrowser->DontCallClose();
  fBrowser->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");

  fBrowser->StartEmbedding(TRootBrowser::kRight);
  auto* c = new TCanvas(Translation.Collected(), Translation.StrangenessSuppression(), 800, 800);
  c->SetTopMargin(0.01);
  c->SetRightMargin(0.01);
  c->SetTicks();
  fBrowser->StopEmbedding(Translation.Summary());
  fBrowser->GetTabRight()->GetTabTab(Translation.Summary())->ShowClose(kFALSE);

  //Hide left and bottom tabs of the browser
  auto *fV1 = (TGCompositeFrame*)(fBrowser->GetTabLeft()->GetParent());
  auto *fHf = (TGCompositeFrame*)(fV1->GetParent());
  auto *fH2 = (TGCompositeFrame*)(fBrowser->GetTabBottom()->GetParent());
  auto *fV2 = (TGCompositeFrame*)(fH2->GetParent());
  fHf->HideFrame(fV1);
  fV2->HideFrame(fH2);

  fBrowser->MapWindow();

  auto* chain = new TChain("meas", "");
  do {
    CombineOpen(c, chain);
  } while (CombineMore(c));

  if(!chain->GetEntries())
      return;

  Float_t cmin, cmax, nPart, nEv;
  Float_t nKaon, eKaon, fKaon;
  Float_t nLambda, eLambda, fLambda;
  Float_t nAntiLambda, eAntiLambda, fAntiLambda;

  chain->SetBranchAddress("cmin", &cmin);
  chain->SetBranchAddress("cmax", &cmax);
  chain->SetBranchAddress("nPart", &nPart);
  chain->SetBranchAddress("nEv", &nEv);

  chain->SetBranchAddress("nKaon", &nKaon);
  chain->SetBranchAddress("eKaon", &eKaon);
  chain->SetBranchAddress("fKaon", &fKaon);

  chain->SetBranchAddress("nLambda", &nLambda);
  chain->SetBranchAddress("eLambda", &eLambda);
  chain->SetBranchAddress("fLambda", &fLambda);

  chain->SetBranchAddress("nAntiLambda", &nAntiLambda);
  chain->SetBranchAddress("eAntiLambda", &eAntiLambda);
  chain->SetBranchAddress("fAntiLambda", &fAntiLambda);

  Obs k("kaon", "K^{0}_{S}", kRed + 2, 20);
  Obs l("lambda", "#Lambda", kGreen + 2, 21);
  Obs a("alambda", "#bar{#Lambda}", kBlue + 2, 22);

  auto* cvsnp = new TProfile("cvsnp", "", 8, 0, 80);
  auto* cvsne = new TProfile("cvsne", "", 8, 0, 80);

  for (Int_t i = 0; i < chain->GetEntries(); i++) {
    chain->GetEntry(i);
    k.Fill(cmin, cmax, nKaon, eKaon, fKaon);
    l.Fill(cmin, cmax, nLambda, eLambda, fLambda);
    a.Fill(cmin, cmax, nAntiLambda, eAntiLambda, fAntiLambda);
    if (cmin < 0) {
      continue;
    }
    cvsnp->Fill((cmax + cmin) / 2, nPart);
    cvsne->Fill((cmax + cmin) / 2, nEv);
  }

  // To draw measurements versus centrality
  if (mode == 0) {
    auto* stackMeas = new THStack;
    stackMeas->Add(k.fHI);
    stackMeas->Add(k.fPP);
    stackMeas->Add(l.fHI);
    stackMeas->Add(l.fPP);
    stackMeas->Add(a.fHI);
    stackMeas->Add(a.fPP);
    stackMeas->Draw("nostack");
    return;
  }

  // To draw measurements versus Npart
  auto* mg = new TMultiGraph;
  mg->Add(k.Graph(mode, cvsnp, cvsne, 0.25));
  mg->Add(l.Graph(mode, cvsnp, cvsne, 0.0615));
  mg->Add(a.Graph(mode, cvsnp, cvsne, 0.0615));
  c->cd();
  mg->Draw("ap");

  TH1* hg = mg->GetHistogram();
  hg->SetXTitle("#it{N}_{part}");
  // TODO Simplify
  hg->SetYTitle(mode == 1 ? Translation.Count()
                          : mode == 2 ? Translation.KeyYield() : Translation.StrangenessEnhancement());
  hg->GetXaxis()->SetNdivisions(210);
  hg->GetYaxis()->SetNdivisions(210);
  // cvsnp->Draw();

  c->BuildLegend(c->GetLeftMargin() + .02, 1 - c->GetTopMargin() - .4, c->GetLeftMargin() + .4,
                 1 - c->GetTopMargin() - .02);
  c->SetFillStyle(0);
  c->SetBorderMode(0);
  c->SetBorderSize(0);

  c->Modified();
  c->Update();
  c->cd();
}
} // namespace Strangeness
