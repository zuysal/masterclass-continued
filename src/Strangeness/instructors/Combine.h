/**
 * @file   StrangeCombine.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Mon Mar 13 21:34:27 2017
 *
 * @brief  Script to combine measurements from different groups
 *
 * @ingroup alice_masterclass_str_part2
 */

#ifndef STRANGECOMBINE_H_LNI6FSZ5
#define STRANGECOMBINE_H_LNI6FSZ5

#include <RtypesCore.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TClass.h>
#include <TError.h>
#include <TFile.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TGraphErrors.h>
#include <TH1.h>
#include <THStack.h>
#include <TNtuple.h>
#include <TProfile.h>
#include <TRootCanvas.h>
#include <TString.h>
#include <TRootBrowser.h>

#include "Utility/AbstractMasterClassContent.h"

class TCanvas;
class TChain;
class TDirectory;
class TGWindow;
class TGraphErrors;
class TNtuple;
class TProfile;

namespace Strangeness {
/**
 * @{
 * @name Combine results
 *
 * @ingroup alice_masterclass_str_part2
 */
/**
 * Structure to hold observations in.
 */
    struct Obs {
        TProfile *fHI;
        TProfile *fPP;
        TProfile *fHF;

        /**
         * Constructor
         *
         * @param name   Name
         * @param title  Title
         * @param c      Colour
         * @param s      Marker style
         */
        Obs(const char *name, const char *title, Color_t c, Style_t s);

        /**
         * Fill in an observation
         *
         * @param c1 Lower centrality bound
         * @param c2 Upper centrality bound
         * @param y  Value
         * @param e  Error (not used)
         * @param f  Efficiency
         */
        void Fill(Float_t c1, Float_t c2, Float_t y, Float_t e, Float_t f);

        /**
         * Generate a TGraphErrors object from the observations
         *
         * @param mode  1: measurements, 2: yields, 3: enhancement
         * @param cvsnp profile of centrality versus Npart
         * @param cvsne profile of centrality versus Nevents
         * @param pp    Yield in pp
         *
         * @return Newly allocated graph object
         */
        TGraphErrors *Graph(UShort_t mode, TProfile *cvsnp, TProfile *cvsne, Double_t pp);
    };

    struct TCombine : Utility::TAbstractExercise {
        TRootBrowser* fBrowser;
        TCombine()
                : TAbstractExercise("combine results") {
        }

        void RunExercise(Bool_t cheat = false) override;

/**
 * Get an Ntuple
 *
 * @param d     Directory
 * @param name  Name of ntuple
 *
 * @return Pointer to histogram or null
 */
        TNtuple *CombineGetN(TDirectory *d, const char *name = "meas");

/**
 * See if we can open the given file and if it contains the Ntuple we want.
 *
 * @param filename File to test
 * @param c        Canvas to show notifications on top of
 *
 * @return true on success, false otherwise
 */
        Bool_t CombineTest(const char *filename, TCanvas *c);

/**
 * Prompt for a file or files.  Adds the selected files to the passed chain.
 *
 * @param c      Canvas to show notifications on top of
 * @param chain  Chain to add files to
 */
        void CombineOpen(TCanvas *c, TChain *chain);

/**
 * Check if we need more data
 *
 * @param c Canvas to show notifications on top of
 *
 * @return true if the user chose "Yes"
 */
        Bool_t CombineMore(TCanvas *c);
    };
}

#endif /* end of include guard: STRANGECOMBINE_H_LNI6FSZ5 */
